Plots resulting from the analysis of the raw experimental data.

Contents:
* `Evolution` Directory containing plots indicating the evolution of measurements over the course of each experiment, each plot being equivalent to Figure 3 of the paper "Laboratory Landquakes:...".
* `P_F` Directory containing plots, for each experiment, of the power spectrum of the base-normal force exerted on the instrumented plate, during the period of steady flow captured by the high-speed camera. Each plot is equivalent to Figure 4 of the paper "Laboratory Landquakes:...".
* `PTV_profiles` Directory containing plots of the profiles, in each experiment, of flow properties at the channel wall, as calculated using Particle Tracking Velocimetry.

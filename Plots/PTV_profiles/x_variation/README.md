Plots indicating the downslope variation, over the $\Delta X = 8$ cm-wide field of view of the high-speed camera and during the period of steady flow in each experiment that was captured by that camera, of the plate-normal profiles of flow properties at the channel wall, as calculated using Particle Tracking Velocimetry. The period of filmed steady flow ranged in duration from $T = 2$ seconds to $T = 10$ seconds as the reservoir release gate height varied from 40 mm to 5 mm.

Contents:
* `g{grad:4.2f}_h{gap:2d}_{rep:d}.pdf` PNG plot of flow profiles for the `rep`th experimental repeat at a channel inclination `grad`, with reservoir release gate height `gap` (mm).

Each plot represents the plate-normal profiles, at different downslope positions, of:
* The relative volume fraction $\phi$
* The mean of particles' downslope velocity $u$
* The variance of particles' downslope velocity $u$

Quantities are non-dimensionalised by the grain diameter $d = 2$ mm and the gravitational acceleration $g = 9.8$ m/s^2. Each profile is coarse-grained from individual particles' velocities using a base-normal Gaussian weighting function of width $d$, centred at different heights $z$, and a downslope Gaussian weighting function with width $5d$, centred at different downslope positions $x_0$ relative to the field of view's left-hand side. Different traces correspond to different values of $t_0$, with colours from blue to red corresponding to $x_0 = 2.5d, 7.5d, 12.5d, ..., 37.5d$.

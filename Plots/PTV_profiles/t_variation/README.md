Plots indicating the temporal variation, over the period of steady flow in each experiment that was captured by the high-speed camera, of the plate-normal profiles of flow properties at the channel wall, as calculated using Particle Tracking Velocimetry. This period of filmed steady flow ranged in duration from $T = 2$ seconds to $T = 10$ seconds as the reservoir release gate height varied from 40 mm to 5 mm.

Contents:
* `g{grad:4.2f}_h{gap:2d}_{rep:d}.pdf` PNG plot of flow profiles for the `rep`th experimental repeat at a channel inclination `grad`, with reservoir release gate height `gap` (mm).

Each plot represents the plate-normal profiles, at different times, of:
* The relative volume fraction $\phi$
* The mean of particles' downslope velocity $u$
* The variance of particles' downslope velocity $u$

Quantities are non-dimensionalised by the grain diameter $d = 2$ mm and the gravitational acceleration $g = 9.8$ m/s^2. Each profile is coarse-grained from individual particles' velocities using a base-normal Gaussian weighting function of width $d$, centred at different heights $z$, and a temporal Gaussian weighting function with width 1/10th of the duration of filming, centred at different times $t_0$. Different traces correspond to different values of $t_0$, with colours from blue to red corresponding to $t_0 / T = 0.05, 0.15, 0.25, ..., 0.95$.

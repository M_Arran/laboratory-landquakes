Directories containing plots of the profiles, in each experiment, of flow properties at the channel wall, as calculated using Particle Tracking Velocimetry.

Contents:
* `t_variation`: Plots indicating the temporal variation of flow properties' profiles, over the period of steady flow captured by the high-speed camera
* `x_variation`: Plots indicating the downslope variation of flow properties' profiles, over the 8 cm downslope window captured by the high-speed camera

Plots of the power spectrum of the plate-normal force exerted during the period of steady flow captured by the high-speed camera, in each experiment.

Contents:
* `g{grad:4.2f}_h{gap:2d}_{rep:d}.pdf` PDF plot of the power spectrum for the `rep`th experimental repeat at a channel inclination `grad`, with reservoir release gate height `gap` (mm).

Use:
In each plot, the blue, solid line indicates the measured power spectrum, while the dotted lines indicate the corner frequency $f_c$ and the low-frequency amplitude $P_F^0$. The black dashed line indicates the Hertzian power spectrum fit to these values and the blue dashed line represents a `corrected' power spectrum, calculated with an estimate for the frequency-dependent systematic relative error in the measurement of $P_F$.

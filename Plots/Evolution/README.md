Plots indicating the evolution of measurements over the course of each experiment.

Contents:
* `g{grad:4.2f}_h{gap:2d}_{rep:d}.pdf` PDF plot of experimental measurements for the `rep`th experimental repeat at a channel inclination `grad`, with reservoir release gate height `gap` (mm).

Use:
Each plot represents the evolution over time $t$ of:
* The cumulative outflow mass $M$
* The net downslope and normal forces $F_x$ and $F_z$ applied to the instrumented plate
* A measured normal plate acceleration $a_1$, with envelope indicated by dotted lines
* The power spectral density $P_F$ of the plate-normal basal force
* The integral of this power spectrum, proportional to the seismic power transmitted to the instrumented plate.

The shaded region indicates the period of steady flow recorded by the camera

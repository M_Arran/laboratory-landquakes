"""
Python3 module to analyse data from the mass balance and picoscope
Used to plot Figures 2, 3, 4 and to calculate q, \sigma, \mu, P_F^0, f_c

Functions:

calc_drift():
    Calculate force sensor drift, from 100 s intervals of 110 s null records

calc_response(plot=False):
    Calculate the force sensor response matrix, from all mu_calib files

Gamma(f):
    Return the accelerometers' high-frequency correction factor

hertz_ode(s, t):
    Calculate kinematic derivatives for a non-dimensionalised Hertzian impact

hertz_theory(calc_fc=False, plot=False, fp=np.zeros(0)):
    Calculate the normal force over a Hertzian impact, and its power spectrum
    Extract f_c, plot the impact's evolution, or interpolate |\tilde{F}|^2

plot_parameter_space():
    Plot the parameters tan theta and h_g used for experiments

plot_evolution(key, rep):
    Plot the evolution of a specified experimental flow

plot_evo_all():
    Plot the evolution of all experimental flows & record flow stage properties

plot_P_F(key, rep):
    Plot the power spectrum of basal force for a specified experimental flow

analyse_exp(key, rep):
    Analyse an experiment to extract properties of the flow and signal

analyse_all():
    Analyse all experiments, saving an output file of the above properties
"""

# Imports
import numpy as np
import pandas as pd
import matplotlib 
from matplotlib import pyplot as plt
from statsmodels.api import OLS
from scipy.signal import stft
from scipy.integrate import odeint
from scipy.optimize import root_scalar
import time

# Settings
#   Max. no of lines of picoscope data to keep in memory at a time
mem_limit = 5e5
#   Reference time (s) and output frequency (Hz) for force measurements
t0, f_force = 0.8, 200
#   Minimum and maximum frequencies and frequency-smoothing range for P_F (Hz)
f_min, f_max, f_smooth = 500, 120000, 2000
#   Max. no of points in plots of the raw data
N_plot = 1000
#   Colour of the patch marking steady flow, in plots of flow evolution
steady_color = [0.8, 0.8, 0.8]
#   Time intervals for spectrogram output in plots of flow evolution
Dt = 0.2
#   Parameters for simulated Rayleigh waves
# (ground density (kg/m^3), S-wave velocity (m/s), receiver radius (m))
rho_g, c_s, r = 2500, 1000, 1

# Folders and filenames for data, plots, and outputs
home_folder = '/media/matt/Seagate Expansion Drive'
drift_filename = home_folder + '/Experimental_data/Drift_data/drift_{0:1d}.txt'
data_folder = home_folder + '/Experimental_data/Inclination_{0:0.2f}'
mu0_filename = data_folder + '/Gap_{1:02d}_plates/mu_calib/calib_{2:1d}.txt'
mass_filename = data_folder + '/Gap_{1:02d}_plates/balance_{2:d}.txt'
pico_filename = data_folder + '/Gap_{1:02d}_plates/scope_{2:d}.txt'
dP_F_filename = home_folder + '/Experiment_results/Lab_analysis/P_F_error.txt'
plot_folder = home_folder + '/Experiment_results/Lab_analysis/Plots'
raw_plotname = plot_folder + '/raw_g{0:0.2f}_h{1:02d}_{2:1d}.png'
out_plotname = plot_folder + '/out_g{0:0.2f}_h{1:02d}_{2:1d}.png'
evo_plotname = plot_folder + '/evo_g{0:0.2f}_h{1:02d}_{2:1d}.pdf'
P_F_plotname = plot_folder + '/P_F_g{0:0.2f}_h{1:02d}_{2:1d}.pdf'
output_folder = home_folder + '/Experiment_results/Lab_analysis'
results_filename = output_folder + '/results.txt'
evo_filename = output_folder + '/evolution.txt'

# Experiment parameters
keys = ['g0.44_h05', 'g0.44_h40', 'g0.44_h10', 'g0.44_h20',
        'g0.48_h40', 'g0.48_h20', 'g0.48_h14',
        'g0.50_h40', 'g0.50_h20',
        'g0.46_h20', 'g0.46_h10', 'g0.46_h40',
        'g0.52_h40', 'g0.52_h28',
        'g0.42_h05', 'g0.42_h20', 'g0.42_h10', 'g0.42_h40',
        'g0.54_h40']
columns = ['grad', 'grad_err', 'gap', 't_start', 't_steady', 'duration', 'amp']
params = pd.DataFrame([(0.4400,  0.002,  5, 70, 10, 200, 0.0316 / 0.006),
                       (0.4400,  0.002, 40,  8,  1,  20, 0.01 / 0.006),
                       (0.4400,  0.002, 10, 30, 10, 100, 0.0316 / 0.006),
                       (0.4400,  0.002, 20, 15,  8,  40, 0.01 / 0.006),
                       (0.4801, 0.0013, 40,  8,  2,  20, 0.00316 / 0.006),
                       (0.4801, 0.0013, 20, 15,  8,  40, 0.00316 / 0.006),
                       (0.4801, 0.0013, 14, 15, 10,  60, 0.00316 / 0.006),
                       (0.4990, 0.0006, 40,  8,  2,  20, 0.00316 / 0.006),
                       (0.4990, 0.0006, 20, 15,  8,  40, 0.00316 / 0.006),
                       (0.4598, 0.0012, 20, 17,  8,  40, 0.00316 / 0.006),
                       (0.4598, 0.0012, 10, 30, 10, 100, 0.00316 / 0.006),
                       (0.4598, 0.0012, 40,  8,  2,  20, 0.00316 / 0.006),
                       (0.5196, 0.0018, 40,  8,  2,  20, 0.00316 / 0.006),
                       (0.5196, 0.0018, 28,  8,  4,  20, 0.00316 / 0.006),
                       (0.4202, 0.0007,  5, 70, 10, 200, 0.0316 / 0.006),
                       (0.4202, 0.0007, 20, 12,  8,  40, 0.01 / 0.006),
                       (0.4202, 0.0007, 10, 30, 10, 100, 0.01 / 0.006),
                       (0.4202, 0.0007, 40, 8.5, 1,  20, 0.00316 / 0.006),
                       (0.5397, 0.0015, 40,  8,  2,  20, 0.00316 / 0.006)],
                      index = keys, columns = columns)
#   Times at which flow stages start, hand-picked from plots
t_names = ['salt', 'frnt', 'stdy', 'dcay', 'end']
times = pd.DataFrame([(5, 45, 50, 200, 200),
                      (2, 4, 6, 10, 20),
                      (3, 15, 18, 66, 86),
                      (3, 8, 11, 24, 40),
                      (5, 5, 8, 12, 20),
                      (4, 4, 8, 23, 40),
                      (2, 2, 5, 35, 45),
                      (5, 5, 7, 12, 20),
                      (5, 5, 8, 23, 30),
                      (5, 7, 12, 25, 40),
                      (0, 2, 5, 58, 75),
                      (3.5, 3.5, 7, 11, 20),
                      (3.5, 3.5, 6, 10, 16),
                      (2, 2, 4, 12, 18),
                      (2, 22, 30, 165, 180),
                      (2, 5, 7, 21, 30),
                      (2, 10, 15, 60, 75),
                      (2, 2.5, 6, 10, 15),
                      (4, 4, 6, 10, 15)],
                      index = keys, columns = t_names)

# Experimental constants
#   Gravitational acceleration (m/s**2)
g = 9.81
#   Mass balance sampling frequency (Hz)
f_balance = 5
#   Picoscope sampling frequency (Hz), time units (s)
f_pico, t_prefix, mu0_prefix = 250000, 1e-6, 1e-3
#   Picoscope channels
ch_t, ch_trigg = 0, 5
ch_Fx, ch_Fz = 'G', 'H'
ch_a = ['A', 'B', 'C', 'D']
mu0_ch = [2, 3]

#   Force sensor drift (V / s)
#       Hardcoded dV, previously calculated
dV = np.array([-0.00121, 0.00225])
dV_err = np.array([0.00007, 0.00018])

def calc_drift():
    """
    Calculates the expected drift rate of the force sensor's voltage output
    
    Returns:
        The estimated expected drift and its error
    """
    id_st, id_en = int(t0 * f_force), int((t0 + 100) * f_force)
    dV = np.nan * np.ones([10, 2])
    for rep in range(10):
        drift_data = np.loadtxt(drift_filename.format(rep),
                                skiprows=2, delimiter=',')
        dV[rep] = (drift_data[id_en, mu0_ch] - drift_data[id_st, mu0_ch]) / 100
    return dV.mean(axis=0), dV.std(axis=0)/np.sqrt(10)

#   Force sensor response matrix (V / N)
#       Vertical load on plate and time interval in which it is stable
calib_load, calib_time = 10 * 0.04423 * g, np.array([3.0, 4.0])
#       Hardcoded response matrix, previously calculated
FtoV = np.array([[0.83102, -0.03522], [-0.13462, 0.35345]])
FtoV_cov = np.array([[[ 9e-05, -4e-05], [-4e-05,  2e-05]],
                     [[ 2.6e-04, -1.2e-04], [-1.2e-04,  6e-05]]])
#       Calculate response from all mu_calib files
def calc_response(plot=False):
    """
    Calculates the parameters of the force sensor's voltage response to forces

    Args:
        plot: boolean for whether to plot the response

    Returns:
        the best-fit parameters B for the linear model DV = DF.B
        and the stacked covariance matrices for DV_x and DV_z
    """
    id_st, id_en = int(calib_time[0] * f_force), int(calib_time[1] * f_force)
    F = np.nan * np.ones([8 * params.shape[0], 2])
    V = np.nan * np.ones([8 * params.shape[0], 2])
    FtoV = np.nan * np.ones([2, 2])
    FtoV_cov = np.nan * np.ones([2, 2, 2])
    # Iterate through all experiments
    for e_id, (grad, gap) in enumerate(zip(params.grad, params.gap)):
        # Calculate force on plate
        th = np.arctan(grad)
        F[8*e_id: 8*(e_id+1)] = calib_load * np.array([np.sin(th), np.cos(th)])
        # Calculate voltage response of force sensor
        # (after accounting for initial jump & linear drift)
        for rep in range(8):
            mu0_data = np.loadtxt(mu0_filename.format(grad, gap, rep),
                                  skiprows=2, delimiter=',')
            V[8*e_id + rep] = (mu0_data[id_st:id_en, mu0_ch].mean(axis=0)
                               - mu0_data[int(f_force * t0), mu0_ch]
                               - (calib_time.mean() - t0) * dV)
    # Least squares fit for linear map between force and voltage
    for c_id in range(2):
        OLS_results = OLS(V[:,c_id], F).fit()
        FtoV[c_id] = OLS_results.params
        FtoV_cov[c_id] = OLS_results.cov_params()
    # Plot fit
    if (plot == True):
        grad = np.arange(np.min(params.grad), np.max(params.grad), 0.001)
        th = np.arctan(grad)
        F = calib_load * np.vstack([np.sin(th), np.cos(th)]).T
        fig = plt.figure(figsize=[4, 3])
        ax = fig.add_axes([0.15, 0.15, 0.8, 0.8])
        ax.plot(params.grad.repeat(8), V[:,0], '.r',
                params.grad.repeat(8), V[:,1], '.b', markersize=2)
        ax.plot(grad, F.dot(FtoV[0]), '--k',
                grad, F.dot(FtoV[1]), '--k')
        ax.set_xlabel(r'$\tan \theta$', fontsize=8)
        ax.set_ylabel(r'$V^r_j$ (V)', fontsize=8)
        ax.tick_params(axis='both', labelsize=8)
        plt.show()
    return FtoV, FtoV_cov

#   Accelerometer sensitivities (pC / (m/s^2))
sensitivities = [0.00675, 0.00561, 0.00538, 0.00658]
#   Accelerometer high-frequency correction
def Gamma(f):
    """
    Calculates the high-frequency correction to the accelerometer output

    Args:
        f: numpy array of frequencies

    Returns:
        correction factor at those frequencies
    """
    return 1 + (f / 6.1e4)**6
#    Proportion of plate energy in vertical motion, plate quality factor
P, Q = 0.25, 99
#   Plate Young's modulus (kg / (m s^2)), Poisson ratio
E_p, nu_p = 200e9, 0.29
#   Plate density (kg / m^3), length (m), width (m), thickness (m)
rho_p, X, Y, H = 7800, 0.18, 0.1, 2e-3
#   Plate bending stiffness (kg m^2/s^2)
D = E_p * H**3 / (12 * (1 - nu_p**2))
#   Prefactor for calculating the power spectrum P_F of the basal force
P_F_prefactor = (rho_p * H)**1.5 * X * Y * np.sqrt(D) / (np.pi * P * Q)
#   Channel width (m)
W = 0.2

# Hertz theory
#   Bead diameter (m), density (kg / m^3),
#       Young's modulus (kg / (m s^2)), Poisson ratio
d, rho, E, nu = 2e-3, 2500, 63e9, 0.23
#   Timestep for Hertzian force integration
hertz_dt = 0.002
#   Number of points used for Fourier transform
hertz_nfft = 2**20
# Calculate the normal force F throughout a non-dimensionalised Hertzian impact
def hertz_ode(s, t):
    """
    Helper function for the simulation of a nondimensional Hertzian impact

    Args:
        s: numpy array for displacement and velocity
        t: float for time

    Returns:
        numpy array of velocity and acceleration
    """
    return np.array([s[1], (-s[0])**1.5 if s[0] < 0 else 0])
# Previously calculated corner frequency f_c of |\tilde{F}|^2
nondim_f_c = 0.208
# Calculate f_c, plot the impact's evolution, or interpolate |\tilde{F}|^2
def hertz_theory(calc_fc=False, plot=False, fp=np.zeros(0)):
    """
    Simulates a nondimensional Hertzian impact, calculating normal force F
    Calculates zeta = |\\tilde{F}|^2 / 4, for Fourier transform \\tilde{}

    Args:
        calc_fc: boolean for whether to calculate the corner frequency
        plot: boolean for whether to produce plots
        fp: array of frequencies at which to return zeta(f), if calc_fc=False
    Returns:
        if calc_fc is True, the corner frequency of zeta
        otherwise, the interpolated values of zeta
    """ 
    global nondim_f_c
    # Simulate a non-dimensional Hertzian impact and calculate the normal force
    t = np.arange(0, 5, hertz_dt)
    solution = odeint(hertz_ode, np.array([0, -1]), t)
    F_t = np.array([((-s)**1.5 if s < 0 else 0) for s in solution[:,0]])
    # Calculate the spectral density of the normal force
    f = np.arange(hertz_nfft) / (hertz_nfft * hertz_dt)
    PSD = np.abs(np.fft.fft(F_t, n=hertz_nfft) * hertz_dt)**2
    # If required, calculate the corner frequency by interpolation
    if (calc_fc == True):
        sol = root_scalar(lambda fs:np.interp(fs, f, PSD) - 2,
                          bracket=[0,1], method='brentq', xtol=1e-4)
        nondim_f_c = sol.root
    # If required, plot the displacement, force, and force's spectral density
    if (plot == True):
        fig = plt.figure(figsize=[4, 2])
        ax_s = fig.add_axes([0.125, 0.25, 0.325, 0.7],
                            xlim=[0, 4], ylim=[-1.2, 1.2], yticks=[-1,0,1])
        ax_s.plot(t, solution[:,0], '-', color='r')
        ax_s.set_xlabel(r'$T$', fontsize=8)
        ax_s.xaxis.set_label_coords(0.5, -0.15)
        ax_s.set_ylabel(r'$\Delta$', fontsize=8, color='r')
        ax_s.yaxis.set_label_coords(-0.15, 0.7)
        ax_s.tick_params('both', labelsize=8)
        ax_F = ax_s.twinx()
        ax_F.plot(t, F_t, '-k')
        ax_F.set_ylim(0, 1.2)
        ax_F.set_yticks([0, 0.5, 1])
        ax_F.set_ylabel(r'$\mathcal{F}_n$', fontsize=8)
        ax_F.yaxis.set_label_coords(1.15, 0.7)
        ax_F.tick_params('both', labelsize=8)
        f_lim = np.array([5e-4, 2])
        PSD_lim = np.array([1e-5, 1e1])
        ax_f = fig.add_axes([0.65, 0.25, 0.325, 0.7], xscale='log', xlim=f_lim,
                            yscale='log', ylim=PSD_lim, yticks=[1e-4, 1e-2, 1])
        ax_f.plot(f, PSD / 4, '-k')
        ax_f.plot(nondim_f_c * np.ones(2), PSD_lim, ':k')
        ax_f.set_xlabel(r'$f \tau$', fontsize=8)
        ax_f.xaxis.set_label_coords(0.5, -0.15)
        ax_f.set_ylabel(r'$\zeta$', fontsize=8)
        ax_f.yaxis.set_label_coords(-0.15, 0.7)
        ax_f.tick_params('both', labelsize=8)
        plt.show()
    # If required, return the corner frequency of the force's spectral density
    if (calc_fc == True):
        return nondim_f_c
    # Otherwise, return the interpolated values of the scaled spectral density
    if (fp.size > 0):
        return(np.interp(fp, f, PSD / 4))

# Plot the parameters \tan \theta and h_g used in experiments
def plot_parameter_space():
    """ Plot the parameters tan theta and h_g used for experiments """
    # Booleans for whether each experimental flow was in the transitional regime
    trans_regime = [False, False, False, False, False, False, True,
                    False, True, False, True, False, False, True,
                    False, False, False, False, True]
    grad_lim = np.array([0.41, 0.55])
    fig = plt.figure(figsize=[4, 2.5])
    ax_grad = fig.add_axes([0.15, 0.15, 0.7, 0.7],
                           xlim=grad_lim, xticks=[0.42, 0.46, 0.5, 0.54],
                           ylim=[4, 50], yscale='log', yticks=[5, 10, 20, 40])
    ax_grad.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    sc = ax_grad.scatter(params.grad[trans_regime], params.gap[trans_regime],
                         c=params.t_steady[trans_regime])
    sc.set_facecolor("none")
    sc = ax_grad.scatter(params.grad[np.logical_not(trans_regime)],
                         params.gap[np.logical_not(trans_regime)],
                         c=params.t_steady[np.logical_not(trans_regime)])
    ax_grad.set_xlabel(r'$\tan \theta$', fontsize=8)
    ax_grad.set_ylabel(r'$h_g$ (mm)', fontsize=8)
    ax_grad.tick_params('both', labelsize=8)
    ax_c = fig.add_axes([0.8, 0.15, 0.05, 0.35]) 
    fig.colorbar(sc, ax_c)
    ax_c.set_ylabel(r'$\Delta t_c$ (s)', fontsize=8)
    ax_c.tick_params('both', labelsize=8)
    ax_th = ax_grad.twiny()
    ax_th.set_xlim(180 / np.pi * np.arctan(grad_lim))
    ax_th.set_xlabel(r'$\theta$ ($\degree$)', fontsize=8)
    ax_th.tick_params('both', labelsize=8)
    plt.show()

# Plot the evolution of the specified repeat of the specified experimental flow
def plot_evolution(key, rep):
    """ 
    Plots the evolution of a specified experimental flow

    Args: 
        key: the key 'g0.##h##' of the desired flow
        rep: the repeat number 0-2 of the desired flow
    Returns:
        array with the four flow stages' durations, corner frequencies,
        mean squared basal forces, and the standard deviation of each
    """

    grad, gap = params.grad[key], params.gap[key]
    print('Plotting flow evolution of rep {2:1d} at grad {0:0.2f}, gap {1:02d}'
          .format(grad, gap, rep))
    # Set up constants:
    #    Force downsampling rate n_av & signal inversion matrix VtoF
    n_av = int(f_pico / f_force)
    VtoF = np.linalg.inv(FtoV)
    #    Frequency range and downsampling rate for calculation of P_F
    f = np.arange(f_min, f_max, 1 / Dt) 
    n_smooth = int(f_smooth * Dt / 2)
    #    Temporal range and chunking rate
    t_Dt = np.arange(0, params.duration[key] + Dt, Dt)
    n_Dt = int(f_pico * Dt)
    chunksize = int(mem_limit // n_Dt) * n_Dt
    # Read the force sensor's offset calibration
    pico_data = pd.read_csv(pico_filename.format(grad, gap, rep),
                            header=1, nrows=int(f_pico*t0)+n_av)
    pico_data.columns = pico_data.columns.str.strip()
    V_av_0 = pico_data[[ch_Fx, ch_Fz]][int(f_pico*t0):].mean()
    # Read the raw data, with the large picoscope file read in chunks
    if not (int(f_pico*Dt) % n_av == 0):
        print('For chunked filereading, f_force Dt must be an integer')
        exit()
    mass_data = pd.read_csv(mass_filename.format(grad, gap, rep),
                            names=['t', 'M'], skiprows=1)
    pico_data = pd.read_csv(pico_filename.format(grad, gap, rep),
                            header=1, chunksize=chunksize)
    # Set up arrays to be populated with time, force, and power spectral data
    t_F = np.empty(0)
    F_t = np.empty([2, 0])
    a_0 = np.empty(0)
    a_e = np.empty(0)
    P_F = np.empty([f.size - 2*n_smooth, 0])
    # Process the picoscope data in chunks, for memory reasons
    for data in pico_data:
        data.columns = data.columns.str.strip()
        t_F = np.hstack([t_F, t_prefix * data.t[::n_av]])
        # Calculate the net force on the instrumented plate
        #    Downsample data from the force sensor
        V_av = pd.DataFrame([data[[ch_Fx, ch_Fz]][n_av*a_id:n_av*(a_id+1)]
                             .mean()
                             for a_id in range(data.shape[0] // n_av)])
        #    Compensate for the initial offset and expected drift
        V = (V_av - V_av_0 - np.outer(t_prefix * data.t[::n_av] - t0, dV))
        #    Invert the force sensor signal for the net force
        F_t = np.hstack([F_t, VtoF @ (V.T)])
        # Record the acceleration
        a_0 = np.hstack([a_0, (data[ch_a[0]][::n_av]
                               / (sensitivities[0] * params.amp[key]))])
        a_rms = np.array([np.sqrt(np.mean(data[ch_a[0]][n_Dt*j:n_Dt*(j+1)]**2))
                          / (sensitivities[0] * params.amp[key])
                          for j in range(int(data.shape[0] // n_Dt))])
        a_e = np.hstack([a_e, a_rms])
        # Calculate the power spectrum of the applied plate-normal force
        U = np.zeros([int(n_Dt // 2 + 1), 
                      int(np.ceil(data.shape[0] / n_Dt))])
        for [ch, s_hat] in zip(ch_a, sensitivities):
            U += np.abs(stft(data[ch] / s_hat, window='boxcar', boundary=None,
                             nperseg=n_Dt, noverlap=0)[2] * Dt)**2
        P_F_new = np.array((P_F_prefactor / (f * Dt)
                            * Gamma(f) / params.amp[key]**2)[:,np.newaxis]
                           * U[(f * Dt).astype(int)])
        P_F_new = np.cumsum(P_F_new, axis=0) / (2*n_smooth)
        P_F = np.hstack([P_F, (P_F_new[2*n_smooth:]
                               - P_F_new[:-2*n_smooth])])
    # Calculate derived quantities: corner freq., integral
    id_max = np.argmax(P_F, axis=0) + 1
    id_c = np.array([id_m + np.argmax(p_F[id_m:] < p_F[:id_m].mean() / 2)
                     for id_m, p_F in zip(id_max, P_F.T)])
    f_c = np.where(id_c - id_max, f[n_smooth + id_c], f_max)
    P = P_F.sum(axis=0) / Dt
    # Plot the evolution of flow properties
    fig = plt.figure(figsize=[4.5, 4.8])
    steady_range = params.t_start[key]+np.array([0, params.t_steady[key]])
    # Plot the balance output
    ax_M = fig.add_axes([0.13, 0.8, 0.71, 0.15],
                        xlim=[0, params.duration[key]], xticklabels=[],
                        ylim=[0, 35])
    ax_M.fill_between(steady_range, 0, 35, color=steady_color)
    ax_M.plot(mass_data.t, mass_data.M)
    ax_M.set_ylabel(r'$M$ (kg)', fontsize=8)
    ax_M.tick_params('both', labelsize=8)
    ax_M.yaxis.set_label_coords(-0.12, 0.5)
    # Plot the net normal force on the instrumented plate
    Fz_max = 1.2 * F_t[1].max()
    ax_F = fig.add_axes([0.13, 0.63, 0.71, 0.15],
                        xlim=[0, params.duration[key]], xticklabels=[],
                        ylim=[0, Fz_max])
    ax_F.fill_between(steady_range, 0, Fz_max, color=steady_color)
    ax_F.plot(t_F, F_t[0], c=(0.37, 0.79, 0.38), label=r'$F_x$')
    ax_F.plot(t_F, F_t[1], c=(0.23, 0.32, 0.55), label=r'$F_z$')
    ax_F.set_ylabel(r'$F_\cdot$ (N)', fontsize=8)
    ax_F.tick_params('both', labelsize=8)
    ax_F.yaxis.set_label_coords(-0.12, 0.5)
    ax_F.legend(loc=(1.01, 0.25), handlelength=1, fontsize=8, frameon=False)
    # Plot the acceleration signal
    a_max = 1.1 * np.abs(a_0).max()
    ax_a = fig.add_axes([0.13, 0.46, 0.71, 0.15],
                        xlim=[0, params.duration[key]], xticklabels=[],
                        ylim=[-a_max, a_max])
    ax_a.fill_between(steady_range, -a_max, a_max, color=steady_color)
    ax_a.plot(t_F, a_0)
    ax_a.plot(t_Dt[:-1] + Dt / 2, a_e, ':k', t_Dt[:-1] + Dt / 2, -a_e, ':k')
    ax_a.set_ylabel(r'$a_1$ (m s$^{-2}$)', fontsize=8)
    ax_a.tick_params('both', labelsize=8)
    ax_a.yaxis.set_label_coords(-0.12, 0.6)
    # Plot a spectrogram of the plate-normal basal force
    ax_P_F = fig.add_axes([0.13, 0.29, 0.71, 0.15],
                          xlim=[0, params.duration[key]], xticklabels=[],
                          ylim=[0, 1e-3 * f[::2*n_smooth][-1]])
    mesh = ax_P_F.pcolormesh(t_Dt, 1e-3 * f[::2*n_smooth],
                             1e6 * P_F[::2*n_smooth], vmin=0)
    ax_P_F.plot(params.t_start[key] * np.ones(2), 1e-3 * f[[0, -1]], '--',
                (params.t_start[key] + params.t_steady[key]) * np.ones(2),
                1e-3 * f[[0, -1]], '--', color=[0.5, 0.5, 0.5])
    ax_P_F.set_ylabel(r'$f$ (kHz)', fontsize=8)
    ax_P_F.tick_params('both', labelsize=8)
    ax_P_F.yaxis.set_label_coords(-0.12, 0.5)
    # Plot the integral of the spectral density
    P_max = 1.1 * P.max()
    ax_P = fig.add_axes([0.13, 0.12, 0.71, 0.15],
                        xlim=[0, params.duration[key]], ylim=[0, P_max])
    ax_P.plot(t_Dt[:-1] + Dt / 2, P)
    ax_P.fill_between(steady_range, 0, P_max, color=steady_color)
    ax_P.set_xlabel(r'$t$ (s)', fontsize=8)
    ax_P.set_ylabel(r'$\int P_F $ d$f$ (N$^2$)', fontsize=8)
    ax_P.tick_params('both', labelsize=8)
    ax_P.yaxis.set_label_coords(-0.12, 0.5)
    # Plot a colorbar
    ax_c = fig.add_axes([0.85, 0.29, 0.025, 0.15], yticklabels=[])
    fig.colorbar(mesh, ax_c)
    ax_c.set_ylabel(r'$P_F$ (mN$^2$ s)', fontsize=8)
    ax_c.tick_params('both', labelsize=8)
    # Save and close the figure
    plt.savefig((evo_plotname.format(grad, gap, rep)))
    plt.show()
    plt.close()
    # Calculate flow properties in each stage of the flow's evolution
    t_salt = times.frnt[key] - times.salt[key]
    t_frnt = times.stdy[key] - times.frnt[key]
    t_stdy = times.dcay[key] - times.stdy[key]
    t_dcay = times.end[key] - times.dcay[key]
    stage_I = (times.salt[key] <= t_Dt[:-1]) * (t_Dt[:-1] < times.frnt[key])
    stage_II = (times.frnt[key] <= t_Dt[:-1]) * (t_Dt[:-1] < times.stdy[key])
    stage_III = (times.stdy[key] <= t_Dt[:-1]) * (t_Dt[:-1] < times.dcay[key])
    stage_IV = (times.dcay[key] <= t_Dt[:-1]) * (t_Dt[:-1] < times.end[key])
    fc_salt = f_c[stage_I].mean()
    fc_frnt = f_c[stage_II].mean()
    fc_stdy = f_c[stage_III].mean()
    fc_dcay = f_c[stage_IV].mean()
    P_salt = P[stage_I].mean()
    P_frnt = P[stage_II].mean()
    P_stdy = P[stage_III].mean()
    P_dcay = P[stage_IV].mean()
    # Calculate variation in flow properties during camera recording
    recorded = ((params.t_start[key] <= mass_data.t+0.5)
                & (mass_data.t-0.5 < params.t_start[key]+params.t_steady[key]))
    q_rec = mass_data.M[recorded].diff(periods=5)
    dq_rec = q_rec.std() / q_rec.mean()
    recorded = ((params.t_start[key] <= t_F)
                & (t_F < params.t_start[key]+params.t_steady[key]))
    Fz_rec = F_t[1, recorded].reshape([-1, int(f_force * Dt)]).mean(axis=0)
    dFz_rec = Fz_rec.std() / Fz_rec.mean()
    recorded = ((params.t_start[key] <= t_Dt[:-1])
                & (t_Dt[:-1] < params.t_start[key] + params.t_steady[key]))
    dfc_rec = np.nanstd(f_c[recorded]) / np.nanmean(f_c[recorded])
    dP_rec = P[recorded].std() / P[recorded].mean()
    res = [t_salt, t_frnt, t_stdy, t_dcay,
           fc_salt, fc_frnt, fc_stdy, fc_dcay,
           P_salt, P_frnt, P_stdy, P_dcay,
           dq_rec, dFz_rec, dfc_rec, dP_rec]
    return np.array(res)

# Plot the evolution of all experimental flows, recording flow stage properties
def plot_evo_all():
    """
    Plots the evolution of all experimental flows
    Saves an output file of flow properties in each flow stage
    """
    header = (  'Gradient,      Gap, Repeat, '
              + '  t_salt,   t_frnt,   t_stdy,   t_dcay, '
              + 'f_c_salt, f_c_frnt, f_c_stdy, f_c_dcay, '
              + '  P_salt,   P_frnt,   P_stdy,   P_dcay, '
              + '  dq_rec,  dFz_rec, df_c_rec,   dP_rec, \n'
              + '        , (plates),       , '
              + '     (s),      (s),      (s),      (s), '
              + '    (Hz),     (Hz),     (Hz),     (Hz), '
              + '   (N^2),    (N^2),    (N^2),    (N^2), '
              + '        ,         ,         ,         ')
    fmts = ['%8.4f', '%8d', '%6d',
            '%8.1f', '%8.1f', '%8.1f', '%8.1f', 
            '%8.0f', '%8.0f', '%8.0f', '%8.0f', 
            '%8.5f', '%8.5f', '%8.5f', '%8.5f', 
            '%8.6f', '%8.6f', '%8.6f', '%8.6f']
    data = np.zeros([len(keys), 19])
    for e_id, key in enumerate(keys):
        data[e_id, 0] = params.grad[key]
        data[e_id, 1] = params.gap[key]
        data[e_id, 2] = 0
        data[e_id, 3:] = plot_evolution(key, 0)
    np.savetxt(evo_filename, data,
               header=header, comments='', delimiter=', ', fmt=fmts)

# Plot the power spectrum of basal force for a specified experimental flow
def plot_P_F(key, rep):
    """
    Plots the power spectrum of basal forces for a specified flow

    Args:
        key: the key 'g0.##h##' of the desired flow
        rep: the repeat number 0-2 of the desired flow
    """
    grad, gap = params.grad[key], params.gap[key]
    print('Plotting P_F for repeat {2:1d} at grad {0:0.2f}, gap {1:02d}'
          .format(grad, gap, rep))
    fig = plt.figure(figsize=[4, 2])
    # Read raw data for the period of steady flow
    n_presteady = int(f_pico * params.t_start[key])
    pico_data = pd.read_csv(pico_filename.format(grad, gap, rep),
                            header=1,
                            skiprows=range(2, 2 + n_presteady),
                            nrows=int(f_pico * params.t_steady[key]))
    pico_data.columns = pico_data.columns.str.strip()
    # Calculate P_F of vertical basal force
    f = np.arange(f_min, f_max, 1/params.t_steady[key])
    n_smooth = int(f_smooth * params.t_steady[key] / 2)
    U = np.zeros(f_pico * params.t_steady[key])
    for [ch, s_hat] in zip(ch_a, sensitivities):
        U += np.abs(np.fft.fft(pico_data[ch] / (s_hat * f_pico)))**2
    P_F = (P_F_prefactor / f * Gamma(f) / params.amp[key]**2
           * U[(f * params.t_steady[key]).astype(int)]) / params.t_steady[key]
    P_F = np.cumsum(P_F) / (2*n_smooth)
    P_F = P_F[2*n_smooth:] - P_F[:-2*n_smooth]
    # Extract corner frequency and P_F amplitude
    id_max = np.argmax(P_F) + 1
    id_corner = id_max + np.argmax(P_F[id_max:] < P_F[:id_max].mean() / 2)
    f_corner = f[id_corner]
    P_F_amp = P_F[:int(id_corner/2)].mean()
    # Calculate corresponding impact velocity
    K_i = 1./3. * E * np.sqrt(d)
    m_i = 1./6. * np.pi * rho * d**3
    T_i = nondim_f_c / f_corner
    v_i = m_i**2 * K_i**-2 * T_i**-5
    f_i = P_F_amp / (2 * m_i * v_i)**2
    print('Corresponding impact velocity {} m/s'.format(v_i))
    print('Corresponding impact rate {} m/s'.format(f_i))
    # Read csv file and interpolate systematic relative error
    error_data = pd.read_csv(dP_F_filename, skiprows=[1])
    error_data.columns = error_data.columns.str.strip()
    id_compare = (n_smooth <= np.arange(f.size)) & (f < 1e5)
    dP_F = np.interp(f[id_compare], error_data.f, error_data.error)
    # Plot power spectrum
    ax = fig.add_axes([0.15, 0.25, 0.8, 0.7], xscale='log', yscale='log')
    line = ax.plot(f[n_smooth:-n_smooth], P_F)
    ax.plot(f[id_compare], P_F[:id_compare.sum()] / (1 + dP_F),
            '--', color=line[0].get_color())
    ax.plot(f[n_smooth:-n_smooth],
            P_F_amp * hertz_theory(fp=f[n_smooth:-n_smooth] * T_i), '--k')
    f_lim = ax.get_xlim()
    P_F_lim = ax.get_ylim()
    ax.plot(np.array([f_lim[0], f_corner/2]), P_F_amp * np.ones(2), ':k')
    ax.text(f[n_smooth], 1.2 * P_F_amp, r'$P_F = P_F^0$', fontsize=8)
    ax.plot(f_corner * np.ones(2), np.array([P_F_lim[0], P_F_amp/2]), ':k')
    ax.text(0.6 * f_corner, P_F[-1], r'$f = f_c$', fontsize=8)
    ax.set_xlim(f_lim)
    ax.set_ylim(P_F_lim)
    ax.set_xlabel(r'$f$ (Hz)', fontsize=8)
    ax.set_ylabel(r'$P_F(f)$ (N$^2$ s)', fontsize=8)
    ax.tick_params('both', labelsize=8)
    fig.savefig(P_F_plotname.format(grad, gap, rep))
    plt.show()
    plt.close()

# Analyse an experiment to extract q, sigma, mu, P_F0, f_c    
def analyse_exp(key, rep):
    """
    Analyse an experiment to extract properties of the flow and signal
    
    Args:
        key: the key 'g0.##h##' of the desired flow
        rep: the repeat number 0-2 of the desired flow
    Returns:
        array of mass flux q and overburden \\sigma, eff. friction \\mu,
        power spectrum corner frequency f_c and amplitude P_F^0,
        mean squared signal for experimental and ideal geophysical cases
    """
    grad, gap = params.grad[key], params.gap[key]
    print('Starting analysis of rep {2:1d} at grad {0:0.2f}, gap {1:02d}'
          .format(grad, gap, rep))
    start_time = time.time()
    # Set up force data processing
    n_av = int(f_pico / f_force)
    VtoF = np.linalg.inv(FtoV).T
    # Read the force sensor's offset calibration
    pico_data = pd.read_csv(pico_filename.format(grad, gap, rep),
                            header=1, nrows=int(f_pico*t0)+n_av)
    pico_data.columns = pico_data.columns.str.strip()
    V_av_0 = pico_data[[ch_Fx, ch_Fz]][int(f_pico*t0):].mean()
    # Read the raw data from the period recorded by the camera
    mass_data = pd.read_csv(mass_filename.format(grad, gap, rep),
                            names=['t', 'M'], skiprows=1)
    pico_data = pd.read_csv(pico_filename.format(grad, gap, rep),
                            header=1,
                            skiprows=range(2, 2 + int(f_pico
                                                      * params.t_start[key])),
                            nrows=int(f_pico * params.t_steady[key]))
    pico_data.columns = pico_data.columns.str.strip()
    # Extract mass flux q (kg m^-1 s^-1)
    q = ((mass_data.M[int(f_balance * (params.t_start[key]
                                       + params.t_steady[key]))]
          - mass_data.M[int(f_balance * params.t_start[key])])
         / (W * params.t_steady[key]))
    print('  Mean flux q = {} kg m^-1 s^-1'.format(q))
    # Extract mass (kg m^-2), effective friction coefficient
    #     Downsample data from the force sensor
    V_av = pd.DataFrame([pico_data[[ch_Fx, ch_Fz]][n_av*a_id:n_av*(a_id+1)]
                         .mean()
                         for a_id in range(pico_data.shape[0] // n_av)])
    #     Compensate for the initial offset and expected drift
    V = (V_av - V_av_0 - np.outer(t_prefix * pico_data.t[::n_av] - t0, dV))
    #     Invert the force sensor signal for the net force
    F_t = V @ VtoF
    #     Calculate mass, effective friction coefficient 
    m = F_t[1].mean() * np.sqrt(1 + grad**2) / (g * X * Y)
    mu = F_t[0].mean() / F_t[1].mean()
    print('  Mean overlying mass m = {} kg m^-2'.format(m))
    print('  Mean friction angle \mu = {}'.format(mu))
    # Extract power spectral density of base-normal force on plate
    f = np.arange(f_min, f_max, 1/params.t_steady[key])
    n_smooth = int(f_smooth * params.t_steady[key] / 2)
    U = np.zeros(f_pico * params.t_steady[key])
    for [ch, s_hat] in zip(ch_a, sensitivities):
        U += np.abs(np.fft.fft(pico_data[ch]) / (s_hat * f_pico))**2
    P_F = (P_F_prefactor / f * Gamma(f) / params.amp[key]**2
           * U[(f * params.t_steady[key]).astype(int)]) / params.t_steady[key]
    P_F = np.cumsum(P_F) / (2* n_smooth)
    P_F = P_F[2*n_smooth:] - P_F[:-2*n_smooth]
    id_max = np.argmax(P_F) + 1
    id_corner = id_max + np.argmax(P_F[id_max:] < P_F[:id_max].mean() / 2)
    # Calculate power spectral density of velocity reponses
    # (for the experiment and for an idealised geophysical scenario)
    P_ve = P_F / (P_F_prefactor * 16 * np.pi**2 * f[n_smooth:-n_smooth])
    P_vR = ((1.2 * f[n_smooth:-n_smooth]**3)
            / (rho_g**2 * c_s**5 * r)) * P_F
    if (id_corner == id_max):
        f_corner = np.nan
        P_F_amp = P_F[:int(P_F.size / 2)].mean()
        ve2_bar = np.nan
        vR2_bar = np.nan
    else:
        f_corner = f[id_corner]
        P_F_amp = P_F[:int(id_corner/2)].mean()
        ve2_bar = P_ve[:id_corner].sum() / params.t_steady[key]
        vR2_bar = P_vR[:id_corner].sum() / params.t_steady[key]
    print('  Amplitude of basal force power P^0_F = {} N^2 s'
          .format(P_F_amp))
    print('  Corner frequency f_c = {} Hz'.format(f_corner))
    print('Finished analysis. Duration {} s'.format(time.time() - start_time))
    return np.array([q, m, mu, f_corner, P_F_amp, ve2_bar, vR2_bar])

# Analyse all experiments
def analyse_all():
    """Analyse all experiments and save an output file of their properties"""
    header = (  'Gradient,      Gap, Repeat,         q,        m,       mu, '
                + '     f_c,       P_F0,    ve2_bar,    vR2_bar\n'
              + '        , (plates),       ,  (kg/m s), (kg/m^2),         , '
                + '    (Hz),    (N^2 s), (m^2 s^-2), (m^2 s^-2)')
    fmts = ['%8.4f', '%8d', '%6d', '%9.3f', '%8.4f', '%8.5f',
            '%8.0f', '%10.4e', '%10.4e', '%10.4e']
    data = np.zeros([3*len(keys), 10])
    for e_id, key in enumerate(keys):
        data[3*e_id:3*(e_id+1), 0] = params.grad[key]
        data[3*e_id:3*(e_id+1), 1] = params.gap[key]
        for rep in range(3):
            data[3*e_id + rep, 2] = rep
            data[3*e_id + rep, 3:] = analyse_exp(key, rep)
    np.savetxt(results_filename, data,
               header=header, comments='', delimiter=', ', fmt=fmts)

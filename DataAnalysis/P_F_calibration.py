"""
Python3 module to calibrate measurements of P_F
(the power spectrum of the basal force applied by the flow)

Functions:

hertz_ode(s, t):
    Function for non-dimensional Hertzian time-evolution

theoretical_predictions():
    Calculate the theoretical energy spectral density imparted by an impact
    
calculate_noise_ESD():
    Calculate the energy spectral density associated with accelerometer noise

plot_example():
    Plot an example of post-impact growth & decay of the energy spectral density

analyse_impact(rep, ESD_noise, publication):
    Analyse the accelerations measured after an impact and produce plots

analyse_impacts():
    Analyse all the impacts and save summary statistics of ESD_0 and k

calibration(f, P, fM, alpha):
    Prefactor converting total plate energy spectral density to measured values
    Input: numpy array f of frequencies
           float P for the proportion of plate energy in vertical motion
           float fM for accelerometer low-pass corner frequency
           float alpha for accelerometer low-pass filter exponent
    Output: numpy array of ln(P / (1 + (f / fM)^alpha))

calibration_output():
    Calibrate the values P, fM, alpha that best-fit measurements to theory

calculate_synthetics(f_min, f_max, Q, true_ESD, B=4.0, C=1.0):
    Test analysis methods on synthetic acceleration data
"""

# Imports
from collections import namedtuple
import numpy as np
import matplotlib.pyplot as plt
import pywt
import warnings
from scipy.optimize import curve_fit
from scipy.integrate import odeint

# Ignore by message
warnings.filterwarnings("ignore", message="Mean of empty slice")

# Input and output filenames
folder = '/media/matt/Seagate Expansion Drive/Experiment/Experimental_data/P_F_calibration'
zero_filename = folder + "/zero.txt"
filename = folder + "/calibration_{:02d}.txt"
accel_plotname = folder + "/Decay_fig/impact{:02d}_acceleration.pdf"
ESD_plotname = folder + "/Decay_fig/impact{:02d}_f0={:.1e}Hz_ESD.pdf"
out_filename = folder + "/calibration.txt"

# Set wavelet properties
B, C = 4.0, 1.0
centre_freqs = 2**np.arange(8, 17.5, 0.5)

# Set material parameters (in S.I. units)
#    Bead radius, Young's modulus, density, Poisson's ratio
r_b, E_b, rho_b, nu_b = 1.e-3, 63.e9, 2500, 0.23
#    Plate thickness, length, width
h_p, X, Y = 2.e-3, 0.18, 0.10
#    Plate Young's modulus, density, Poisson's ratio
E_p, rho_p, nu_p = 200e9, 7800, 0.29

# Set impact properties (in S.I. units)
#    Number of impacts
n_i = 25
#    Gravitational acceleration, impact height
g, h0 = 9.81, 1.0

# Set resolution for theoretical calculations
#    Timestep for Hertzian force integration
hertz_dt = 0.01
#    Number of points in Fourier transform
Nfft = 2**10
# Calculate the spectral density of the normal force during an impact
#    Function for non-dimensional Hertzian time-evolution
def hertz_ode(s, t):
    """
    Helper function for the simulation of a nondimensional Hertzian impact
    
    Args:
        s: numpy array for displacement and velocity
        t: float for time
    
    Returns:
        array of velocity and acceleration
    """
    return np.array([s[1], (-s[0])**1.5 if s[0] < 0 else 0])
#    Simulate a non-dimentionalised impact
t = np.arange(0, 5, hertz_dt)
solution = odeint(hertz_ode, np.array([0, -1]), t)
F_t = np.array([((-s)**1.5 if s < 0 else 0) for s in solution[:,0]])
F_f = np.fft.fft(F_t, n=Nfft) * hertz_dt
f = np.arange(Nfft) / (Nfft * hertz_dt)

# Set accelerometer properties
#    Number of acceleros
n_a = 4
#    Max freq. recorded by acceleros
f_max = 2.5e5
#   Sampling period, time prefix, no. of samples pre-impact
dt, t_prefix, id_0 = 4.e-6, 1.e-6, 250
#   Accelerometer sensitivites, pC / (m/s^2)
sensitivity = [0.00675, 0.00561, 0.00538, 0.00658]
#   Conditioner amplification, V / pC
amplification = 0.01 / 0.006

# Set up Gabor wavelets to be used for analysis
wavelet = 'cmor{0:3.1f}-{1:3.1f}'.format(B, C)
scales = np.round(C / (dt * centre_freqs)).astype(int)
centre_freqs = C / (dt * scales)
#  pywt uses discrete, L1 normalisation (\sum_j |\psi(t_j)| = 1),
#  whereas we want continuous, L2 normalisation (\int |\psi(t)|^2 dt = 1),
#  so we require a prefactor wt_pf
wt_pf = (2 * np.pi * B * dt**2)**0.25
wavelet_widths = 2 * centre_freqs / (np.pi * np.sqrt(B))

def theoretical_predictions():
    """
    Calculate the theoretical energy spectral density imparted by an impact
    Assumes:
    - a vertical impact on fixed beads in a hexagonal packing
    - impacting bead position uniformly distributed
    - interaction force entirely Hertzian and normal
    - fixed beads only transmit vertical force to the plate, and then perfectly
    - fixed beads have no effect on plate response
    - plate response perfectly described by Kirchoff-Love theory
    - accelerometers have point locations on the plate
    - accelerometer frequency reponse is perfectly flat
    Calculations are based on the paper's equations (F14) and (F15)

    Returns:
        numpy arrays of the expected ESD and its error
    """
    print('Starting theoretical calculation...')
    #   Generate a length-n array of random impact angles on a hexagonal pack
    angle_i = np.array([])
    while angle_i.size < n_i:
        x = np.random.rand()
        y = np.random.rand()
        # if (2/sqrt(3) x, y) lies inside a unit hexagon...
        if x < (1 - 0.5 * y):
            # ... use the corresponding impact angle
            angle_i = np.append(angle_i,
                                np.arcsin(0.5 * np.sqrt(4./3. * x**2 + y**2))) 
    # Calculate the dimensional impact parameters
    #     Normal impact velocities
    v_i = np.sqrt(2 * g * h0) * np.cos(angle_i)
    #     Effective stiffness, bead mass
    K_i = 2./3. * E_b / (1 - nu_b**2) * np.sqrt(r_b / 2)
    m_b = 4./3. * np.pi * rho_b * r_b**3
    #     Impact length and time scales
    L_i = (m_b * v_i**2 / K_i)**0.4
    T_i = (m_b**2 / (v_i * K_i**2))**0.2
    
    # Calculation of plate response
    #    Bending stiffness
    D = E_p * h_p**3 / (12 * (1 - nu_p**2))
    #   Mean energy per unit area per mode and mode mean spectral density
    E_thy = (np.array([(np.interp(f0 * T_i, f, np.abs(F_f)**2)
                        * (np.cos(angle_i) * m_b * L_i / T_i)**2).mean()
                       for f0 in centre_freqs])
             / (4 * rho_p * h_p * X**2 * Y**2))
    SD_thy = 0.5 * X * Y * np.sqrt(rho_p * h_p / D)
    #   Mean energy spectral density and error, assuming N_modes~Poisson
    ESD_thy_bar = E_thy * SD_thy
    ESD_thy_err = E_thy * np.sqrt(SD_thy / wavelet_widths)
    print('Theoretical calculations finished')
    return ESD_thy_bar, ESD_thy_err

def calculate_noise_ESD():
    """
    Calculate the energy spectral density associated with accelerometer noise
    
    Returns:
        numpy array of the energy at each pre-defined Gabor wavelet
    """
    # Calculate the levels at which data cannot be distinguished from noise
    print('  Calculating noise level')
    #  Read data from an experiment without an impact
    data = np.loadtxt(zero_filename, skiprows=2, delimiter=',')
    #  Initialise the mean spectral density of the acceleration
    U = np.zeros([centre_freqs.size, data.shape[0]])
    #  Calculate wavelet transforms of the measured acceleration
    for a_id in range(n_a):
        acceleration = data[:, 1+a_id] / (sensitivity[a_id] * amplification)
        coeff = wt_pf * pywt.cwt(acceleration, scales, wavelet)[0]
        U += np.abs(coeff)**2 / n_a
        #  Use the 99th percentile as a noise threshhold
        return (rho_p * h_p * np.percentile(U, 99, axis=1)
                / (2 * np.pi * centre_freqs)**2)
    
def analyse_impact(rep, ESD_noise, publication=False):
    """
    Analyse the accelerations measured after an impact
    
    Args:
        rep: integer for impact number
        ESD_noise: numpy array of the noise level
        publication: boolean determining whether plots are publication-standard
    
    Returns:
        numpy array of the energy imparted to the plate at each wavelet
        numpy array of the decay rate of that energy
    """
    #  Analyse an impact to extract energy spectral densities and decay rates
    print('  Analysing impact {}'.format(rep))
    # Read the impact data and calculate the spectral density for each wavelet
    data = np.loadtxt(filename.format(rep), skiprows=2, delimiter=',')
    time = t_prefix * data[:, 0]
    # Initialise the mean spectral density of the acceleration
    U = np.zeros([centre_freqs.size, data.shape[0]])
    for a_id in range(n_a):
        acceleration = data[:, 1+a_id] / (sensitivity[a_id] * amplification)
        plt.plot(time[::25], acceleration[::25], linewidth=0.5)
        coeff = wt_pf * pywt.cwt(acceleration, scales, wavelet)[0]
        U += np.abs(coeff)**2 / n_a
    # Save the plot of the measured accelerations
    plt.xlabel(r'$t$ / s')
    plt.ylabel(r'$a$ / m s$^{-2}$')
    plt.title('Impact {}'.format(rep))
    plt.savefig(accel_plotname.format(rep))
    plt.close()
    
    #  Initialise outputs: the initial spectral density and its decay rate
    ESD_0 = np.nan * np.ones(centre_freqs.size)
    k = np.nan * np.ones(centre_freqs.size)
    # Iterate through wavelets
    for f_id in range(coeff.shape[0]):
        fig = plt.figure()
        ax = fig.add_axes([0.15, 0.15, 0.7, 0.7], yscale='log')
        # Calculate the associated energy spectral density over time
        ESD = rho_p * h_p * U[f_id] / (2 * np.pi * centre_freqs[f_id])**2
        # Identify times at which ESD's decaying value exceeds the noise level
        id_max = np.argmax(ESD[:2*id_0])
        id_end = id_max + np.argmax(ESD[id_max:] < ESD_noise[f_id])
        ax.semilogy(time[:2*id_end], ESD[:2*id_end])
        # Calculate the timescale of the wavelet's decay to the noise level
        n_decay = int(np.sqrt(B) * scales[f_id]
                      * np.sqrt(np.log(ESD[id_max] / ESD_noise[f_id])))
        # If there's a sufficiently long decay distinguishable from noise...
        if (id_max + 2 * n_decay < id_end):
            # then perform a linear regression over this timescale
            id_st = id_max + n_decay
            y_r = np.log(ESD[id_st:id_end])
            X_r = np.vstack([np.ones(id_end - id_st),
                             time[id_st:id_end] - time[id_0]]).T
            b = np.linalg.lstsq(X_r, y_r, rcond=None)[0]
            # If this regression adequately fits the data...
            if ((y_r - X_r.dot(b))**2).max() < np.log(10)**2:
                # then extract the outputs
                ESD_0[f_id] = 2 * np.exp(b[0])
                k[f_id] = -b[1]
                ax.semilogy(time[[id_st, id_end]],
                             np.exp(X_r[[0,-1],:].dot(b)), '--k')
        # Save the plot of the energy spectral density's decay
        if (publication):
            fig.set_size_inches(2.4, 3)
            ax.set_position([0.25, 0.15, 0.7, 0.8])
            label = r'$\hat{\Psi}_{f_0} = \hat{\Psi}^0_{f_0} e^{-k_{f_0}t}$'
            ax.text(1e-2, 1e-11, label, fontsize=8)
            ax.set_xlabel(r'$t$ (s)', fontsize=8)
            ax.set_ylabel(r'$|\hat{\Psi}_{f_0}|^2$ (J m$^{-2}$ Hz$^{-1}$)',
                          fontsize=8)
            ax.tick_params('both', labelsize=8)
            ax.yaxis.set_label_coords(-0.25, 0.5)
        else:
            ax.set_title('Impact {}, $f_0$ = {:.0f} Hz'
                         .format(rep, centre_freqs[f_id]))
            ax.set_xlabel(r'$t$ / s')
            ax.set_ylabel(r'$|\hat{\Psi}_{f_0}|^2$ / J m$^{-2}$ Hz$^{-1}$')
            
        plt.savefig(ESD_plotname.format(rep, centre_freqs[f_id]))
        plt.close()
    return ESD_0, k

def analyse_impacts():
    """Analyse all the impacts and save summary statistics of ESD_0 and k"""
    # Calculate theoretical predictions, for comparison
    ESD_thy_bar, ESD_thy_err = theoretical_predictions()
    print('Starting analysis of measurements')
    # Initialise outputs: the initial spectral density and its decay rate
    ESD_0 = np.nan * np.ones([n_i, centre_freqs.size])
    k = np.nan * np.ones([n_i, centre_freqs.size])
    ESD_noise = calculate_noise_ESD()
    for rep in range(n_i):
        ESD_0[rep], k[rep] = analyse_impact(rep, ESD_noise)
    print('Analysis of measurements finished')
    # Plot results
    plt.loglog(centre_freqs, ESD_thy_bar)
    plt.loglog(np.tile(centre_freqs, (n_i, 1)).T, ESD_0.T, '.')
    plt.xlabel('$f_0$ (Hz)')
    plt.ylabel('$|\hat{\Psi}^0_{f_0}|^2$ (J m$^{-2}$ Hz$^{-1}$)')
    plt.show()
    # Calculate sample means and their standard deviations
    ESD_0_bar = np.nanmean(ESD_0, axis=0)
    ESD_0_err = (np.nanstd(ESD_0, axis=0)
                 / np.sqrt(np.isfinite(ESD_0).sum(axis=0)))
    k_bar = np.nanmean(k, axis=0)
    k_err = (np.nanstd(k, axis=0)
             / np.sqrt(np.isfinite(k).sum(axis=0)))
    # Save results to a text file
    data = np.vstack([centre_freqs,
                      ESD_thy_bar, ESD_thy_err,
                      ESD_0_bar, ESD_0_err,
                      k_bar, k_err]).T

    header = ('f0 / Hz, Energy Spectral Density / J m^-2 Hz^-1        , Decay rate / s^-1 \n'
              '       , thy_mean  , thy_error , obs_mean  , obs_error , mean      , error')
    formats = ' % 8.0f, %.4e, %.4e, %.4e, %.4e, %.4e, %.4e'
    np.savetxt(out_filename, data, header=header, fmt=formats)

def calibration(f, P, fM, alpha):
    """
    Return factor converting plate energy spectral density to measured values

    Args:
        f: numpy array of frequencies at which to calculate conversion factor
        P: float for the proportion of plate energy in vertical motion
        fM: float for accelerometer low-pass corner frequency
        alpha: float for accelerometer low-pass filter exponent

    Returns:
        numpy array of ln(P / (1 + (f / fM)^alpha))
    """
    return np.log(P / (1 + (f / fM)**alpha))

def calibration_output():
    """Calibrate the values P, fM, alpha that best-fit measurements to theory"""
    # Read calibration results from text file
    data_filename = folder + "/calibration.txt"
    data = np.loadtxt(data_filename, skiprows=2, delimiter=',')
    f_0 = data[:, 0]
    ESD_thy_bar = data[:, 1]
    ESD_thy_err = data[:, 2]
    ESD_0_bar = data[:, 3]
    ESD_0_err = data[:, 4]
    k_bar = data[:, 5]
    k_err = data[:, 6]
    # Calculate best-fit P, fM, n for high-frequency accelerometer correction
    id_use = np.nonzero(np.isfinite(ESD_0_bar))[0]
    log_ratio = np.array([np.log((Obs_b + Obs_e * np.random.randn(100))
                                 / (Thy_b + Thy_e * np.random.randn(100)))
                          for Thy_b, Thy_e, Obs_b, Obs_e in data[id_use,1:5]])
    P, fM, a = curve_fit(calibration, f_0[id_use],
                         np.nanmean(log_ratio, axis=1),
                         p0 = (1, 1e5, 4),
                         sigma = np.nanstd(log_ratio, axis=1))[0]
    print('P/\Gamma = {:.2f} / (1 + (f / {:.1e} Hz)**{:.1f}'
          .format(P, fM, a))
    # Calculate quality factor Q
    id_use = np.isfinite(k_bar)
    Q_bar = (2 * np.pi * f_0[id_use] / k_bar[id_use]).mean()
    Q_err = ((2 * np.pi * f_0[id_use] / k_bar[id_use]).std()
             / np.sqrt(id_use.sum()))
    print('Quality factor {:.1f} +/- {:.1f}'.format(Q_bar, Q_err))
    # Interpolate theoretical values of post-impact energy spectral density
    f_calib = 2**np.arange(10, 17.1, 0.1)
    ESD_calib = (np.interp(f_calib, f_0, ESD_thy_bar)
                 * np.exp(calibration(f_calib, P, fM, a)))
    k_label = r'$\langle k_{f_0} \rangle = 2 \pi f_0 / Q$'
    psi_label = r'$\langle \hat{\Psi}^0_{f_0} \rangle = \langle \Psi^0_{f_0} \rangle / \Gamma$'
    # Plot theoretical and measured energy spectral density, and fit to latter
    fig = plt.figure(figsize=[2, 3])
    ax1 = fig.add_axes([0.25, 0.15, 0.7, 0.35])
    ax1.plot(f_0, P * ESD_thy_bar, color='black')
    ax1.fill_between(f_0,
                     P * (ESD_thy_bar - ESD_thy_err),
                     P * (ESD_thy_bar + ESD_thy_err),
                     color='black', alpha=0.25)
    ax1.plot(f_calib, ESD_calib , '--k')
    ax1.errorbar(f_0, ESD_0_bar,
                 xerr=f_0 / (2 * np.pi * np.sqrt(B)),
                 yerr=ESD_0_err,
                 fmt='+', markersize=1)
    ax1.text(8e3, 2e-11, psi_label, fontsize=8)
    ax1.set_xscale('log')
    ax1.set_xlim([1e3, 2e5])
    ax1.set_xlabel(r'$f_0$ (Hz)', fontsize=8)
    ax1.set_yscale('log')
    ax1.set_ylim([2e-12, 2e-9])
    ax1.set_ylabel(r'$\langle \hat{\Psi}^0_{f_0}\rangle$ (J m$^{-2}$ Hz$^{-1}$)',
                   fontsize=8)
    ax1.yaxis.set_label_coords(-0.24, 0.5)
    ax1.tick_params('both', labelsize=8)
    # Plot measured decay rates, and fit with constant quality factor
    ax2 = fig.add_axes([0.25, 0.6, 0.7, 0.35])
    ax2.errorbar(f_0, k_bar,
                 xerr=f_0 / (2 * np.pi * np.sqrt(B)),
                 yerr=k_err,
                 fmt='+', markersize=1)
    ax2.plot(f_0[id_use], 2 * np.pi * f_0[id_use] / Q_bar, '--k')
    ax2.text(4e3, 3e3, k_label, fontsize=8)
    ax2.set_xscale('log')
    ax2.set_xlim([1e3, 2e5])
    ax2.set_yscale('log')
    ax2.set_ylabel(r'$\langle k_{f_0} \rangle$ (s$^{-1}$)',
                   fontsize=8)
    ax2.yaxis.set_label_coords(-0.24, 0.5)
    ax2.tick_params('both', labelsize=8)

    plt.show()

def calculate_synthetics(f_min, f_max, Q, true_ESD, B=4.0, C=1.0):
    """
    Test analysis methods on synthetic acceleration data
    
    Args:
        f_min: float, the minimum frequency at which there's energy content
        f_max: float, the maximum frequency at which there's energy content
        Q: float, the quality factor for the decay of plate energy
        true_ESD(f): function for initial spectral energy density at freq f
        B: float, the normalised bandwidth of wavelets
        C: float, the normalised centre frequency of wavelets
    """
    # Settings for synthetics
    t0, dt, t_max = -0.1, 1.e-4, 2
    n_r, n_s = 4, 4
    # Settings for wavelet analysis
    wavelet = 'cmor{:3.1f}-{:3.1f}'.format(B, C)
    wt_pf = (2 * np.pi * B * dt**2)**0.25
    f0 = np.array([20, 40, 80, 160])
    # Calculate the number of initial points and which scales to use
    n0 = int(-t0 / dt)
    scales = (1 / (f0 * dt)).astype(int)
    # Initialise energy spectral density and decay rate
    ESD_0 = np.zeros([f0.size, n_r])
    k = np.zeros([f0.size, n_r])
    # Iterate over impacts
    for rep in range(n_r):
        # Simulate resonances with average density 1/Hz
        freqs = f_min + (f_max - f_min) * np.random.rand(int(f_max - f_min))
        # Initialise time and mean acceleration spectral density
        t = np.arange(t0, t_max, dt)
        U = np.zeros([f0.size, t.size])
        # Iterate over measurements
        for sample in range(n_s):
            # Initialise synthetic acceleration
            a = np.zeros(t.size, 'complex')
            # Synthesise acceleration as composed of different resonant modes
            for f, phi in zip(freqs, np.random.rand(freqs.size)):
                # ... at frequencies f with initial energies true_ESD(f),
                # ... decaying linearly with timescales Q / 2 pi f
                a[n0:] =  a[n0:] + (np.sqrt(8 * true_ESD(f)) * np.pi * f
                                    * np.sin(2 * np.pi * (f * t[n0:] + phi))
                                    * np.exp(-np.pi * f * t[n0:] / Q))
            # Extract ESD, k as for the true data
            U = U + np.abs(wt_pf * pywt.cwt(a, scales, wavelet)[0])**2 / n_s
        for f_id in range(f0.size):
            ESD = np.abs(U) / (2 * np.pi * f0[f_id])**2
            id_st, id_en = n0 + 5*scales[f_id], t.size - 5*scales[f_id]
            y = np.log(ESD[f_id, id_st:id_en])
            X = np.vstack([np.ones(id_en - id_st), t[id_st:id_en]]).T
            b = np.linalg.lstsq(X, y, rcond=None)[0]
            ESD_0[f_id, rep] = 2 * np.exp(b[0])
            k[f_id, rep] = -b[1]
            if (rep == 0):
                plt.semilogy(t[::50], ESD[f_id,::50])
                plt.semilogy(t[id_st:id_en], np.exp(X.dot(b)))
                plt.xlabel(r'$t$ (s)')
                plt.ylabel(r'$\Psi_{f_0}$ (J m$^{-2}$ s)')
                plt.title(r'$f_0$ = {:.0f} Hz'.format(f0[f_id]))
                plt.show()                        
    plt.loglog(np.tile(f0, (n_r, 1)).T, ESD_0, '.')
    plt.xlabel(r'$f_0$ (Hz)')
    plt.ylabel(r'$\Psi^0_{f_0}$ (J m$^{-2}$ s)')
    plt.show()
    print(ESD_0.mean(axis=1))
    print(2 * np.pi * f0 / k.mean(axis=1))

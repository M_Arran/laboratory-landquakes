"""
Python3 module to combine and analyse results
Used to plot Figures 5, 6, 7, 8, 9; and to produce Table 2

Functions:

hertz_ode(s, t):
    Calculate kinematic derivatives for a non-dimensionalised Hertzian impact

zeta(f0):
    Calculate the normalised spectral density of Hertzian impact force

Kean_model(K=1, fit=False):
    Calculates the predictions for P_F^0 of Kean et al. (2015)'s model

Lai_model():
    Calculates the predictions for P_F^0 of Lai et al. (2018)'s model

Farin_fz2(du):
    Farin et al. (2019)'s P_F^0 prefactor, accounting for impact geometry

Farin_factors(du=1):
    Calculates prefactors for P_F^0 and f_c, via impact geometry simulations

Farin_thin_model(du=1, e=0.9, fit=False):
    Calculates predictions for P_F^0, f_c of Farin (2019)'s 'thin-flow' model

Farin_thick_model(chi=1.25, du=1, e=0.9, fit=False):
    Calculates predictions for P_F^0, f_c of Farin (2019)'s 'thick-flow' model

Bachelet_P_F0(gamma=0.01):
    Calculates predictions for P_F^0 of Bachelet (2018)'s model

Bachelet_model(gamma=0.01, fit=False):
    Calculates predictions for P_F^0 and f_c of Bachelet (2018)'s model

model_comparison():
    Compares the five models' best-fit predictions for P_F^0 and f_c

plot_profiles(plot_index='g0.52_h28_0'):
    Plots base-normal profiles of flow properties measured at the channel wall

plot_model_discrepancies():
    Produces plots of unexpected experimental observations

plot_I_hat():
    Produces plots of the bulk inertial number against dimensionless parameters

plot_v2_bar():
    Produces plots demonstrating the importance of the Green's function

Bachelet_variant_P_F0(U=lambda index: 1, gamma=lambda f, h, z: 0.01):
    Calculates predictions for P_F^0 for a variant of Bachelet (2018)'s model

Bachelet_variant_model(U=lambda index: 1, gamma=lambda f, h, z: 0.01):
    Calculates predictions for P_F^0, f_c for a variant of Bachelet (2018)

fit_Bachelet_variant(U_mode='unit', gamma_mode='constant'):
    Fit free parameters of a Bachelet (2018) model variant and plot results

Bagnold_Bachelet_P_F0(T0=1, gamma=0.01):
    Calculates predictions for P_F^0 for a Bagnold profile and Bachelet impacts

Bagnold_Bachelet_model(T0=1, gamma=0.01):
    Calculates predictions for P_F^0 and f_c for a Bagnold + Bachelet model

fit_Bagnold_Bachelet():
    Calculates the best-fit gamma for the above model and plots the results
"""

## Imports
import numpy as np
import pandas as pd
from scipy.integrate import odeint
from scipy.optimize import least_squares
from statsmodels.api import OLS
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter, NullFormatter

#plt.rc('text', usetex=True)

## Material constants (SI units)
# Gravitational acceleration (m s^-2) 
g = 9.81
# Particle density (kg m^-3), diameter (m)
rho, d = 2500, 2e-3
# Particle Young's modulus (kg m^-1 s^-2), Poisson's ratio
E, nu = 63e9, 0.23
# Plate length (m), width (m), thickness (m), channel width (m)
X, Y, h_p, W = 0.18, 0.1, 2e-3, 0.2
# Plate Young's modulus (kg m^-1 s^-2), Poisson's ratio, density (kg m^-3)
E_p, nu_p, rho_p = 200e9, 0.29, 7800

## Experiments for which the flow was in the transitional regime
transitional_regime_keys = np.array(['g0.46_h10', 'g0.48_h14', 'g0.50_h20',
                                     'g0.52_h28', 'g0.54_h40'])

## Locations of input and output files
folder = '/media/matt/Seagate Expansion Drive/Experiment_results'
lab_data_filename = folder + '/Lab_analysis/results.txt'
profile_filename = folder + '/PTV_analysis/profiles/{:s}.txt'
plot_filename = folder + '/Plots/model_comparison.pdf'

## Read results of picoscope and mass balance analysis, PTV analysis
data = pd.read_csv(lab_data_filename, skiprows=[1], sep='\s*,\s*',
                   engine='python')
data.index = ['g{:.2f}_h{:02d}_{:d}'
              .format(exp['Gradient'], int(exp['Gap']), int(exp['Repeat']))
              for _, exp in data.iterrows()]
# Read profiles from PTV analysis
profiles = [pd.read_csv(profile_filename.format(index),
                        skiprows=[1], sep='\s*,\s*', engine='python')
            for index in data.index]
# Calculate flow depth, flux, and mean velocity at the channel wall
data['h'] = np.array([pf.z[np.argmax(pf.phi)
                           + np.argmax(pf.phi[np.argmax(pf.phi):]
                                       < 0.5 * pf.phi.max())]
                      for pf in profiles]) * d
data['Q_wall'] = np.array([np.sum((pf.phi * pf.u)[:-1] * np.diff(pf.z))
                           for pf in profiles]) * np.sqrt(g * d) * d
data['u_wall'] = np.array([np.sum(pf.phi * pf.u) / np.sum(pf.phi)
                           for pf in profiles]) * np.sqrt(g * d)
## Alternative definition of flow depth h
# data.h = np.array([pf.z[np.argmax(np.cumsum(pf.phi) > 0.95 * pf.phi.sum())]
#                    for pf in profiles]) * d
# Calculate channel inclination, mean volume fraction, mean velocity
data['th'] = np.arctan(data.Gradient)
data['phi'] = data.m / (rho * data.h)
data['u_bar'] = data.q / data.m
# Label experiments in the transitional and dense regimes
data['trans'] = [index[:-2] in transitional_regime_keys
                 for index in data.index]
data['dense'] = np.logical_not(data.trans)

## Calculate results from Hertz theory
#   Hertz modulus, bead mass, corner frequency
k = 1./3. * E / (1 - nu**2) * np.sqrt(d)
m = 1./6. * np.pi * rho * d**3
hertz_tau = lambda u_n: (m**2 / (k**2 * u_n))**0.2
#   Non-dimensional power spectral density of normal force
#       Timestep for Hertzian force integration
hertz_dt = 0.002
#       Number of points in Fourier transform
hertz_nfft = 2**20
#       Calculate the normal force throughout a non-dimensionalised impact
def hertz_ode(s, t):
    """
    Helper function for the simulation of a nondimensional Hertzian impact

    Args:
        s: numpy array for displacement and velocity
        t: float for time

    Returns:
        numpy array of velocity and acceleration
    """
    return np.array([s[1], (-s[0])**1.5 if s[0] < 0 else 0])
#       Calculate the force's spectral density
t = np.arange(0, 5, hertz_dt)
solution = odeint(hertz_ode, np.array([0, -1]), t)
F_t = np.array([((-s)**1.5 if s < 0 else 0) for s in solution[:,0]])
f = np.arange(hertz_nfft) / (hertz_nfft * hertz_dt)
P_F = np.abs(np.fft.fft(F_t, n=hertz_nfft) * hertz_dt)**2
#       Non-dimensional function for spectral density
def zeta(f0):
    """
    Simulates a nondimensional Hertzian impact, calculating normal force F
    Calculates zeta = |\\tilde{F}|^2/4, for Fourier transform \\tilde{}

    Args:
        f0: array of frequencies at which to return zeta(f)
    Returns:
        the interpolated values of zeta
    """ 
    return np.interp(f0, f, P_F / 4)
#       Non-dimensional corner frequency
nondim_f_c = 0.208

## Functions to calculate predictions of existing models
def Kean_model(K=1, fit=False):
    """
    Calculates the predictions for P_F^0 of Kean et al. (2015)'s model.
    Uses model of Tsai et al. (2012) with u ~ u(h) (so ~ u_bar), Delta p ~ F_z

    Args:
        K: float for the model prefactor, used if fit=False
        fit: boolean for whether to calculate the best-fit prefactor

    Returns:
        predicted values of P_F^0
    """
    pP_F0 = X * Y * data.u_bar / d**3 * (data.m * g * np.cos(data.th))**2
    if (fit == True):
        # Calculate log least squares fit for free parameter K
        print('Fitting Kean (2015) model:')
        K = np.exp(np.log(data.P_F0 / pP_F0).mean())
        print('  Best fit: prefactor K = {:.1e} m^4 s^2'.format(K))
    # Return predictions for P_F0
    output = pd.DataFrame({'P_F0': K * pP_F0}, index=data.index)
    return output

def Lai_model():
    """
    Calculates the predictions for P_F^0 of Lai et al. (2018)'s model.
    Uses model of Tsai et al. (2012) with u = u_bar, Delta p = 2 m u_bar

    Returns: predicted values of P_F^0
    """
    P_F0 = 4 * X * Y * m**2 * data.u_bar**3 / d**3
    f_c = nondim_f_c / hertz_tau(data.u_bar)
    output = pd.DataFrame({'P_F0': P_F0, 'f_c': f_c}, index=data.index)
    # Return predictions for P_F0 and f_c
    return output

def Farin_fz2(du):
    """
    Farin et al. (2019)'s P_F^0 prefactor, accounting for impact geometry

    Args:
        du: float for the normalised standard deviation of impact velocities

    Returns:
        0.23^2 * (1 + du^2 / 0.18)
    """
    return 0.23**2 * (1 + du**2 / 0.18)

def Farin_factors(du=1):
    """
    Calculates P_F^0 and f_c prefactors in Farin et al. (2019)'s model extension
    Simulates Farin et al. (2019)'s impact geometry, with Hertzian collisions
    
    Args:
        du: float for the normalised standard deviation of impact velocities

    Returns:
        array of the nondimensional prefactors for P_F^0 and f_c
    """
    # Simulate N impacts, to calc. P_F at non-dim frequencies freq_hat
    N, freq_hat = 10000, np.linspace(0, nondim_f_c * (1 + du)**0.2, 200)
    # Polar and azimuthal angles for impact velocities and impact locations
    alpha = np.pi/6 * np.random.random(N)
    psi = np.pi * np.random.random(N)
    alphaT = 2 * np.pi * np.random.random(N)
    psiT = 2 * np.pi * np.random.random(N)
    # Normal impact velocities
    un = ((1 + du * np.sin(psi) * np.cos(psiT))
          * np.sin(alpha) * np.cos(alphaT)
          + du * np.sin(psi) * np.sin(psiT) * np.sin(alpha) * np.sin(alphaT)
          + du * np.cos(psi) * np.cos(alpha))
    # Squared vertical impulses
    Iz2 = (un * np.cos(alpha))**2
    # Mean power spectrum of vertical force
    P_F_hat = np.array([((Iz2 * zeta(f_hat * np.abs(un)**-0.2)
                          * np.sin(alpha) * np.sin(psi))[un > 0].sum()
                         / (np.sin(alpha) * np.sin(psi))[un > 0].sum())
                        for f_hat in freq_hat])
    # Calculate the implied non-dimensional prefactors for P_F0 and f_c
    id_c = np.argmax(P_F_hat < P_F_hat[:np.argmax(P_F_hat)+1].mean() / 2)
    P_F0_hat = np.nanmean(P_F_hat[:int(id_c // 2 + 1)])
    f_c_hat = freq_hat[id_c]
    return np.array([P_F0_hat, f_c_hat])

def Farin_thin_model(du=1, e=0.9, fit=False):
    """
    Calculates predictions for P_F^0, f_c of Farin (2019)'s 'thin-flow' model
    Uses model of Tsai et al. (2012) with:
     u = u_bar, Delta p = (1 + e) * m * u * fz(du)
    
    Args:
        du: float for the normalised standard deviation of impact velocities
        e: float for impacts' coefficient of restitution
        fit: boolean for whether to calculate the best-fit value of du

    Returns: predicted values of P_F^0 and f_c
    """
    pP_F0 = X * Y * data.phi * (1 + e)**2 * m**2 * data.u_bar**3 / d**3
    pf_c = 1 / hertz_tau(data.u_bar)
    # log ratio between P_F0 data and predictions for prefactor equal to 1
    logA = np.log(data.P_F0 / pP_F0)
    if (fit == True):
        # Calculate log least squares fit of P_F(0) for du
        print('Fitting Farin (2019) thin-flow model:')
        du = least_squares(lambda du:np.log(Farin_fz2(du)) - logA, 1,
                           method='lm', xtol=5e-2).x[0]
        print('  Best fit for P_F0: du/u_x = {:.1e}'.format(du))
    # Calculate the corresponding prefactors for P_F0 and f_c
    P_F0_hat, f_c_hat = Farin_factors(du)
    # Return predictions for P_F0 and f_c
    output = pd.DataFrame({'P_F0': P_F0_hat * pP_F0, 'f_c': f_c_hat * pf_c},
                          index=data.index)
    return output

def Farin_thick_model(chi=1.25, du=1, e=0.9, fit=False):
    """
    Calculates predictions for P_F^0, f_c of Farin (2019)'s 'thick-flow' model
    Uses model of Tsai et al. (2012), with:
      u = p u d / h, Delta p = (1 + e) * m * u * fz(du)
    
    Args:
        chi: float for the velocity profile's shape factor (1.25 is Bagnoldian)
        du: float for the normalised standard deviation of impact velocities
        e: float for impacts' coefficient of restitution
        fit: boolean for whether to calculate the best-fit value of du

    Returns:
        predicted values of P_F^0 and f_c
    """
    pP_F0 = X*Y * data.phi * (1 + e)**2 * m**2 * (chi * data.u_bar / data.h)**3
    pf_c = 1 / hertz_tau(data.u_bar)
    # log ratio between P_F0 data and predictions for prefactor equal to 1
    logA = np.log(data.P_F0 / pP_F0)
    if (fit == True):
        # Calculate log least squares fit of P_F(0) for du
        print('Fitting Farin (2019) thick-flow model:')
        du = least_squares(lambda du:np.log(Farin_fz2(du)) - logA, 1,
                           method='lm', xtol=5e-2).x[0]
        print('  Best fit for P_F0: du/u_x = {:.1e}'.format(du))
    # Calculate the corresponding prefactors for P_F0 and f_c
    P_F0_hat, f_c_hat = Farin_factors(du)
    # Return predictions for P_F0 and f_c
    output = pd.DataFrame({'P_F0': P_F0_hat * pP_F0, 'f_c': f_c_hat * pf_c},
                          index=data.index)
    return output

def Bachelet_P_F0(gamma=0.01):
    """
    Calculates predictions for P_F^0 of Bachelet (2018)'s model.
    Within Tsai (2012)'s framework, with contributions from throughout the flow,
    uses n_I = 4 phi du/dz / pi d^3, Delta p = 2 * m * sqrt(T), and assumes
    exponential attenuation exp(-gamma z)
    
    Args:
        gamma: float for the constant of squared impulse attenuation with height

    Returns:
        predicted values of P_F^0
    """
    # Profiles are nondimensionalised by velocity g/d, so we add this here
    n_i0 = 4 * np.sqrt(g / d) / (np.pi * d**3)
    Dp20 = (2 * m * np.sqrt(g * d))**2
    output = pd.DataFrame(columns=['P_F0'])
    # For each experiment's index, volume fraction, and channel-wall profiles,
    for index, phi, prof in zip(data.index, data.phi, profiles):
        # ... find the index of the surface
        id_m = np.argmax(prof.phi)
        id_s = id_m + np.argmax(prof.phi[id_m:] < prof.phi.max() / 2)
        # ... and integrate contributions with depth
        P_F0 = (X * Y * d * n_i0 * Dp20 * phi
                * (-prof.u.diff(periods=-1)[:id_s] * prof['T'][:id_s]
                   * np.exp(-gamma * d * prof.z[:id_s])).sum())
        output = output.append(pd.Series({'P_F0': P_F0},
                                         name=index))
    # Return predictions for P_F0
    return output

def Bachelet_model(gamma=0.01, fit=False):
    """
    Calculates predictions for P_F^0 and f_c of Bachelet (2018)'s model
    Within Tsai (2012)'s framework, with contributions from throughout the flow,
    uses n_I = 4 phi du/dz / pi d^3, Delta p = 2 * m * sqrt(T), and assumes
    exponential attenuation exp(-gamma z)

    Args:
        gamma: float for the constant of squared impulse attenuation with height
        fit: boolean for whether to calculate the best-fit value of gamma

    Returns:
        predicted values of P_F^0 and f_c
    """
    n_i0 = 4 * np.sqrt(g / d) / (np.pi * d**3)
    Dp20 = (2 * m * np.sqrt(g * d))**2
    tau0 = (np.pi**2 * rho**2 * (1 - nu**2)**2
            / ((4 * E**2) * np.sqrt(g * d)))**0.2 * d
    if (fit == True):
        # Calculate log least squares fit of P_F(0) for gamma
        print('Fitting Bachelet (2019) model, with gamma')
        gamma = least_squares(lambda gamma: np.log(Bachelet_P_F0(gamma)
                                                   .P_F0 / data.P_F0),
                              x0=0.01, bounds=(0, np.inf), xtol=5e-2).x[0]
        print('  Best fit: gamma = {:.1e} m^-1'.format(gamma))
    # Calculate the corresponding predictions for P_F0 and f_c, by,
    freq = np.linspace(0, 2 * nondim_f_c / tau0, 200)
    output = pd.DataFrame(columns=['P_F0', 'f_c'])
    # for each experiment's index, volume fraction, and channel-wall profiles,
    for index, phi, prof in zip(data.index, data.phi, profiles):
        # ... finding the index of the surface,
        id_m = np.argmax(prof.phi)
        id_s = id_m + np.argmax(prof.phi[id_m:] < prof.phi.max() / 2)
        # ... integrating contributions with depth at each frequency,
        P_F = (X * Y * d * n_i0 * Dp20 * phi
               * np.array([(-prof.u.diff(periods=-1)[:id_s] * prof['T'][:id_s]
                            * np.exp(-gamma * d * prof.z[:id_s])
                            * zeta(f * tau0 * prof['T'][:id_s]**-0.1)).sum()
                           for f in freq]))
        # ... and extracting corner frequency f_c, power spectral density P_F0
        id_c = np.argmax(P_F < P_F[:np.argmax(P_F)+1].mean() / 2)
        P_F0 = P_F[:int(id_c // 2)].mean()
        f_c = freq[id_c]
        output = output.append(pd.Series({'P_F0': P_F0, 'f_c': f_c},
                                         name=index))
    # Return predictions for P_F0 and f_c
    return output

def model_comparison():
    """
    Compares the five models' best-fit predictions for P_F^0 and f_c
    Fits all models' free parameters, produces plots comparing predictions,
    and calculates the Akaike Information Criterion for the models for P_F^0
    """
    # Model names
    models = ["Kean et al. (2015)", "Lai et al. (2018)",
              "Farin et al. (2019): 'thick-flow'",
              "Farin et al. (2019): 'thin-flow'",
              "Bachelet et al."]
    fc_models = [None, None,
                 "'Thick-flow' extension",
                 "'Thin-flow' extension",
                 "Bachelet et al."]
    # Number of fitting parametes
    n_param = [1, 0, 1, 1, 1]
    # Axes positions within figure
    pos = [[0.099, 0.74],
           [0.099, 0.41],
           [0.432, 0.74],
           [0.432, 0.41],
           [0.432, 0.08]]
    # Markers within figure
    markers = ['s', 'o', 'v', '^', 'D']
    # Extract predictions, using above functions
    predictions = [Kean_model(fit=True),
                   Lai_model(),
                   Farin_thick_model(chi=1.25, e=0.9, fit=True),
                   Farin_thin_model(e=0.9, fit=True),
                   Bachelet_model(fit=True)]
    # Calculate geometric standard error
    epsilon = [np.exp(np.sqrt(np.mean(np.log(data.P_F0 / pred.P_F0)**2)))
               for pred in predictions]
    print('Geometric standard errors:')
    print(epsilon)
    # Calculate Akaike Information criterion (up to a constant)
    AIC = np.array([2 * k + data.shape[0]
                    * (1 + np.log(2 * np.pi * np.mean(np.log(data.P_F0
                                                             / pred.P_F0)**2)))
                    for k, pred in zip(n_param, predictions)])
    print('Akaike information criteria:')
    print(AIC)
    # Calculate the implied probabilities of minimising information loss
    rel_p_min = np.exp(0.5 * (AIC.min() - AIC))
    print('Akaike Information Criterion minimised by the {} model'
          .format(models[np.argmin(AIC)]))
    print('Relative probabilities of minimising information loss:')
    print(rel_p_min)
    # Set up figure comparing models
    obs_PF0_range = [0.5 * data.P_F0.min(), 2 * data.P_F0.max()]
    obs_fc_range = [0.9 * data.f_c.min(), 1.1 * data.f_c.max()]
    fig = plt.figure(figsize=[6, 6])
    # Iterate over the models
    for m_id in range(len(models)):
        # Set up new axes for the power spectral density P_F0
        thy_PF0_range = [0.5 * predictions[m_id].P_F0.min(),
                         2 * predictions[m_id].P_F0.max()]
        PF0_range = [min(obs_PF0_range[0], thy_PF0_range[0]),
                     max(obs_PF0_range[1], thy_PF0_range[1])]
        ax1 = fig.add_axes([pos[m_id][0], pos[m_id][1], 0.23, 0.23],
                           xscale='log', yscale='log',
                           xlim=PF0_range, ylim=PF0_range)
        # Plot perfect agreement for P_F0
        ax1.plot(PF0_range, PF0_range, 'k', color=[0.5, 0.5, 0.5])
        # Plot predictions for P_F0 for dense flows against observations
        sc_dense = ax1.scatter(predictions[m_id].P_F0[data.dense],
                               data.P_F0[data.dense],
                               marker=markers[m_id], c=data.q[data.dense],
                               s=16, cmap='plasma', vmin=0, vmax=30)
        # Plot predictions for transitional-regime flows against observations
        sc_trans = ax1.scatter(predictions[m_id].P_F0[data.trans],
                               data.P_F0[data.trans],
                               marker=markers[m_id], c=data.q[data.trans],
                               s=16, cmap='plasma', vmin=0, vmax=30)
        sc_trans.set_facecolor("none")
        # Label axes
        ax1.set_xlabel(r'$\hat{P}_F^0$ (N$^2$ s)', fontsize=8)
        ax1.xaxis.set_label_coords(0.5, -0.17)
        ax1.set_ylabel(r'$P_F^0$ (N$^2$ s)', fontsize=8)
        ax1.yaxis.set_label_coords(-0.25, 0.5)
        ax1.tick_params(axis='both', labelsize=8)
        # Add a title,
        fig.text(pos[m_id][0], pos[m_id][1]+0.24,
                 models[m_id], fontsize=8)
        # If the model makes a prediction for f_c, ...
        if (fc_models[m_id]):
            # set up new axes for the corner frequency f_c,
            thy_fc_range = [0.9 * predictions[m_id].f_c.min(),
                            1.11 * predictions[m_id].f_c.max()]
            fc_range = [1e-3 * min(obs_fc_range[0], thy_fc_range[0]),
                        1e-3 * max(obs_fc_range[1], thy_fc_range[1])]
            ax2 = fig.add_axes([pos[m_id][0]+0.33, pos[m_id][1], 0.23, 0.23],
                               xscale='log', yscale='log',
                               xticks=[40, 60, 80, 100, 120],
                               yticks=[40, 60, 80, 100, 120],
                               xlim=fc_range, ylim=fc_range)
            # plot perfect agreement for f_c,
            ax2.plot(fc_range, fc_range, 'k', color=[0.5, 0.5, 0.5])
            # plot predictions for f_c for dense flows against observations,
            sc_dense = ax2.scatter(1e-3 * predictions[m_id].f_c[data.dense],
                                   1e-3 * data.f_c[data.dense],
                                   marker=markers[m_id], c=data.q[data.dense],
                                   s=16, cmap='plasma', vmin=0, vmax=30)
            # plot predictions for transitional-regime flows,
            sc_trans = ax2.scatter(1e-3 * predictions[m_id].f_c[data.trans],
                                   1e-3 * data.f_c[data.trans],
                                   marker=markers[m_id], c=data.q[data.trans],
                                   s=16, cmap='plasma', vmin=0, vmax=30)
            sc_trans.set_facecolor("none")
            # label axes,
            ax2.set_xlabel(r'$\hat{f}_c$ (kHz)', fontsize=8)
            ax2.xaxis.set_label_coords(0.5, -0.17)
            ax2.xaxis.set_minor_formatter(NullFormatter())
            ax2.xaxis.set_major_formatter(ScalarFormatter())
            ax2.set_ylabel(r'$f_c$ (kHz)', fontsize=8)
            ax2.yaxis.set_label_coords(-0.25, 0.5)
            ax2.yaxis.set_minor_formatter(NullFormatter())
            ax2.yaxis.set_major_formatter(ScalarFormatter())
            ax2.tick_params(axis='both', which='both', labelsize=8)
            # and add a title,
            fig.text(pos[m_id][0]+0.33, pos[m_id][1]+0.24,
                     fc_models[m_id], fontsize=8)
    # Add a colorbar
    ax_c = fig.add_axes([0.099, 0.08, 0.02, 0.23])
    fig.colorbar(sc_dense, ax_c)
    ax_c.set_ylabel(r'$q$ (kg m$^{-1}$ s$^{-1}$)', fontsize=8)
    ax_c.yaxis.set_label_coords(4, 0.5)
    ax_c.tick_params(labelsize=8)
    #fig.savefig(plot_filename)
    plt.show()
    plt.close()    
    
def plot_profiles(index='g0.52_h28_0'):
    """
    Plots base-normal profiles of flow properties measured at the channel wall

    Args:
         index: experiment id, format 'g{incline:4.2f}_h{h_g:02d}_{repeat:d}'
    """
    # Identify the experiment for which to plot profiles
    id_plot = data.index.get_loc(index)
    # Identify the position of the flow surface and appropriate axis limits
    z_max = 1.2 * data.h[id_plot] / d
    id_s = np.argmax(profiles[id_plot].z >= 0.5)
    id_zm = np.argmax(profiles[id_plot].z > z_max)
    phi_max = 1.2 * profiles[id_plot].phi[:id_zm].max()
    u_max = 1.2 * profiles[id_plot].u[:id_zm].max()
    T_max = 1.2 * np.sqrt(profiles[id_plot]['T'][:id_zm].max())
    fig = plt.figure(figsize=[4, 2])
    # Plot the relative volume fraction profile measured at the channel wall
    ax_phi = fig.add_axes([0.14, 0.2, 0.26, 0.75],
                          xlim=[0, phi_max], ylim=[0, z_max])
    #                      xticks=[0, 0.3, 0.6],
    ax_phi.plot(profiles[id_plot].phi[id_s:], profiles[id_plot].z[id_s:])
    ax_phi.plot([0, phi_max], np.ones(2) * data.h[id_plot] / d, '--k')
    ax_phi.set_xlabel(r'$\phi_w$', fontsize=8)
    ax_phi.set_ylabel(r'$z / d$', fontsize=8)
    ax_phi.xaxis.set_label_coords(0.5, -0.15)
    ax_phi.tick_params(axis='both', which='both', labelsize=8)
    # Plot the downslope velocity profile measured at the channel wall
    ax_u = fig.add_axes([0.43, 0.2, 0.26, 0.75],
                        yticklabels=[],
                        xlim=[0, u_max], ylim=[0, z_max])
    #                    xticks=[0, 2, 4, 6],
    ax_u.plot(profiles[id_plot].u[id_s:], profiles[id_plot].z[id_s:])
    ax_u.plot([0, u_max], np.ones(2) * data.h[id_plot] / d, '--k')
    ax_u.set_xlabel(r'$u_w / \sqrt{gd}$', fontsize=8)
    ax_u.xaxis.set_label_coords(0.5, -0.15)
    ax_u.tick_params(axis='both', which='both', labelsize=8)
    # Plot the granular temperature profile measured at the channel wall
    ax_T = fig.add_axes([0.72, 0.2, 0.26, 0.75],
                        yticklabels=[],
                        xlim=[0, T_max], ylim=[0, z_max])
    #                    xticks=[0, 0.5, 1, 1.5],
    ax_T.plot(np.sqrt(profiles[id_plot]['T'][id_s:]),
              profiles[id_plot].z[id_s:])
    ax_T.plot([0, T_max], np.ones(2) * data.h[id_plot] / d, '--k')
    ax_T.set_xlabel(r'$\sqrt{T_w / gd}$', fontsize=8)
    ax_T.xaxis.set_label_coords(0.5, -0.15)
    ax_T.tick_params(axis='both', which='both', labelsize=8)
    # fig.savefig('.//PTV_analysis//Plots//{}.png'.format(index))
    # plt.close()
    plt.show()

def plot_model_discrepancies():
    """
    Produces plots of unexpected experimental observations.
    Plots experiments' channel inclines against their effective frictions,
    and the mean velocities measured at the channel wall vs. those of the bulk
    """
    g_range = [0.41, 0.55]
    mu_range = [0.3, 0.5]
    fig = plt.figure(figsize=[4.5, 2])
    # Set up axes of channel incline against measured effective friction
    ax_g = fig.add_axes([0.125, 0.2, 0.3, 0.6], xticks=[0.42, 0.46, 0.5, 0.54],
                        xlim=g_range, ylim=mu_range)
    # Plot dense flows
    sc = ax_g.scatter(data.Gradient[data.dense], data.mu[data.dense],
                      c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30,
                      s=16, marker='o')
    # Plot transitional-regime flows
    sc_t = ax_g.scatter(data.Gradient[data.trans], data.mu[data.trans],
                        c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30,
                        s=16, marker='o')
    sc_t.set_facecolor("none")
    # Plot the condition for uniform, steady-state flow
    ax_g.plot(g_range, g_range, '-', color=[0.5, 0.5, 0.5])
    # Label axes with channel incline
    ax_g.set_xlabel(r'$\tan \theta$', fontsize=8)
    ax_g.set_ylabel(r'$\mu$', fontsize=8)
    ax_g.tick_params(axis='both', which='both', labelsize=8)
    # Label axes with channel angle of inclination
    ax_th = ax_g.twiny()
    ax_th.set_xlim(180 / np.pi * np.arctan(g_range))
    ax_th.set_xlabel(r'$\theta$ ($\degree$)', fontsize=8)
    ax_th.tick_params('both', labelsize=8)
    # Calculate the mean flow velocity measured at the channel wall
    u_w_range = np.array([0.8 * data.u_wall.min(), 1.25 * data.u_wall.max()])
    u_range = np.array([0.8 * data.u_bar.min(), 1.25 * data.u_bar.max()])
    # Set up axes of mean velocity at the wall against mean velocity in the bulk
    ax_u = fig.add_axes([0.57, 0.2, 0.3, 0.675], xscale='log', yscale='log',
                        xlim=u_w_range/np.sqrt(g*d), ylim=u_range/np.sqrt(g*d))
    # Plot dense flows
    sc = ax_u.scatter(data.u_wall[data.dense] / np.sqrt(g * d),
                      data.u_bar[data.dense] / np.sqrt(g * d),
                      c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30,
                      s=16, marker='o')
    # Plot transitional-regime flows
    sc_t = ax_u.scatter(data.u_wall[data.trans] / np.sqrt(g * d),
                        data.u_bar[data.trans] / np.sqrt(g * d),
                        c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30,
                        s=16, marker='o')
    sc_t.set_facecolor("none")
    # Label axes
    ax_u.set_xlabel(r'$\bar{u}_w / \sqrt{g d}$', fontsize=8)
    ax_u.xaxis.set_label_coords(0.5, -0.175)
    ax_u.xaxis.set_minor_formatter(NullFormatter())
    ax_u.set_ylabel(r'$\bar{u} / \sqrt{g d}$', fontsize=8)
    ax_u.tick_params(axis='both', which='both', labelsize=8)
    # Add a colorbar
    ax_c = fig.add_axes([0.875, 0.2, 0.025, 0.6])
    fig.colorbar(sc, ax_c)
    ax_c.set_ylabel(r'$q$ (kg m$^{-1}$ s$^{-1}$)', fontsize=8)
    ax_c.tick_params('both', labelsize=8)
    plt.show()

def plot_I_hat():
    """
    Produces plots of the bulk inertial number against dimensionless parameters
    Calculates inertial number estimates from each flow's bulk properties
    & plots them against channel incline and the fluctuating/mean force ratio
    """
    fig = plt.figure(figsize=[4.5, 1.8])
    # Estimate flows' inertial numbers I from bulk flow properties
    I = (2.5 * d * data.u_bar
         / (data.h * np.sqrt(data.phi * g * np.cos(data.th) * data.h)))
    # Set up axes of I against channel inclination
    ax_g = fig.add_axes([0.125, 0.225, 0.3, 0.75], xlim=[6e-2, 6], xscale='log',
                        ylim=[0.41, 0.55], yticks=[0.42, 0.46, 0.5, 0.54])
    # Plot dense flows
    ax_g.scatter(I[data.dense], data.Gradient[data.dense],
                 c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30,
                 s=16, marker='o')
    # Plot transitional-regime flows
    sc_t = ax_g.scatter(I[data.trans], data.Gradient[data.trans],
                        c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30,
                        s=16, marker='o')
    sc_t.set_facecolor("none")
    # Label axes
    ax_g.set_xlabel(r'$\hat{I}$', fontsize=8)
    ax_g.xaxis.set_label_coords(0.5, -0.175)
    ax_g.set_ylabel(r'$\tan \theta$', fontsize=8)
    ax_g.tick_params(axis='both', which='both', labelsize=8)
    # Calculate ratio DF2 of mean square fluctuating force to squared mean force
    DF2 = (2 * data.P_F0 * data.f_c
           / (X * Y * data.m * g * np.cos(data.th))**2)
    # Set up axes of Iagainst DF2
    ax_dF = fig.add_axes([0.57, 0.225, 0.3, 0.75], xscale='log', yscale='log',
                         xlim=[6e-2, 6], ylim=[8e-4, 8])
    # Plot dense flows
    sc = ax_dF.scatter(I[data.dense], DF2[data.dense],
                       c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30,
                       s=16, marker='o')
    # Plot transitional-regime flows
    sc_t = ax_dF.scatter(I[data.trans], DF2[data.trans],
                         c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30,
                         s=16, marker='o')
    sc_t.set_facecolor("none")
    # Plot power law for comparison
    ax_dF.plot([0.1, 1.0], [0.01, 1.0], '--k')
    ax_dF.text(0.2, 0.1, r'$\hat{I}^2$')
    # Label axes
    ax_dF.set_xlabel(r'$\hat{I}$', fontsize=8)
    ax_dF.xaxis.set_label_coords(0.5, -0.175)
    ax_dF.set_ylabel(r'$\delta \mathcal{F}^2$', fontsize=8)
    ax_dF.tick_params(axis='both', which='both', labelsize=8)
    cax = fig.add_axes([0.875, 0.225, 0.025, 0.55])
    cbar = fig.colorbar(sc, cax=cax)
    cbar.set_label(r'$q$ (kg m$^{-1}$ s$^{-1}$)', fontsize=8)
    cax.tick_params(labelsize=8)
    plt.show()

def plot_v2_bar():
    """
    Produces plots demonstrating the importance of the Green's function
    Plots the ratios of the mean squared values of a) experimental velocities
    and b) velocities for a idealised geophysical Green's function
    """
    fig = plt.figure(figsize=[5, 2])
    # Set up axes of the experimental mean squared velocity ve2 against
    # the ratio to ve2 of a theoretical geophysical mean squared velocity vR2
    ax = fig.add_axes([0.12, 0.225, 0.3, 0.75], xscale='log', yscale='log',
                      xlim=[2e-9, 5e-6], ylim=[4e-3, 5e-2])
    ax.set_yticks([0.01], minor=False)
    ax.set_yticklabels(['0.01'], minor=False)
    ax.set_yticks([0.004, 0.005, 0.006, 0.007, 0.008, 0.009,
                   0.02, 0.03, 0.04, 0.05], minor=True)
    ax.set_yticklabels(['', '0.005', '', '', '', '',
                        '0.02', '', '', '0.05'], minor=True)
    # Plot dense flows
    ax.scatter(data.ve2_bar[data.dense],
               data.vR2_bar[data.dense] / data.ve2_bar[data.dense],
               c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30,
               s=16, marker='o')
    # Plot transitional-regime flows
    sc = ax.scatter(data.ve2_bar[data.trans],
                    data.vR2_bar[data.trans] / data.ve2_bar[data.trans],
                    c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30,
                    s=16, marker='o')
    sc.set_facecolor("none")
    # Label axes
    ax.set_xlabel(r'$\langle v_j^2 \rangle$ (m$^2$ s$^{-2}$)',
                  fontsize=8)
    ax.xaxis.set_label_coords(0.5, -0.175)
    ax.set_ylabel(r'$\langle v_r^2 \rangle / \langle v_j^2 \rangle$',
                  fontsize=8)
    ax.yaxis.set_label_coords(-0.2, 0.5)
    ax.tick_params(axis='both', which='both', labelsize=8)
    # Set up axes of the experimental mean squared velocity vR2 against
    # the ratio to vR2 of a theoretical geophysical mean squared velocity ve2
    ax = fig.add_axes([0.54, 0.225, 0.3, 0.75], xscale='log', yscale='log',
                      xlim=[1e-11, 1e-6], ylim=[20, 250])
    ax.set_yticks([100], minor=False)
    ax.set_yticklabels(['100'], minor=False)
    ax.set_yticks([20, 30, 40, 50, 60, 70, 80, 90, 200], minor=True)
    ax.set_yticklabels(['20', '', '', '50', '', '',
                        '', '', '200'], minor=True)
    # Plot dense flows
    ax.scatter(data.vR2_bar[data.dense],
               data.ve2_bar[data.dense] / data.vR2_bar[data.dense],
               c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30,
               s=16, marker='o')
    # Plot transitional-regime flows
    sc = ax.scatter(data.vR2_bar[data.trans],
                    data.ve2_bar[data.trans] / data.vR2_bar[data.trans],
                    c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30,
                    s=16, marker='o')
    sc.set_facecolor("none")
    # Label axes
    ax.set_xlabel(r'$\langle v_r^2 \rangle$ (m$^2$ s$^{-2}$)',
                  fontsize=8)
    ax.xaxis.set_label_coords(0.5, -0.175)
    ax.set_ylabel(r'$\langle v_j^2 \rangle / \langle v_r^2 \rangle$',
                  fontsize=8)
    ax.yaxis.set_label_coords(-0.2, 0.5)
    ax.tick_params(axis='both', which='both', labelsize=8)
    # Add a colorbar
    cax = fig.add_axes([0.85, 0.225, 0.02, 0.55])
    cbar = fig.colorbar(sc, cax=cax)
    cbar.set_label(r'$q$ (kg m$^{-1}$ s$^{-1}$)', fontsize=8)
    cax.tick_params(labelsize=8)
    plt.show()

def Bachelet_variant_P_F0(U=lambda index: 1, gamma=lambda f, h, z: 0.01):
    """
    Calculates predictions for P_F^0 for a variant of Bachelet (2018)'s model.
    Assumes no. of impacts per unit time & volume n_I = 4 phi du/dz / pi d^3,
    typical squared impulse Delta p = 2 * m * sqrt(T), and attenuation with
    depth of the impulse transferred, by exp(-gamma), with velocity profile
    u = U u_w and temperature profile T = U^2 T_w

    Args:
        U: function for U from the experiment's index
        gamma: function for gamma from frequency, flow depth, and impact height

    Returns:
        predicted values of P_F^0
    """
    # Profiles are nondimensionalised by velocity g/d, so we add this here
    n_i0 = 4 * np.sqrt(g / d) / (np.pi * d**3)
    Dp20 = (2 * m * np.sqrt(g * d))**2
    output = pd.DataFrame(columns=['P_F0'])
    # For each experiment's index and channel-wall profiles,
    for index, prof in zip(data.index, profiles):
        # ... find the index of the surface
        id_m = np.argmax(prof.phi)
        id_s = id_m + np.argmax(prof.phi[id_m:] < prof.phi.max() / 2)
        # ... and integrate contributions with depth to P_F(1 kHz)
        P_F0 = (X * Y * d * n_i0 * Dp20 * data.phi[index] * U(index)**3
                * (-prof.u.diff(periods=-1)[:id_s] * prof['T'][:id_s]
                   * np.exp(-gamma(1e3, data.h[index], prof.z[:id_s]))).sum())
        output = output.append(pd.Series({'P_F0': P_F0},
                                         name=index))
    # Return predictions for P_F0
    return output

def Bachelet_variant_model(U=lambda index: 1, gamma=lambda f, h, z: 0.01):
    """
    Calculates predictions for P_F^0, f_c for a variant of Bachelet (2018).
    Assume no. of impacts per unit time & volume n_I = 4 phi du/dz / pi d^3,
    typical squared impulse Delta p = 2 * m * sqrt(T), and attenuation with
    depth of the impulse transferred, by exp(-gamma), with velocity profile
    u = U u_w and temperature profile T = U^2 T_w
    
    Args:
        U: function for U from the experiment's index
        gamma: function for gamma from frequency, flow depth, and impact height

    Returns:
        predicted values of P_F^0 and f_c
    """
    # Profiles are nondimensionalised by velocity g/d, so we add this here
    n_i0 = 4 * np.sqrt(g / d) / (np.pi * d**3)
    Dp20 = (2 * m * np.sqrt(g * d))**2
    tau0 = (np.pi**2 * rho**2 * (1 - nu**2)**2
            / ((4 * E**2) * np.sqrt(g * d)))**0.2 * d
    # Set up frequencies f at which P_F will be estimated, plus the output
    freq = np.linspace(1e3, 2 * nondim_f_c / tau0, 200)
    output = pd.DataFrame(columns=['P_F0', 'f_c'])
    # For each experiment's index and channel-wall profiles,
    for index, prof in zip(data.index, profiles):
        # ... find the index of the surface,
        id_m = np.argmax(prof.phi)
        id_s = id_m + np.argmax(prof.phi[id_m:] < prof.phi.max() / 2)
        # ... integrate contributions with depth to P_F(f) for each f,
        P_F = (X * Y * d * n_i0 * Dp20 * data.phi[index] * U(index)**3
               * np.array([(-prof.u.diff(periods=-1)[:id_s] * prof['T'][:id_s]
                            * np.exp(-gamma(f, data.h[index], prof.z[:id_s]))
                            * zeta(f * tau0 * prof['T'][:id_s]**-0.1)).sum()
                           for f in freq]))
        # ... and extract corner frequency f_c, power spectral density P_F0 
        id_c = np.argmax(P_F < P_F[:np.argmax(P_F)+1].mean() / 2)
        P_F0 = P_F[:int(id_c // 2)+1].mean()
        f_c = freq[id_c]
        # Plot an example
        if (index == 'g0.44_h20_0'):
            plt.figure()
            plt.loglog(freq, P_F)
            plt.xlabel(r'$f$ (Hz)')
            plt.ylabel(r'$P_F$ (N$^2$s)')
        output = output.append(pd.Series({'P_F0': P_F0, 'f_c': f_c},
                                         name=index))
    # Return predictions for P_F0 and f_c
    return output

def fit_Bachelet_variant(U_mode='unit', gamma_mode='constant'):
    """
    Fit free parameters of a Bachelet (2018) model variant and plot results.
    Modes of U and gamma select in which models free parameters are fitted.
    Read source code for details.

    Args:
        U_mode: 'unit'/'constant'/'const_flux', model for function U
        gamma_mode: 'constant'/'mindlin'/'const_Q'/'linear_Q'/'mindlin_Q',
                    model for function gamma
    """
    if (U_mode == 'unit'):
        # Assume profiles in the bulk are identical to those at the wall
        U = lambda U0: (lambda index: 1)
        U_descr = 'U = {:.1e}'
    elif (U_mode == 'constant'):
        # Assume a constant scaling factor between wall and bulk profiles
        U = lambda U0: (lambda index: U0)
        U_descr = 'U = {:.1e}'
    elif (U_mode == 'const_flux'):
        # Scale wall profiles to match the observed mass flux for each flow
        U = lambda U0: (lambda index: (data.q[index]
                                       / (rho * 1e-4 * data.Q_wall[index])))
        U_descr = 'U = q / q_wall'
    if (gamma_mode == 'constant'):
        # Assume a constant rate of attenuation with depth
        gamma = lambda gamma0: (lambda f, h, z: gamma0 * z)
        g_descr = 'gamma = {:.1e} z'
    elif (gamma_mode == 'mindlin'):
        # Assume a Mindlin-theory attenuation rate, ~ pressure^(-1/6)
        gamma = lambda gamma0: (lambda f, h, z: gamma0 * (1 - (1-z/h)**(5/6)))
        g_descr = 'gamma = {:.1e} (1 - (1-z/h)^(5/6))'
    elif (gamma_mode == 'const_Q'):
        # Assume a constant quality factor Q, so that attenuation rate ~ f
        gamma = lambda iQc0: (lambda f, h, z: 2 * np.pi * f * z * iQc0)
        g_descr = 'gamma = 2 \pi f z / {:.1e}^-1'
    elif (gamma_mode == 'linear_Q'):
        # Assume Q is linear in depth, so that attenuation rate ~ f / z
        gamma = lambda h_Qc0: (lambda f, h, z: -h_Qc0 * f * np.log(1 - z/h))
        g_descr = 'gamma = {:.1e} f ln(1 - z/h)'
    elif (gamma_mode == 'mindlin_Q'):
        # Assume Q ~ pressure^{1/6}, so that attenuation rate ~ f / (h-z)^1/6
        gamma = lambda h_Qc0: (lambda f, h, z: h_Qc0 * f * (1 - (1-z/h)**(5/6)))
        g_descr = 'gamma = {:.1e} f (1 - (1-z/h)^(5/6))'
    print('Fitting variant of Bachelet model')
    # Fit the free parameters in the above models for U and gamma, using P_F0
    params = least_squares(lambda p: np.log(Bachelet_variant_P_F0(U(p[0]),
                                                                  gamma(p[1]))
                                            .P_F0 / data.P_F0),
                           x0=(1, 0.01), bounds=(0, np.inf), xtol=5e-2).x
    # Print the best-fit parameter values
    print('  Best fit:')
    print('    ' + U_descr.format(params[0]))
    print('    ' + g_descr.format(params[1]))
    # Calculate the predictions for P_F0 and f_c of the specified variant
    pred = Bachelet_variant_model(U(params[0]), gamma(params[1]))
    # Calculate the ranges of predictions and observations, for axes limits
    y1_range = np.array([0.5 * data.P_F0.min(), 2 * data.P_F0.max()])
    y2_range = 1e-3 * np.array([0.9 * data.f_c.min(), 1.1 * data.f_c.max()])
    P_F0_range = (0.5 * pred.P_F0.min(), 2 * pred.P_F0.max())
    f_c_range = (0.9e-3 * pred.f_c.min(), 1.11e-3 * pred.f_c.max())
    fig = plt.figure(figsize=[6, 2.7])
    # Prepare axes of predictions of P_F0 against observations
    ax1 = fig.add_axes([0.14, 0.2, 0.36, 0.8], xscale='log', yscale='log',
                       xlim=P_F0_range, ylim=y1_range)
    # Plot perfect agreement for power spectral density P_F0
    ax1.plot(y1_range, y1_range, '-', color=[0.5, 0.5, 0.5])
    # Plot P_F0 for dense flows
    sc_dense = ax1.scatter(pred.P_F0[data.dense], data.P_F0[data.dense],
                           c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30)
    # Plot P_F0 for transitional-regime flows
    sc_trans = ax1.scatter(pred.P_F0[data.trans], data.P_F0[data.trans],
                           c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30)
    sc_trans.set_facecolor("none")
    # Label axes
    ax1.set_xlabel(r'$\hat{P}_F^0$ (N$^2$ s)', fontsize=8)
    ax1.set_ylabel(r'$P_F^0$ (N$^2$ s)', fontsize=8)
    ax1.tick_params(axis='both', which='both', labelsize=8)
    # Prepare axes of predictions of f_c against observations
    ax2 = fig.add_axes([0.64, 0.2, 0.36, 0.8], xscale='log', yscale='log',
                       xlim=f_c_range, ylim=y2_range)
    # Plot perfect agreement for corner frequency f_c
    ax2.plot(y2_range, y2_range, '-', color=[0.5, 0.5, 0.5])
    # Plot f_c for dense flows
    sc_dense = ax2.scatter(1e-3*pred.f_c[data.dense], 1e-3*data.f_c[data.dense],
                           c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30)
    # Plot f_c for transitional-regime flows
    sc_trans = ax2.scatter(1e-3*pred.f_c[data.trans], 1e-3*data.f_c[data.trans],
                           c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30)
    sc_trans.set_facecolor("none")
    # Label axes
    ax2.set_xlabel(r'$\hat{f}_c$ (kHz)', fontsize=8)
    ax2.set_ylabel(r'$f_c$ (kHz)', fontsize=8)
    ax2.tick_params(axis='both', which='both', labelsize=8)
    plt.show()
    
def Bagnold_Bachelet_P_F0(T0=1, gamma=0.01):
    """
    Calculates predictions for P_F^0 for a Bagnold profile and Bachelet impacts,
    assuming a Bagnold velocity profile & granular temperature T ~ (d du/dz)^2.

    Assumes no. of impacts per unit time & volume n_I = 4 phi du/dz / pi d^3,
    typical squared impulse Delta p = 2 * m * sqrt(T),
    and attenuation with depth of the impulse transferred, by exp(-gamma z),
    with velocity profile u = 5 (1 - (1 - z/h)^3/2) \\bar{u} / 3
    and temperature profile T = 2 T0 (d du/dz)^2
    
    Args:
        T0: float for the constant of proportionality for T
        gamma: float for the constant of squared impulse attenuation with height

    Returns:
        predicted values of P_F^0
    """
    dz = 0.01
    z = np.arange(0, 1, dz)
    # Non-dimensional integral of non-dim contributions at different heights
    I = np.array([((1 - z)**1.5 * np.exp(-gamma*h*z)).sum() * dz
                  for h in data.h])
    # Power spectral density P_F0 = int_0^h n_I (Delta p)^2 e^(-gamma z) dz
    P_F0 = (500 / np.pi * X * Y * m**2 / d
            * data.phi * T0 * data.u_bar**3 / data.h**2 * I)
    # Return predictions for P_F0
    output = pd.DataFrame({'P_F0': P_F0}, index=data.index)
    return output

def Bagnold_Bachelet_model(T0=1, gamma=0.01):
    """
    Calculate P_F^0 & f_c predictions for a Bagnold + Bachelet model,
    assuming a Bagnold velocity profile & granular temperature T ~ (d du/dz)^2.

    Assumes no. of impacts per unit time & volume n_I = 4 phi du/dz / pi d^3,
    typical squared impulse Delta p = 2 * m * sqrt(T),
    and attenuation with depth of the impulse transferred, by exp(-gamma z),
    with velocity profile u = 5 (1 - (1 - z/h)^3/2) \\bar{u} / 3
    and temperature profile T = 2 T0 (d du/dz)^2
    
    Args:
        T0: float for the constant of proportionality for T
        gamma: float for the constant of squared impulse attenuation with height

    Returns:
        predicted values of P_F^0
    """
    dz = 0.01
    z = np.arange(0, 1, dz)
    # Hertzian impact timescale for impacts at z = 0
    tau = hertz_tau(5 * np.sqrt(T0 / 2) * data.u_bar * d / data.h)
    output = pd.DataFrame(columns=['P_F0', 'f_c'])
    # For each experiment's index, impact timescale and flow depth,
    for index, tau0, h in zip(data.index, tau, data.h):
        # ... define the frequencies f at which P_F is to be calculated
        freq = np.linspace(1e3, 2 * nondim_f_c / tau0, 200)
        # ... calculate at each f the non-dimensional integral of contributions
        I = np.array([((1 - z)**1.5 * zeta(f * tau0 / (1 - z)**0.1)
                       * np.exp(-gamma*h*z)).sum() * dz
                      for f in freq])
        # ... extract the corner frequency and the non-dimensional P_F0
        id_c = np.argmax(I < I[:np.argmax(I)+1].mean() / 2)
        P_F0 = I[:int(id_c // 2)+1].mean()
        f_c = freq[id_c]
        output = output.append(pd.Series({'P_F0': P_F0, 'f_c': f_c},
                                         name=index))
    # Rescale P_F0
    output.P_F0 = (500 / np.pi * X * Y * m**2 / d
                   * data.phi * T0 * data.u_bar**3 / data.h**2 * output.P_F0)
    # Return predictions for P_F0 and f_c
    return output
    
def fit_Bagnold_Bachelet():
    """
    Calculate the best-fit gamma for the Bagnold-Bachelet model and plot results
    For description of the model, see help(Bagnold_Bachelet_model)
    """
    # Find the log least squares best-fit value of gamma for P_F0
    gamma = least_squares(lambda params: np.log(Bagnold_Bachelet_P_F0(1,
                                                                      params)
                                                   .P_F0 / data.P_F0),
                             x0=(0.01), bounds=(0, np.inf), xtol=5e-2).x[0]
    # Print the best-fit value
    print('  Best fit: gamma = {}'.format(gamma))
    pred = Bagnold_Bachelet_model(1, gamma)
    # Calculate the typical error factor and print it
    epsilon = np.exp(np.sqrt(np.mean(np.log(data.P_F0 / pred.P_F0)**2)))
    print('  Misfit \epsilon = {}'.format(epsilon))
    # Calculate the range of observations and predictions
    y1_range = np.array([0.5 * data.P_F0.min(), 2 * data.P_F0.max()])
    y2_range = 1e-3 * np.array([0.9 * data.f_c.min(), 1.1 * data.f_c.max()])
    P_F0_range = (0.5 * pred.P_F0.min(), 2 * pred.P_F0.max())
    f_c_range = (0.9e-3 * pred.f_c.min(), 1.11e-3 * pred.f_c.max())
    fig = plt.figure(figsize=[6, 2.7])
    # Prepare axes for the comparison of predictions and observations for P_F0
    ax1 = fig.add_axes([0.14, 0.2, 0.36, 0.8], xscale='log', yscale='log',
                       xlim=P_F0_range, ylim=y1_range)
    # Plot perfect agreement
    ax1.plot(y1_range, y1_range, '-', color=[0.5, 0.5, 0.5])
    # Plot dense flows
    sc_dense = ax1.scatter(pred.P_F0[data.dense], data.P_F0[data.dense],
                           c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30)
    # Plot transitional-regime flows
    sc_trans = ax1.scatter(pred.P_F0[data.trans], data.P_F0[data.trans],
                           c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30)
    sc_trans.set_facecolor("none")
    # Label axes
    ax1.set_xlabel(r'$\hat{P}_F^0$ (N$^2$ s)', fontsize=8)
    ax1.set_ylabel(r'$P_F^0$ (N$^2$ s)', fontsize=8)
    ax1.tick_params(axis='both', which='both', labelsize=8)
    # Prepare axes for the comparison of predictions and observations for f_c
    ax2 = fig.add_axes([0.64, 0.2, 0.36, 0.8], xscale='log', yscale='log',
                       xlim=f_c_range, ylim=y2_range)
    # Plot perfect agreement
    ax2.plot(y2_range, y2_range, '-', color=[0.5, 0.5, 0.5])
    # Plot dense flows
    sc_dense = ax2.scatter(1e-3*pred.f_c[data.dense], 1e-3*data.f_c[data.dense],
                           c=data.q[data.dense], cmap='plasma', vmin=0, vmax=30)
    # Plot transitional-regime flows
    sc_trans = ax2.scatter(1e-3*pred.f_c[data.trans], 1e-3*data.f_c[data.trans],
                           c=data.q[data.trans], cmap='plasma', vmin=0, vmax=30)
    sc_trans.set_facecolor("none")
    # Label axes
    ax2.set_xlabel(r'$\hat{f}_c$ (kHz)', fontsize=8)
    ax2.set_ylabel(r'$f_c$ (kHz)', fontsize=8)
    ax2.tick_params(axis='both', which='both', labelsize=8)
    plt.show()

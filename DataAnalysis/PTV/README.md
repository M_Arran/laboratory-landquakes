Code to perform Particle Tracking Velocimetry on images of a flow at a transparent channel sidewall

Contents:
* `track_particles.m` Matlab function to perform PTV, tracking particles between subsequent images to extract their velocities
* `coarse_grain.m` Matlab function to coarse-grain particle velocity measurements, saving depth profiles of continuum fields
* `plot_x_t_variation.m` Matlab function to plot the variation of depth profiles with a) downslope position in the field of view and b) time

Use:
* Ensure that all the required images have been captured and are accessible:
  * Frames from footage of the granular flow, on which particle tracking velocimetry is to be performed
  * An image of a checkerboard pattern, which will be used to calibrate distances in images
  * An image in which only particles are visible, which will be used to calibrate detection thresholds
  * Square images of individual particles, which will be used to generate a search pattern
* Update definitions of variables `folder`, `checkerboard`, and `im` in `track_particles.m`, to correctly refer to these images' locations.
* Update definitions of variables `d`, `frame_rate`, `max_speed`, `min_age`, `pattern_size`, `n_checks`, `check_size` in `track_particles.m`, as required.
* Update definitions of `data_filename` in `track_particles.m`; `data_folder`, `data_filename`, `results_folder`, `profile_filename`, and `results_filename` in `coarse_grain.m`; and `data_folder`, `plot_folder`, `data_filename`, and `filename` in `plot_x_t_variation.m`, for storage of results and outputs, as desired.
* Update definitions of variable `d` in `coarse_grain.m` and `plot_x_t_variation.m`, as required; and `t_range`, `coarseness_t`, `x_range`, `coarseness_x`, `z_range`, and `coarseness_z` in `coarse_grain.m`, as desired.
* From Matlab, run `track_particles`, `coarse_grain`, and then `plot_x_t_variation` for each experimental gradient, release gate gap height, and repeat.
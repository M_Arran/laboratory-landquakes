function [] = track_particles(grad, gap, rep, n_frames)
% TRACK_PARTICLES Analyse images to track particles and extract velocities
%   For repeat rep of the experiment at incline grad, release gate gap,
%   conduct particle tracking velocimetry over n_frames frames

%% Parameters
% Particle diameter (m)
d = 0.002;
% Frame rate (Hz)
frame_rate = 2000;
% Maximum detectable particle speed (m/s)
max_speed = 1.2;
% Number of frames over which a particle must be tracked to be used
min_age = 5;

% Folder for experimental data
folder = '/media/matt/Seagate Expansion Drive/Experimental_data';

%% Calibrate and set up particle tracking
% Pixels across 'ideal' beads
pattern_size = 18;
% Number of corners in the calibration image, horizontally and vertically
n_checks = [7, 3];
% Distance between adjacent corners in the calibration image (m)
check_size = 0.01;

% Use an image to calibrate resolution in x & y (px / m)
checkerboard = imread(sprintf('%s/Inclination_%0.2f/calibration.png', ...
                              folder, grad));
%   Convolve image with a check pattern to highlight checkerboard corners
check = int8((2*((1:20)' > 10) - 1) * (2*((1:20) > 10) - 1));
corner_intensity = conv2(checkerboard(:,10:end-10,1), check, 'valid').^2;
%   Find corner locations at subpixel resolution and sort into an array
region_stats = regionprops(corner_intensity > 0.25 * (128*2*10^2)^2, ...
    corner_intensity, 'WeightedCentroid');
corners = reshape([region_stats.WeightedCentroid], ...
    [2, n_checks(2), n_checks(1)]);
for row = 1:n_checks(1)
    [corners(2, :, row), I] = sort(corners(2, :, row));
    corners(1, :, row) = corners(1, I, row);
end
%   Calculate the resolution using the mean no. of pixels between corners
resolution_x = mean(mean(diff(corners(1,:,:), 1, 3))) / check_size;
resolution_z = -mean(mean(diff(corners(2,:,:), 1, 2))) / check_size;
%   Convert bead size and maximum speed to pixel, frame rate units
bead_size = round(resolution_x * d);
max_displ = resolution_x * max_speed / frame_rate;

% Use individually identified beads to produce a search pattern
pattern = zeros(pattern_size, 'single');
for im_id = 0:24
    im = imread(sprintf('%s/Inclination_%0.2f/beads/bead_%02d.png', ...
        folder, grad, im_id));
    pattern = pattern + single(im(:,:,1));
end
pattern = (pattern - mean(mean(pattern))) / 25;

% Convolve a bead-only image with the search pattern
im = imread(sprintf('%s/Inclination_%0.2f/bead_sample.png', folder, grad));
correlation = conv2(im(:,:,1), rot90(pattern, 2), 'same');
% Use the upper quartile of the convolution product as a threshold
threshold = prctile(correlation(:), 75);

% Define a parabolic smoothing filter and its corresponding circular mask
edge = bead_size / 2;
span = (0:bead_size-1) - edge;
sq = repmat(span.^2, [bead_size,1]);
filt = max(edge^2 - sq - sq', 0);
mask = filt > 0;

%% Find particle centres in each frame
n = cell(n_frames, 1);
centres = cell(n_frames, 1);
for f_id = 0:(n_frames-1)
    % Read image
    im = imread(sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/frames_%d/frame_%05d.Png', ...
                        folder, grad, gap, rep, f_id));
    % Calculate correlation of image and search pattern, threshold & smooth
    correlation = conv2(im(:,:,1), rot90(pattern, 2), 'same');
    bead_map = conv2(correlation .* (correlation > threshold), filt, 'same');
    % Identify beads by maxima of thresholded, smoothed correlation
    [z, x] = find(bead_map(1+edge:size(bead_map, 1)-bead_size,...
                           1+edge:size(bead_map, 2)-edge));
    x = x + edge;
    z = z + edge;
    c_id = 0;
    centres{f_id+1} = nan(numel(bead_map) / bead_size^2, 2);
    % Iterating over all non-zero pixels, ...
    for j=1:length(z)
        % If the pixel is a local maximum of the smoothed correlation, ...
        if ((bead_map(z(j), x(j)) >= bead_map(z(j)-1, x(j)-1)) &&...
                (bead_map(z(j), x(j)) > bead_map(z(j)-1, x(j))) &&...
                (bead_map(z(j), x(j)) >= bead_map(z(j)-1, x(j)+1)) &&...
                (bead_map(z(j), x(j)) > bead_map(z(j), x(j)-1)) &&...
                (bead_map(z(j), x(j)) > bead_map(z(j), x(j)+1)) &&...
                (bead_map(z(j), x(j)) >= bead_map(z(j)+1, x(j)-1)) &&...
                (bead_map(z(j), x(j)) > bead_map(z(j)+1, x(j))) &&...
                (bead_map(z(j), x(j)) >= bead_map(z(j)+1, x(j)+1)))
            
            % Identify the pre-smoothing maximal pixel in a bead's radius
            local_corr = correlation(z(j) + span, x(j) + span) .* mask;
            [~, max_id] = max(local_corr(:));
            [max_local_z, max_local_x] = ind2sub(size(mask), max_id);
            max_x = x(j) - edge + max_local_x - 1;
            max_z = z(j) - edge + max_local_z - 1;
            if ((max_x > edge && max_x <= size(correlation,2) - edge)...
                    && (max_z > edge && max_z <= size(correlation,1) - bead_size))
                % Fit quadratics to identify the underlying subpixel maximum
                p_x = polyfit(-2:2, correlation(max_z, max_x + (-2:2)), 2);
                p_z = polyfit(-2:2, correlation(max_z + (-2:2), max_x)', 2);
                c_id = c_id + 1;
                centres{f_id+1}(c_id, 1) = max_x - 0.5 * p_x(2)/p_x(1);
                centres{f_id+1}(c_id, 2) = max_z - 0.5 * p_z(2)/p_z(1);
            end
        end
    end
    n{f_id+1} = c_id;
    centres{f_id+1}(c_id+1:end,:) = [];
end

%% Associate particles identified in subsequent frames
% Pre-allocate memory
age = nan(50*n_frames, 1);
f0 = nan(50*n_frames, 1);
x0 = nan(50*n_frames, 2);
vp = cell(50*n_frames, 1);
% Store information for frame 0
n_p = n{1};
prev_p_ids = 1:n_p;
age(1:n_p) = 0;
f0(1:n_p) = 0;
x0(1:n_p, :) = centres{1}(1:n_p,:);
for id = 1:n_p
    vp{id} = zeros(0, 2);
end
% Run through frames, maintaining particle IDs and recording their velocities
for f_id = 1:(n_frames-1)
    p_ids = nan(n{f_id+1}, 1);
    % For each particle centre, find in the prev frame the closest to it
    [sq_dist, id_closest] = min((repmat(centres{f_id+1}(:, 1), [1, n{f_id}]) ...
        - repmat(centres{f_id}(:, 1)', [n{f_id+1}, 1])).^2 ...
        + (repmat(centres{f_id+1}(:, 2), [1, n{f_id}]) ...
        - repmat(centres{f_id}(:, 2)', [n{f_id+1}, 1])).^2, [], 2);
    % Consider only those closer than max_displ
    id_close = find(sq_dist < max_displ^2)';
    % Where 2+ of these share a closest centre, associate only the closest
    [~, sort_order] = sort(sq_dist(id_close));
    [~, unique_order, ~] = unique(id_closest(id_close(sort_order)), 'stable');
    id_old = id_close(sort_order(unique_order));
    
    % Assign particles ID, either the associated previous IDs or new ones
    id_new = setdiff(1:n{f_id+1}, id_old);
    n_new = numel(id_new);
    p_ids(id_old) = prev_p_ids(id_closest(id_old));
    p_ids(id_new) = n_p + (1:n_new);
    % For previously identified particles, update age & calculate velocity
    age(p_ids(id_old)) = age(p_ids(id_old)) + 1;
    for id = id_old
        vp{p_ids(id)} = [vp{p_ids(id)}; ...
            centres{f_id+1}(id, :) - centres{f_id}(id_closest(id), :)];
    end
    % For new particles, initialise age & velocity, and record start point
    age(p_ids(id_new)) = 0;
    f0(p_ids(id_new)) = f_id;
    x0(p_ids(id_new), :) = centres{f_id+1}(id_new, :);
    for id = id_new
        vp{p_ids(id)} = zeros(0, 2);
    end
    
    % Update no. of particles, and 'previous' particle IDs.
    n_p = n_p + n_new;
    prev_p_ids = p_ids;
end


%% Smooth particle velocities and convert to SI units
% Calculate the vertical and horizontal limits of the detection frame
z0 = -(size(im, 1) - bead_size) / resolution_z;
x_lim = [edge, size(im, 2) - edge] / resolution_x;
% Calculate smoothed velocities of all particles tracked sufficiently long
% Integrate smoothed velocities to calculate particle positions
xp_smth = cell(n_p, 1);
vp_smth = cell(n_p, 1);
for p_id = find(age >= min_age)'
    vp_smth{p_id} = [frame_rate / resolution_x * smooth(1:age(p_id), vp{p_id}(:, 1)', min_age, 'rlowess'),...
                     frame_rate / resolution_z * smooth(1:age(p_id), vp{p_id}(:, 2)', min_age, 'rlowess')];
    xp_smth{p_id} = [0, z0] + x0(p_id, :) ./ [resolution_x, resolution_z] + cumsum(vp_smth{p_id}, 1) / frame_rate;
end

%% Save data
data_filename = sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/ptv_raw_data_%d', ...
                        folder, grad, gap, rep);
save(data_filename, 'f0', 'age', 'xp_smth', 'vp_smth', 'min_age', 'x_lim', 'n_frames')

end

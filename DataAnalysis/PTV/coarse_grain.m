function [] = coarse_grain(grad, gap, rep)
% COARSE_GRAIN Calculate continuum fields from individual particle data
%   For repeat rep of the experiment at incline grad, release gate gap,
%   coarse-grain the particle velocities saved in a data file, outputting
%   particle number density, flux, and apparent stress

%% Parameters
% Bead diameter (m), gravity (m / s^2)
d = 0.002; g = 9.8;
% Times at which fields are to be evaluated (recording durations)
t_range = 0.05:0.1:0.95;
% Smoothing time (recording durations)
coarseness_t = 0.05;
% Downslope positions at which fields are to be evaluated (bead diameters)
x_range = 2.5:5:37.5;
% Downslope smoothing width (bead diameters) 
coarseness_x = 2.5;
% Base-normal heights at which fields are to be evaluated (bead diameters)
z_range = 0:0.25:25;
% Base-normal smoothing width (bead diameters) 
coarseness_z = 0.5;

% Folder for experimental data
data_folder = '/media/matt/Seagate Expansion Drive/Experimental_data';
data_filename = sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/ptv_raw_data_%d', ...
                        data_folder, grad, gap, rep);
% Format for profile files
results_folder = '/media/matt/Seagate Expansion Drive/Experiment_results/PTV_analysis';
profile_filename = sprintf('%s/Profiles/g%0.2f_h%02d_%d.txt', ...
                           results_folder, grad, gap, rep);
header = ['    z,     phi,       u,       w,       T\n'...
          '   /d,        , /rt(gd), /rt(gd),     /gd\n'];
format = '%5.2f, %7.5f, %7.5f, %7.5f, %7.5f\n';

%% Load data and set up constants
% Load data from file
load(data_filename, 'f0', 'age', 'xp_smth', 'vp_smth', 'min_age', 'x_lim', 'n_frames');
% Convert properties to SI units and extract numbers of points
t = n_frames * t_range;
N_t = numel(t);
s_t = n_frames * coarseness_t;
x = d * x_range;
N_x = numel(x);
s_x = d * coarseness_x;
z = d * z_range';
N_z = numel(z);
s_z = d * coarseness_z;
% Calculate normalisations for the Gaussian coarse-graining functions
norm_t = kron(normcdf(n_frames, t, s_t) - normcdf(0, t, s_t), ...
              1 - normcdf(0, z, s_z)) * 2 * pi * s_t * diff(x_lim) * s_z;
norm_x = kron(normcdf(x_lim(2), x, s_x) - normcdf(x_lim(1), x, s_x), ...
              1 - normcdf(0, z, s_z)) * 2 * pi * n_frames * s_x * s_z;
norm = (1 - normcdf(0, z, s_z)) * sqrt(2 * pi) * n_frames * diff(x_lim) * s_z;

%% Coarse-grain data
% Set up empty arrays
n_t = zeros(N_z, N_t);
v_t = zeros(N_z, N_t, 2);
v2_t = zeros(N_z, N_t, 2, 2);
n_x = zeros(N_z, N_x);
v_x = zeros(N_z, N_x, 2);
v2_x = zeros(N_z, N_x, 2, 2);
n = zeros(N_z, 1);
v = zeros(N_z, 2);
v2 = zeros(N_z, 2, 2);
% Iterate over frames
for f_id = 1:(n_frames-1)
    % Iterate over particles detected in the frame & tracked long enough
    for p_id = find((age >= min_age) & (f_id > f0) & (f_id <= f0 + age))'
        % Evaluate the coarse-graining function at the particle's position
        w_t = kron(exp(-0.5 * (f_id - t).^2 / s_t^2), ...
                   exp(-0.5 * (shiftdim(xp_smth{p_id}(f_id - f0(p_id), 2), -2) - z).^2 / s_z^2)) ./ norm_t;
        w_x = kron(exp(-0.5 * (shiftdim(xp_smth{p_id}(f_id - f0(p_id), 1), -2) - x).^2 / s_x^2), ...
                   exp(-0.5 * (shiftdim(xp_smth{p_id}(f_id - f0(p_id), 2), -2) - z).^2 / s_z^2)) ./ norm_x;
        w = exp(-0.5 * (shiftdim(xp_smth{p_id}(f_id - f0(p_id), 2), -1) - z).^2 / s_z^2) ./ norm;
        % Add its contributions to the arrays of continuum field values
        n_t = n_t + w_t;
        v_t = v_t + w_t .* reshape(vp_smth{p_id}(f_id - f0(p_id), :), [1,1,2]);
        v2_t = v2_t + w_t .* reshape(vp_smth{p_id}(f_id - f0(p_id), :)' ...
                                     * vp_smth{p_id}(f_id - f0(p_id), :), [1,1,2,2]);
        n_x = n_x + w_x;
        v_x = v_x + w_x .* reshape(vp_smth{p_id}(f_id - f0(p_id), :), [1,1,2]);
        v2_x = v2_x + w_x .* reshape(vp_smth{p_id}(f_id - f0(p_id), :)' ...
                                     * vp_smth{p_id}(f_id - f0(p_id), :), [1,1,2,2]);
        n = n + w;
        v = v + w .* reshape(vp_smth{p_id}(f_id - f0(p_id), :), [1,2]);
        v2 = v2 + w .* reshape(vp_smth{p_id}(f_id - f0(p_id), :)' ...
                               * vp_smth{p_id}(f_id - f0(p_id), :), [1,2,2]);
    end
end
v_t = v_t ./ n_t;
v2_t = v2_t ./ n_t;
v_x = v_x ./ n_x;
v2_x = v2_x ./ n_x;
v = v ./ n;
v2 = v2 ./ n;

%% Save data
% Save profile files
phi = n * pi() / 6;
u = v(:, 1);
w = v(:, 2);
T = sum((v2(:,[1,4]) - v.^2), 2);

profile_file = fopen(profile_filename, 'w');
fprintf(profile_file, header);
fprintf(profile_file, format, ...
        [z / d, phi, u / sqrt(g * d), w / sqrt(g * d), T / (g * d)]);        
fclose(profile_file);

% Save data files for variation downslope and over time
results_filename = sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/ptv_tz_data_%d', ...
                        data_folder, grad, gap, rep);
save(results_filename, 't', 'z', 's_t', 's_z', 'n_t', 'v_t', 'v2_t');
results_filename = sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/ptv_xz_data_%d', ...
                        data_folder, grad, gap, rep);
save(results_filename, 'x', 'z', 's_x', 's_z', 'n_x', 'v_x', 'v2_x');
end
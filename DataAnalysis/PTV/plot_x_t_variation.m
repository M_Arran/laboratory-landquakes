function plot_x_t_variation(grad, gap, rep)
% PLOT_X_T_VARIATION Plot profiles' variation downslope and over time
%   For repeat rep of the experiment at incline grad, release gate gap,
%   plot profiles of volume fraction, velocity, and granular temperature,
%   averaged around different a) downslope positions and b) times

% Folders for data files and plotted outputs
data_folder = '/media/matt/Seagate Expansion Drive/Experimental_data';
plot_folder = '/media/matt/Seagate Expansion Drive/Experiment_results/PTV_analysis';

% Gravitational acceleration (m/s^2)
g = 9.81;
% Bead diameter (m)
d = 0.002;

%% Plot depth profiles of flow properties at different times
% Load file of coarse-grained profiles at different times
data_filename = sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/ptv_tz_data_%d.mat', ...
                        data_folder, grad, gap, rep);
load(data_filename, 't', 'z', 'n_t', 'v_t', 'v2_t');
N_t = numel(t);

% Set up plot
figure;
subplot(1,3,1); hold on;
xlabel('$\phi_w / \max(\phi_w)$', 'interpreter', 'latex')
ylabel('$z / d$', 'interpreter', 'latex')
xlim([0, 1])
subplot(1,3,2); hold on;
xlabel('$E[u_w] / \sqrt{g d}$', 'interpreter', 'latex')
title(sprintf('g%0.2f-h%02d-%d: t-variation', grad, gap, rep))
subplot(1,3,3); hold on;
xlabel('$Var[\mathbf{u}_w] / g d$', 'interpreter', 'latex')
% Plot profiles for different times
for j = 1:N_t
    color = [0, 0, 1.0] + j / N_t * [1, 0.5, -0.5];
    subplot(1,3,1);
    [n_max, id_max] = max(n_t(:, j));
    id_use = 1:(id_max + find(n_t(id_max:end, j) < 0.5 * n_max, 1) + 4);
    plot(n_t(id_use, j) / max(n_t(:, j), [], 1), z(id_use) / d, 'Color', color)
    subplot(1,3,2);
    plot(v_t(id_use, j, 1) / sqrt(g * d), z(id_use) / d, 'Color', color)
    subplot(1,3,3);
    T = sum(v2_t(:, j, [1,4]) - v_t(:, j, :).^2, 2);
    plot(T(id_use) / (g * d), z(id_use) / d, 'Color', color)
end
% Save figure
filename = sprintf('%s/t_variation/g%0.2f_h%02d_%d', ...
                   plot_folder, grad, gap, rep);
print(filename, '-dpng')
%close

%% Plot depth profiles of flow properties at different downslope positions
% Load file of coarse-grained profiles at different times
data_filename = sprintf('%s/Inclination_%0.2f/Gap_%02d_plates/ptv_xz_data_%d', ...
                        data_folder, grad, gap, rep);
load(data_filename, 'x', 'z', 'n_x', 'v_x', 'v2_x');
N_x = numel(x);

% Set up plot
figure;
subplot(1,3,1); hold on;
xlabel('$\phi_w / \max(\phi_w)$', 'interpreter', 'latex')
ylabel('$z / d$', 'interpreter', 'latex')
xlim([0, 1])
subplot(1,3,2); hold on;
xlabel('$E[u_w] / \sqrt{g d}$', 'interpreter', 'latex')
title(sprintf('g%0.2f-h%02d-%d: x-variation', grad, gap, rep))
subplot(1,3,3); hold on;
xlabel('$Var[\mathbf{u}_w] / g d$', 'interpreter', 'latex')
% Plot profiles for different downslope positions
for j = 1:N_x
    color = [0, 0, 1.0] + j / N_x * [1, 0.5, -0.5];
    subplot(1,3,1);
    [n_max, id_max] = max(n_x(:, j));
    id_use = 1:(id_max + find(n_x(id_max:end, j) < 0.5 * n_max, 1) + 4);
    plot(n_x(id_use, j) / n_max, z(id_use) / d, 'Color', color)
    subplot(1,3,2);
    plot(v_x(id_use, j, 1) / sqrt(g * d), z(id_use) / d, 'Color', color)
    subplot(1,3,3);
    T = sum(v2_x(:, j, [1,4]) - v_x(:, j, :).^2, 2);
    plot(T(id_use) / (g * d), z(id_use) / d, 'Color', color)
end
% Save figure
filename = sprintf('%s/x_variation/g%0.2f_h%02d_%d', ...
                   plot_folder, grad, gap, rep);
%print(filename, '-dpng')
%close
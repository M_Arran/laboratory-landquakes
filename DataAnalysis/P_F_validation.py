## Python3 script for validating calibration of P_F
## (the power spectrum of the basal force applied by the flow)

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# Frequencies used for theoretical calculations (Hz)
centre_freqs = 2**np.arange(10, 17, 0.1)
# Minimum and maximum frequencies in analysis (Hz)
f_min, f_max = 500, 120000
# Width of moving average in frequency space (Hz)
Df = 2e3

# Set material parameters (in S.I. units)
#    Bead radius, Young's modulus, density, Poisson's ratio
r_i, E_i, rho_i, nu_i = 1.5625e-3, 200.e9, 7800, 0.29
r_b, E_b, rho_b, nu_b = 1.e-3, 63.e9, 2500, 0.23
#    Plate thickness, length, width
h_p, X, Y = 2.e-3, 0.18, 0.10
#    Plate Young's modulus, density, Poisson's ratio
E_p, rho_p, nu_p = 200e9, 7800, 0.29
#    Proportion of plate energy in vertical motion, plate quality factor
P, Q = 0.25, 99

# Set impact properties (in S.I. units)
#    Number of impacts
n_i = 10
#    Gravitational acceleration, impact height
g, h0 = 9.81, 0.1

# Set resolution for theoretical calculations
#    Timestep for Hertzian force integration
hertz_dt = 0.01
#    Number of points in Fourier transform
Nfft = 2**15

# Data folder
folder = "/media/matt/Seagate Expansion Drive/Experimental_data/P_F_validation"
output = "/media/matt/Seagate Expansion Drive/Experiment_results/Lab_analysis"
# Set accelerometer properties
#    Number of acceleros
n_a = 4
#   Sampling frequency, time prefix, duration
f_pico, prefix, Dt = 2.5e5, 1.e-6, 4e-2
#   Accelerometer sensitivites, pC / (m/s**2)
sensitivities = [0.00675, 0.00561, 0.00538, 0.00658]
#   Conditioner amplification for [on-, off-]plate impacts, V / pC
amplification = [0.00316 / 0.006, 0.1 / 0.006]
#   Calibration curve Gamma
def Gamma(f):
    return 1 + (f / 6.1e4)**6

# Predict PSD of basal force measurements from theory, assuming:
#    - fixed beads in a hexagonal packing
#    - impacting bead position uniformly distributed
#    - interaction force entirely Hertzian and normal
#    - fixed beads only transmit vertical force, and then perfectly

print('Starting theoretical calculation...')

#   Generate a length-n array of random impact angles on a hexagonal pack
angle_i = np.array([])
while angle_i.size < n_i:
    x = np.random.rand()
    y = np.random.rand()
    # if (2/sqrt(3) x, y) lies inside a unit hexagon...
    if x < (1 - 0.5 * y):
        # ... use the corresponding impact angle
        angle_i = np.append(angle_i,
                            np.arcsin(np.sqrt(4./3. * x**2 + y**2)
                                      / (1 + r_i/r_b)))

# Calculate the normal force throughout a non-dimensionalised impact
def hertz_ode(s, t):
    return np.array([s[1], (-s[0])**1.5 if s[0] < 0 else 0])

t = np.arange(0, 5, hertz_dt)
solution = odeint(hertz_ode, np.array([0, -1]), t)
F_t = np.array([((-s)**1.5 if s < 0 else 0) for s in solution[:,0]])
F_f = np.fft.fft(F_t, n=Nfft) * hertz_dt
f = np.arange(Nfft) / (Nfft * hertz_dt)

# Calculate the dimensional impact parameters
#     Normal impact velocities
v_i = np.sqrt(2 * g * h0) * np.cos(angle_i)
#     Effective stiffness, bead mass
E_c = ((1 - nu_b**2) / E_b + (1 - nu_i)**2 / E_i)**-1
K_c = 4./3. * E_c / np.sqrt(1 / r_b + 1 / r_i)
m_i = 4./3. * np.pi * rho_i * r_i**3
#     Impact length and time scales
L_i = (m_i * v_i**2 / K_c)**0.4
T_i = (m_i**2 / (v_i * K_c**2))**0.2

# Calculate the theoretical power spectral density of basal force
P_F_thy = np.array([(np.interp(f0 * T_i, f, np.abs(F_f)**2 / Dt)
                    * (np.cos(angle_i) * m_i * v_i)**2)
                    for f0 in centre_freqs]).T
P_F_thy_bar = np.nanmean(P_F_thy, axis=0)
P_F_thy_err = np.nanstd(P_F_thy, axis=0) / np.sqrt(n_i)
print('Theoretical calculations finished')

# Extract PSD of basal force from accelerometer measurements, assuming:
#   - plate response perfectly described by Kirchoff-Love theory
#   - proportion of energy in measurable vertical motion P
#   - energy linearly attenuated with quality factor Q
#   - high-frequency accelero response fixed by calibrated Gamma
#   - accelerometer sensitivity given by documented values

print('Starting analysis of measurements')

# Filenames for accelerometer data
on_file = folder + "/on_plate_{:01d}.txt"
off_file = folder + "/off_plate_{:01d}.txt"

# Plate bending stiffness (kg m^2/s^2)
D = E_p * h_p**3 / (12 * (1 - nu_p**2))
# Prefactor for calculating the power spectrum P_F of the basal force
P_F_prefactor = (rho_p * h_p)**1.5 * X * Y * np.sqrt(D) / (np.pi * P * Q)
# Estimate power spectral density of base-normal force of on-plate impacts
f = np.arange(f_min, f_max, 1 / Dt)
n_smooth = int(Df * Dt / 2)
f_smooth = f[n_smooth:-n_smooth]
P_F = np.zeros([n_i, f.size])
for rep in range(n_i):
    data = np.loadtxt(on_file.format(rep), skiprows=2, delimiter=',')
    U = np.zeros(int(f_pico * Dt))
    for a_id, s_hat in zip(range(n_a), sensitivities):
        U += np.abs(np.fft.fft(data[:,1+a_id]) / (s_hat * f_pico))**2
    P_F[rep] = (P_F_prefactor / f * Gamma(f) / amplification[0]**2
                * U[(f * Dt).astype(int)]) / Dt
P_F = np.cumsum(P_F, axis=1) / (2 * n_smooth)
P_F = P_F[:, 2*n_smooth:] - P_F[:, :-2*n_smooth]
P_F_on_bar = np.nanmean(P_F, axis=0)
P_F_on_err = np.nanstd(P_F, axis=0) / np.sqrt(n_i)

# Estimate power spectral density of base-normal force of off-plate impacts
P_F = np.zeros([n_i, f.size])
for rep in range(n_i):
    data = np.loadtxt(off_file.format(rep), skiprows=2, delimiter=',')
    U = np.zeros(int(f_pico * Dt))
    for a_id, s_hat in zip(range(n_a), sensitivities):
        U += np.abs(np.fft.fft(data[:,1+a_id]) / (s_hat * f_pico))**2
    P_F[rep] = (P_F_prefactor / f * Gamma(f) / amplification[1]**2
                * U[(f * Dt).astype(int)]) / Dt
P_F = np.cumsum(P_F, axis=1) / (2 * n_smooth)
P_F = P_F[:, 2*n_smooth:] - P_F[:, :-2*n_smooth]
P_F_off_bar = np.nanmean(P_F, axis=0)
P_F_off_err = np.nanstd(P_F, axis=0) / np.sqrt(n_i)

# Calculate typical systematic error up to 100 kHz and save it to a text file
id_compare = (f_smooth < 1e5)
P_F_thy_compare = np.interp(f_smooth[id_compare], centre_freqs, P_F_thy_bar)
relative_error = P_F_on_bar[id_compare] / P_F_thy_compare - 1
sigma = np.sqrt(np.mean(np.log(1 + relative_error)**2))
frac_err = np.exp(sigma) - 1
print('Typical percentage error: {}%'.format(100 * frac_err))
np.savetxt(output + "/P_F_error.txt",
           np.vstack([f_smooth[id_compare], relative_error]).T,
           fmt='%10.6f', delimiter=',', comments='',
           header = (  '          f,     error\n'
                     + '       (Hz),          '))           

# Produce plots
def error_region(ax, x, y, yerr, color):
    ax.plot(x, y, color=color)
    ax.plot(x, y + yerr, color=color, alpha=0.5)
    ax.plot(x, y - yerr, color=color, alpha=0.5)
    ax.fill_between(x, y - yerr, y + yerr, color=color, alpha=0.25)

fig = plt.figure(figsize=[4, 2.5])
ax = fig.add_axes([0.2, 0.2, 0.75, 0.75],
                  xscale='log', yscale='log', xlim=[1e3, 1.2e5])
error_region(ax, centre_freqs, P_F_thy_bar, P_F_thy_err, [0, 0, 0])
error_region(ax, f_smooth, P_F_on_bar, P_F_on_err, [0.35, 0.7, 0.9])
error_region(ax, f_smooth, P_F_off_bar, P_F_off_err, [0.0, 0.6, 0.5])
ax.set_xlabel(r'$f$ (Hz)', fontsize=8)
ax.set_ylabel(r'$|\tilde{F}(f)|^2$ (N$^2$ s$^2$)', fontsize=8)
ax.tick_params('both', labelsize=8)
plt.show()

function [x, y, u_bar, v_bar, u_err, v_err] = surface_piv()
% SURFACE_PIV Performs particle image velocimetry on the flow surface
% 

% Frame rate (Hz) and number of frames
frame_rate = 1000; n_f = 1000;
% PIV performed between x_min and x_max m upslope, at a resolution Dx m.
x_min = 0.05; x_max = 1; Dx = 0.02;
% PIV performed across channel width W m, at a resolution Dy m.
W = 0.2; Dy = 0.02;
% Channel incline and reservoir gap width
grad = 0.46; gap = 20;
% Gravitational acceleration (m / s^2)
g = 9.81;
% Volume flux of beads per unit width (m^2 / s)
q = 9.2 / (2500 * 0.6);
 
% Calibrate to calculate geometrical parameters linking pixels to distances
p_fit = calibrate_piv(grad, gap);
% Vertical and horizontal pixel positions of the channel's end and centre
px0 = 1280; py0 = 200;
% Vertical pixel position as a function of upslope distance (m)
px = @(x) px0 - (atan(x / p_fit(3) + tan(p_fit(1))) - p_fit(1)) / p_fit(2);
% Upslope extent (m) of a pixel, as a function of its vertical position
dx = @(px) p_fit(3) * p_fit(2) * cos(p_fit(1) + p_fit(2) * (px0 - px)).^-2;
% Cross-slope extent (m) of a pixel, as a function of its vertical position
dy = @(px) p_fit(3) * p_fit(2) ./ cos(p_fit(1) + p_fit(2) * (px0 - px));

% Folder and filename 
folder = ['/media/matt/Seagate Expansion Drive/Experimental_data/', ...
          'Inclination_0.46/Gap_20_plates/overhead/frames'];
im_filename = @(f_id) sprintf('%s/frame_%04d.Png', folder, f_id);

% Number of windows, vertically and horizontally, in which to perform PIV
n_x = floor((1 + eps) * (x_max - x_min) / Dx); n_y = floor(W / Dy);
% Upslope distances (m) of centres of windows in which to perform PIV
x_c = repmat(x_min + Dx/2:Dx:x_max - Dx/2, [1, n_y]);
% Cross-slope distances (m) of centres of windows in which to perform PIV
y_c = repelem(-(W - Dy)/2:Dy:(W - Dy)/2, n_x); 
% Number of windows which to perform PIV
n_c = length(x_c);
% Vertical and horizontal pixel positions at which to perform PIV
px_c = round(px(x_c));
py_c = py0 + round(y_c ./ dy(x_c));
% Upslope and cross-slope extents (m) of a pixel at each position
dx_c = dx(double(px_c));
dy_c = dy(double(px_c));
% Vertical and horizontal half-extents (px) of windows to correlate
Dpx = floor(0.5 * Dx ./ dx_c);
Dpy = floor(0.5 * Dy ./ dy_c);

% Set up matrices of downslope and cross-slope velocities for each window
u = nan * ones(n_f, n_c);
v = nan * ones(n_f, n_c);
% Set up cell arrays of image patches to be correlated
patch0 = cell(n_c, 1);
patch = cell(n_c, 1);
% Using the specified windows, store patches of the first frame
im0 = imread(im_filename(0));
for c_id = 1:n_c
    patch0{c_id} = im0(px_c(c_id) + (-Dpx(c_id):Dpx(c_id)), ...
                       py_c(c_id) + (-Dpy(c_id):Dpy(c_id)), 1);
    patch0{c_id} = patch0{c_id} - mean(patch0{c_id}(:));
end
% Iterating over frames, ...
tic;
for f_id = 1:n_f-1
    % read each new frame as an image.
    im = imread(im_filename(f_id));
    % Iterating over the windows of analysis, ...
    for c_id = 1:n_c
        % store the patch of the new frame for the window, ...
        patch{c_id} = im(px_c(c_id) + (-Dpx(c_id):Dpx(c_id)), ...
                         py_c(c_id) + (-Dpy(c_id):Dpy(c_id)), 1);
        patch{c_id} = patch{c_id} - mean(patch{c_id}(:));
        % calculate the cross-correlation of the new and old patches, ...
        corr_map = xcorr2(patch{c_id}, patch0{c_id});
        % and find both the pixel maximising the cross-correlation...
        [~, ind] = max(corr_map(:));
        [U, V] = ind2sub(size(corr_map), ind);
        % and the sub-pixel position of the underlying maximum.
        % If the maximum pixel is at the top or bottom of the window, ...
        if (or(U <= 2, U >= size(corr_map, 1) - 2))
            % use the 1-point estimate to avoid out-of-range errors.
            dU = 0;
        % If the vertical cross-correlation rapidly decreases to noise, ...
        elseif (min(corr_map(U + [-1,1], V)) < 0.5 * corr_map(U, V))
            % use a 3-point Gaussian estimate.
            dU = 0.5 * (log(corr_map(U + 1, V)) - log(corr_map(U - 1, V)))...
                 / (2 * log(corr_map(U, V)) - log(corr_map(U - 1, V)) - log(corr_map(U + 1, V)));
        % Otherwise, ...
        else
            % use a 5-point Gaussian estimate.
            poly_x = polyfit((-2:2)', log(corr_map(U + (-2:2), V)), 2);
            dU = -0.5 * poly_x(2) / poly_x(1);
        end
        % If the maximum pixel is at the top or bottom of the window, ...
        if (or(V <= 1, V >= size(corr_map, 1) - 1))
            % use the 1-point estimate to avoid out-of-range errors.
            dV = 0;
        % Otherwise, ...
        else
            % use a 3-point Gaussian estimate.
            dV = 0.5 * (log(corr_map(U, V + 1)) - log(corr_map(U, V - 1)))...
                 / (2 * log(corr_map(U, V)) - log(corr_map(U, V - 1)) - log(corr_map(U, V + 1)));
        end
        % Calculate corresponding flow velocities downslope and cross-slope
        u(f_id, c_id) = dx_c(c_id) * frame_rate * (U + dU - (2*Dpx(c_id) + 1));
        v(f_id, c_id) = dy_c(c_id) * frame_rate * (V + dV - (2*Dpy(c_id) + 1));
        % Update the old patch, ready for the next frame
        patch0{c_id} = patch{c_id};
    end
end
toc;
% Set up upslope and cross-slope coordinates
x = x_c(1:n_x);
y = y_c(1:n_x:end);
% Calculate averages over time and their standard errors
u_bar = reshape(nanmean(u, 1), [n_x, n_y]);
v_bar = reshape(nanmean(v, 1), [n_x, n_y]);
u_err = reshape(nanstd(u, 0, 1), [n_x, n_y]) / sqrt(n_f);
v_err = reshape(nanstd(v, 0, 1), [n_x, n_y]) / sqrt(n_f);
% Plot the mean downslope velocities
figure;
surface(y, x, u_bar); hold all;
plot([-0.05, 0.05, 0.05, -0.05], [0.4, 0.4, 0.58, 0.58], '+r')
xlabel('Cross-slope distance / m')
ylabel('Up-slope distance / m')
c = colorbar;
c.Label.String = '$u$ / m s^{-1}';
c.Label.Interpreter = 'latex';
% Plot the cross-slope averaged downslope velocities in the channel centre
figure; hold all
id_mid = abs(y) < 0.5 - Dy/2;
u_W = mean(u_bar(:, id_mid), 2);
error_region(x, mean(u_bar(:, id_mid), 2), mean(u_err(:, id_mid), 2))
plot([0.58, 0.58], [min(u_bar(:)), max(u_bar(:))], '--r')
xlabel('Up-slope distance / m')
ylabel('$u$ / m s^{-1}', 'Interpreter', 'latex')
% Plot the inferred effective friction coefficient between the flow & base
figure; hold all
h = q ./ u_W;
plot(x, h)
plot([0.58, 0.58], [min(h), max(h)], '--r')
xlabel('Up-slope distance / m')
ylabel('$h$ / m')
figure; hold all
% $\mu - \tan\theta$, as inferred from acceleration, as a function of
% chi = <u^2>/<u>^2 and Ubar = <u> / u(h), for depth-average <.>
dmu = @(chi, Ubar) (0.5 * chi * sqrt(grad^2 + 1) / g * Ubar^2 * u_W(2:end) ...
                    - q ./ (Ubar * u_W(2:end).^2)) .* diff(u_W) / Dx;
% Plot inferred $\mu$ for plug flow, Poisseuille flow, and Bagnold flow
plot(x(2:end), grad + dmu(1, 1), ':k', ...
    x(2:end), grad + dmu(4/3, 0.5), '--k', ...
    x(2:end), grad + dmu(1.25, 0.6), '-.k')
% Plot gradient \tan \theta
plot([0, 1], [grad, grad], '-k')
set(gca, 'XLim', [0, 1], 'YLim', [0.4, 0.6])
xlabel('Distance upslope / m')
ylabel('Inferred \mu')
patch([0.4, 0.58, 0.58, 0.4], [0, 0, 1, 1], [0, 0, 0], 'FaceAlpha', 0.1)
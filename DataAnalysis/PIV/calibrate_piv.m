function [p_fit] = calibrate_piv(grad, gap)
% CALIBRATE_PIV Calibrates particle image velocimetry of the flow surface
% Uses images of a checkerboard pattern to calculate the geometrical
% parameters linking image pixels to distances upslope along the channel

% Location of calibration images & vertical limits of the pattern in each
folder = sprintf(['/media/matt/Seagate Expansion Drive/Experimental_data',...
                  '/Inclination_%4.2f/Gap_%02d_plates/overhead'],...
                  grad, gap);
calib_im_filename = @(rep) sprintf('%s/calibration%d.png', folder, rep);
lim = [857, 1280; 570, 850; 377, 568; 240, 375];
% Ideal checkerboard corner pattern, for convolution
check = kron(2 * ((0:3)' > 1) - 1, 2 * ((0:3) > 1) - 1);

figure; hold all;
px = [];
% For each calibration image...
for rep = 1:4
    % ...load the image, ...
    calib_im = imread(calib_im_filename(rep - 1));
    image(0, 1280 - lim(rep, 2), calib_im(lim(rep, 2):-1:lim(rep,1),:,:));
    % ...convolve with the ideal pattern and sum horizontally, ...
    calib_prof = sum(conv2(calib_im(lim(rep, 2):-1:lim(rep,1),:,1), ...
                           check, 'same').^2, 2);
    % ...and find peaks of the sum, corresponding to checkerboard lines.
    [~, px_pk] = findpeaks(calib_prof, 1280 - (lim(rep, 2):-1:lim(rep, 1)), ...
                           'MinPeakHeight', 2e5);
    % Vertical pixel coordinates are recorded upwards from the channel end
    px = [px, px_pk];
end
plot(200, px, '+r')

% Fit the observed regularly spaced line positions to theory
x = 0.01 * (1:length(px));
p_fit = fminsearch(@(p) rss_px2x(p, px, x), [pi()/6, pi()/6000, 1]);
px_thy = 0:1000;
x_thy = p_fit(3) * (tan(p_fit(1) + p_fit(2) * px_thy) - tan(p_fit(1)));
figure;
plot(px, x, '+r', px_thy, x_thy, '-k')
end

function rss = rss_px2x(p, px, x)
% Residual sum of squares between theory and observations for given params
rss = sum((x - p(3) * (tan(p(1) + p(2) * px) - tan(p(1)))).^2);
end
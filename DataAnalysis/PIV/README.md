Code to perform Particle Image Velocimetry on images of a flow's surface recorded by a high-speed camera.

Contents:
* `calibrate_piv.m` Matlab function to calibrate the PIV using images of a checkerboard pattern
* `surface_piv.m` Matlab function to perform PIV, extracting the surface velocities of a flow from high-speed camera images

Use:
* Ensure that all the required images have been captured and are accessible:
  * Frames from footage of the granular flow, on which particle image velocimetry is to be performed
  * Images of a checkerboard pattern at different positions up the channel, which will be used to calibrate distances in images
* Update definitions of variables `folder` and `calib_im_filename` in `calibrate_piv.m`; and `folder` and `im_filename` in `surface_piv.m`, to correctly refer to these images' locations.
* Update definitions of variables `lim` in `calibrate_piv.m`; and `frame_rate`, `n_f`, `W`, `g`, `Q` , `px0`, and `py0` in `surface_piv.m`, as required.
* Update definitions of variables `x_min`, `x_max`, `Dx`, and `Dy` in `surface_piv.m`, as desired.
* From Matlab, run `surface_piv`
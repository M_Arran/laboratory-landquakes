U
    p�`�Q  �                   @   s
  d Z ddlmZ ddlZddlmZ ddlZddl	Z	ddl
mZ ddlmZ e	jddd� d	Zed
 Zed Zed Zed Zed Zd\ZZde�ddd� Zd\ZZZZd\ZZZd\Z Z!Z"dZ#d\Z$Z%dZ&dZ'dd� Z(e�dde&�Z)ee(e�*ddg�e)�Z+e�*dd � e+dd�df D ��Z,ej-j-e,e'd!�e& Z.e�e'�e'e&  Z/d"Z0d#Z1d$\Z2Z3Z4d%d&d'd(gZ5d)Z6d*�7ee�Z8e�9ee2e  ��:e;�Z<ee2e<  Zdej= e e2d  d+ Z>de ej=e�?e�  Z@d,d-� ZAd.d/� ZBd=d1d2�ZCd3d4� ZDd5d6� ZEd7d8� ZFd>d;d<�ZGdS )?a   
Python3 module to calibrate measurements of P_F
(the power spectrum of the basal force applied by the flow)

Functions:

hertz_ode(s, t):
    Function for non-dimensional Hertzian time-evolution

theoretical_predictions():
    Calculate the theoretical energy spectral density imparted by an impact
    
calculate_noise_ESD():
    Calculate the energy spectral density associated with accelerometer noise

plot_example():
    Plot an example of post-impact growth & decay of the energy spectral density

analyse_impact(rep, ESD_noise):
    Analyse the accelerations measured after an impact

analyse_impacts():
    Analyse all the impacts and save summary statistics of ESD_0 and k

calibration(f, P, fM, alpha):
    Prefactor converting total plate energy spectral density to measured values
    Input: numpy array f of frequencies
           float P for the proportion of plate energy in vertical motion
           float fM for accelerometer low-pass corner frequency
           float alpha for accelerometer low-pass filter exponent
    Output: numpy array of ln(P / (1 + (f / fM)^alpha))

calibration_output():
    Calibrate the values P, fM, alpha that best-fit measurements to theory

calculate_synthetics(f_min, f_max, Q, true_ESD, B=4.0, C=1.0):
    Test analysis methods on synthetic acceleration data
�    )�
namedtupleN)�	curve_fit)�odeint�ignorezMean of empty slice)�messagezP/media/matt/Seagate Expansion Drive/Experiment/Experimental_data/P_F_calibrationz	/zero.txtz/calibration_{:02d}.txtz(/Decay_fig/impact{:02d}_acceleration.pdfz+/Decay_fig/impact{:02d}_f0={:.1e}Hz_ESD.pdf�/calibration.txt)�      @�      �?�   �   g     �1@�      �?)g����MbP?g   l/V-Bi�	  gq=
ףp�?)g����Mb`?g
ףp=
�?皙�����?)g   �vHGBix  g���(\��?�   )g��Q��#@r	   �{�G�z�?i   c                 C   s,   t �| d | d dk r$| d  d ndg�S )z�
    Helper function for the simulation of a nondimensional Hertzian impact
    
    Args:
        s: numpy array for displacement and velocity
        t: float for time
    
    Returns:
        array of velocity and acceleration
    �   r   �      �?)�np�array)�s�t� r   �b/home/matt/Documents/IPGP_Postdoc/Experiment/laboratory-landquakes/DataAnalysis/P_F_calibration.py�	hertz_odeU   s    r   �   �����c                 C   s"   g | ]}|d k r| d nd �qS )r   r   r   )�.0r   r   r   r   �
<listcomp>d   s     r   )�n�   g    ��A)g�������>g�����ư>��   g���S�{?g|,}��v?gE���V	v?g�A�L��z?g�������?zcmor{0:3.1f}-{1:3.1f}�      �?c            	         s�  t d� t�g ���jtk rntj�� } tj�� }| dd|  k rt��t�dt�	d| d  |d  � ���qt�	dt
 t �t��� }dt dtd   t�	td � }dtj t td  ��|d  | d � �d ||d   d	 �ttd  d
dtd    }t�� ���fdd�tD ��dt t td  td   }dt t t�	tt | � }|| }|t�	|t � }t d� ||fS )a�  
    Calculate the theoretical energy spectral density imparted by an impact
    Assumes:
    - a vertical impact on fixed beads in a hexagonal packing
    - impacting bead position uniformly distributed
    - interaction force entirely Hertzian and normal
    - fixed beads only transmit vertical force to the plate, and then perfectly
    - fixed beads have no effect on plate response
    - plate response perfectly described by Kirchoff-Love theory
    - accelerometers have point locations on the plate
    - accelerometer frequency reponse is perfectly flat
    Calculations are based on the paper's equations (F14) and (F15)

    Returns:
        numpy arrays of the expected ESD and its error
    z#Starting theoretical calculation...r   r   gUUUUUU�?r
   gUUUUUU�?�   g�������?g�������?�   c              	      sF   g | ]>}t �|� tt �t�d  �t ���� �  � d   �� �qS )r
   )r   �interp�f�abs�F_f�cos�mean)r   �f0�ZL_iZT_iZangle_iZm_br   r   r   �   s   ��z+theoretical_predictions.<locals>.<listcomp>r   z!Theoretical calculations finished)�printr   r   �size�n_i�random�rand�appendZarcsin�sqrt�g�h0r'   �E_b�nu_b�r_b�pi�rho_b�E_p�h_p�nu_p�centre_freqs�rho_p�X�Y�wavelet_widths)	�x�yZv_iZK_i�DZE_thyZSD_thy�ESD_thy_bar�ESD_thy_errr   r*   r   �theoretical_predictions~   s2    



"�"��rF   c                  C   s�   t d� tjtddd�} t�tj| jd g�}tt	�D ]|}| dd�d| f t
| t  }tt�|tt�d  }|t�|�d t	 7 }tt tj|ddd	� dtj t d    S dS )
z�
    Calculate the energy spectral density associated with accelerometer noise
    
    Returns:
        numpy array of the energy at each pre-defined Gabor wavelet
    z  Calculating noise levelr
   �,�ZskiprowsZ	delimiterr   Nr   �c   �Zaxis)r+   r   �loadtxt�zero_filename�zerosr<   r,   �shape�range�n_a�sensitivity�amplification�wt_pf�pywt�cwt�scales�waveletr%   r=   r:   Z
percentiler7   )�data�U�a_id�acceleration�coeffr   r   r   �calculate_noise_ESD�   s     �r]   Fc              
   C   s�  t d�| �� tjt�| �ddd�}t|dd�df  }t�tj|j	d g�}t
t�D ]t}|dd�d| f t| t  }tj|ddd� |ddd� d	d
� tt�|tt�d  }|t�|�d t 7 }qVt�d� t�d� t�d�| �� t�t�| �� t��  tjt�tj� }	tjt�tj� }
t
|j	d �D �]x}t�� }|jddddgdd�}t t! ||  dtj" t|  d  }t�#|ddt$ � �}|t�#||d� || k � }|�%|dd| � |dd| � � t&t�'t(�t|  t�'t�)|| ||  �� �}|d|  |k �r�|| }t�)|||� �}t�*t�|| �|||� |t$  g�j+}tj,j-||dd�d }||�.|� d �/� t�)d�d k �r�dt�0|d � |	|< |d  |
|< |�%|||g t�0|ddgdd�f �.|��d� |�rj|�1dd� |�2ddddg� d}|j3dd|dd� |j4ddd� |j5d dd� |j6d!dd"� |j7�8d#d	� n*|�9d$�| t| �� |�4d� |�5d%� t�t:�| t| �� t��  �q:|	|
fS )&ap  
    Analyse the accelerations measured after an impact
    
    Args:
        rep: integer for impact number
        ESD_noise: numpy array of the noise level
        title: boolean determining whether plots have titles
    
    Returns:
        numpy array of the energy imparted to the plate at each wavelet
        numpy array of the decay rate of that energy
    z  Analysing impact {}r
   rG   rH   Nr   r   r   r   )Z	linewidthz$t$ / sz$a$ / m s$^{-2}$z	Impact {}�333333�?�ffffff�?�log)Zyscale�Zrcond�
   r   �--kg333333@r!   r    g�������?z5$\hat{\Psi}_{f_0} = \hat{\Psi}^0_{f_0} e^{-k_{f_0}t}$r   g�dy���=r   �Zfontsize�$t$ (s)z-$|\hat{\Psi}_{f_0}|^2$ (J m$^{-2}$ Hz$^{-1}$)�both�Z	labelsizeg      пzImpact {}, $f_0$ = {:.0f} Hzz-$|\hat{\Psi}_{f_0}|^2$ / J m$^{-2}$ Hz$^{-1}$);r+   �formatr   rK   �filename�t_prefixrM   r<   r,   rN   rO   rP   rQ   rR   �plt�plotrS   rT   rU   rV   rW   r%   �xlabel�ylabel�titleZsavefig�accel_plotname�close�nan�ones�figure�add_axesr=   r:   r7   Zargmax�id_0�semilogy�intr1   �Br`   �vstack�T�linalg�lstsq�dot�max�expZset_size_inchesZset_position�text�
set_xlabel�
set_ylabel�tick_params�yaxis�set_label_coordsZ	set_title�ESD_plotname)�rep�	ESD_noiseZpublicationrX   �timerY   rZ   r[   r\   �ESD_0�k�f_id�figZax�ESDZid_maxZid_endZn_decay�id_stZy_rZX_r�bZlabelr   r   r   �analyse_impact�   sx     $

&$��& �� �

r�   c               	   C   sV  t � \} }td� tjt�ttjg� }tjt�ttjg� }t� }t	t�D ]}t
||�\||< ||< qLtd� t�t| � t�t�ttdf�j|jd� t�d� t�d� t��  tj|dd�}tj|dd�t�t�|�jdd�� }tj|dd�}tj|dd�t�t�|�jdd�� }	t�t| |||||	g�j}
d	}d
}tjt|
||d� dS )zBAnalyse all the impacts and save summary statistics of ESD_0 and kz!Starting analysis of measurementsz!Analysis of measurements finishedr   �.�
$f_0$ (Hz)z/$|\hat{\Psi}^0_{f_0}|^2$ (J m$^{-2}$ Hz$^{-1}$)r   rJ   z�f0 / Hz, Energy Spectral Density / J m^-2 Hz^-1        , Decay rate / s^-1 
       , thy_mean  , thy_error , obs_mean  , obs_error , mean      , errorz+ % 8.0f, %.4e, %.4e, %.4e, %.4e, %.4e, %.4e)�header�fmtN)rF   r+   r   rr   rs   r-   r<   r,   r]   rO   r�   rk   �loglog�tiler{   rm   rn   �show�nanmean�nanstdr1   �isfinite�sumrz   Zsavetxt�out_filename)rD   rE   r�   r�   r�   r�   �	ESD_0_bar�	ESD_0_err�k_bar�k_errrX   r�   Zformatsr   r   r   �analyse_impacts  s@    


��   �r�   c                 C   s   t �|d| | |   �S )a�  
    Return factor converting plate energy spectral density to measured values

    Args:
        f: numpy array of frequencies at which to calculate conversion factor
        P: float for the proportion of plate energy in vertical motion
        fM: float for accelerometer low-pass corner frequency
        alpha: float for accelerometer low-pass filter exponent

    Returns:
        numpy array of ln(P / (1 + (f / fM)^alpha))
    r   )r   r`   )r$   �P�fM�alphar   r   r   �calibrationA  s    r�   c               	   C   sx  t d } tj| ddd�}|dd�df }|dd�df }|dd�df }|dd�df }|dd�d	f }|dd�d
f }|dd�df }t�t�|��d }	t�dd� ||	dd
�f D ��}
tt||	 tj|
dd�dtj	|
dd�d�d \}}}t
d�|||�� t�|�}	dtj ||	  ||	  �� }dtj ||	  ||	  �� t�|	�� � }t
d�||�� dt�ddd� }t�|||�t�t||||�� }d}d}tjddgd�}|�ddddg�}|j||| dd� |j||||  |||  ddd� |�||d � |j|||dtj t�t�  |d!dd"� |jd#d$|d%d&� |�d'� |�d(d)g� |jd*d%d&� |�d'� |� d+d,g� |j!d-d%d&� |j"�#d.d/� |j$d0d%d1� |�dd2ddg�}|j|||dtj t�t�  |d!dd"� |�||	 dtj ||	  | d � |jd3d4|d%d&� |�d'� |�d(d)g� |�d'� |j!d5d%d&� |j"�#d.d/� |j$d0d%d1� t�%�  dS )6zFCalibrate the values P, fM, alpha that best-fit measurements to theoryr   r
   rG   rH   Nr   r   r!   r   r   �   c              
   S   sB   g | ]:\}}}}t �||t j�d �  ||t j�d �   ��qS )�d   )r   r`   r.   Zrandn)r   ZThy_bZThy_eZObs_bZObs_er   r   r   r   ^  s   
��z&calibration_output.<locals>.<listcomp>rJ   )r   g     j�@r   )Zp0Zsigmaz0P/\Gamma = {:.2f} / (1 + (f / {:.1e} Hz)**{:.1f}z Quality factor {:.1f} +/- {:.1f}rb   g�����1@r   z)$\langle k_{f_0} \rangle = 2 \pi f_0 / Q$zL$\langle \hat{\Psi}^0_{f_0} \rangle = \langle \Psi^0_{f_0} \rangle / \Gamma$)Zfigsizer    r^   r_   gffffff�?Zblack)�color)r�   r�   rc   �+)ZxerrZyerrr�   Z
markersizeg     @�@g�dy���=r   rd   r`   g     @�@g     jAr�   g�-����=g��&�.!>z:$\langle \hat{\Psi}^0_{f_0}\rangle$ (J m$^{-2}$ Hz$^{-1}$)g���Q�οr   rf   rg   g333333�?g     @�@g     p�@z$$\langle k_{f_0} \rangle$ (s$^{-1}$))&�folderr   rK   Znonzeror�   r   r   r�   r�   r�   r+   rh   r7   r(   Zstdr1   r�   �aranger#   r�   rk   rt   ru   rl   Zfill_betweenZerrorbarry   r�   Z
set_xscaleZset_xlimr�   Z
set_yscaleZset_ylimr�   r�   r�   r�   r�   )Zdata_filenamerX   Zf_0rD   rE   r�   r�   r�   r�   Zid_useZ	log_ratior�   r�   �aZQ_barZQ_errZf_calibZ	ESD_calibZk_labelZ	psi_labelr�   Zax1Zax2r   r   r   �calibration_outputP  s�    
�
��
  �
��

 � �

� �$

�r�   r   r	   c           !      C   sn  d\}}}d\}	}
d� ||�}dtj | |d  d }t�dddd	g�}t| | �}d
||  �t�}t�|j|	g�}t�|j|	g�}t|	�D �]n}| ||  tj	�
t||  ��  }t�|||�}t�|j|jg�}t|
�D ]�}t�|jd�}t|tj	�
|j��D ]�\}}||d� t�d||� �tj | t�dtj |||d�  |  � t�tj | ||d�  | �  ||d�< �q|t�|t�|||�d  �d |
  }q�t|j�D �]B}t�|�dtj ||  d  }|d||   |jd||    }}t�||||�f �}t�t�|| �|||� g�j}tjj||dd�d } dt�| d � |||f< | d
  |||f< |dk�r�t�|ddd� ||ddd�f � t�|||� t�|�| ��� t�d� t�d� t�d� || �� t��  �q�q�t� t�!||	d
f�j|d� t�d� t�d� t��  t"|j#d
d�� t"dtj | |j#d
d� � dS )a�  
    Test analysis methods on synthetic acceleration data
    
    Args:
        f_min: float, the minimum frequency at which there's energy content
        f_max: float, the maximum frequency at which there's energy content
        Q: float, the quality factor for the decay of plate energy
        true_ESD(f): function for initial spectral energy density at freq f
        B: float, the normalised bandwidth of wavelets
        C: float, the normalised centre frequency of wavelets
    )g��������g-C��6?r
   )r   r   zcmor{:3.1f}-{:3.1f}r
   r    �   �(   �P   �   r   �complexNr   r   r   ra   �2   re   z$\Psi_{f_0}$ (J m$^{-2}$ s)z$f_0$ = {:.0f} Hzr�   r�   z$\Psi^0_{f_0}$ (J m$^{-2}$ s)rJ   )$rh   r   r7   r   rx   �astyperM   r,   rO   r.   r/   r�   �zipr1   Zsinr�   r%   rT   rU   r`   rz   rs   r{   r|   r}   rk   rw   r~   rm   rn   ro   r�   r�   r�   r+   r(   )!Zf_min�f_max�QZtrue_ESDry   �CZt0�dtZt_maxZn_rZn_srW   rS   r)   Zn0rV   r�   r�   r�   Zfreqsr   rY   Zsampler�   r$   Zphir�   r�   r�   Zid_enrB   r>   r�   r   r   r   �calculate_synthetics�  sV    
 $"� �* $"
$ 



r�   )F)r   r	   )H�__doc__�collectionsr   Znumpyr   Zmatplotlib.pyplotZpyplotrk   rT   �warningsZscipy.optimizer   Zscipy.integrater   �filterwarningsr�   rL   ri   rp   r�   r�   ry   r�   r�   r<   r6   r4   r8   r5   r:   r>   r?   r9   r=   r;   r-   r2   r3   Zhertz_dtZNfftr   r   r   ZsolutionZF_tZfftr&   r$   rP   r�   r�   rj   rv   rQ   rR   rh   rW   �roundr�   rx   rV   r7   rS   r1   r@   rF   r]   r�   r�   r�   r�   r�   r   r   r   r   �<module>   s\   (

 
5
T$L
Summary data resulting from the analysis of the raw experimental data.

Contents:
* `profiles` Directory containing the base-normal profiles inferred, by particle tracking velocimetry, from high-speed photography of the channel's sidewall
* `results.txt` Text file with the results of the raw experimental data's analysis, as performed by the Python module `DataAnalysis/analysis.py`

Use:
* The columns of `results.txt` are measurements, over the period of steady flow recorded by the high-speed camera, of:
    * The incline `Gradient` at which an experiment was performed
    * The release gate height `Gap` during the experiment, in the no. of mm-thick plates used
    * The index `Repeat` of the experimental repeat
    * The mass flux per unit width `q` down the channel, in kg / m s
    * The mass per unit area `m` overlying the instrumented plate, in kg / m^2
    * The effective basal friction coefficient `mu` between the flow and the plate
    * The corner frequency `f_c` of the power spectrum of the basal force exerted upon the plate, in Hz
    * The low-frequency power spectral density `P_F0` of the basal force exerted upon the plate, in N^2 s
    * The mean squared normal velocity `ve2_bar` imparted to the plate by the flow's forcing, in m^2 / s^2
    * The mean squared normal velocity `vR2_bar` that this forcing would impart in an idealised geophysical situation, in m^2 / s^2
* The data are read by the Python module `DataAnalysis/results.py`
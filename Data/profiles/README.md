Data summarising the results of particle tracking velocimetry, performed on images captured through the experimental channel's sidewall.

Contents:
* `g{grad:4.2f}_h{gap:2d}_{rep:d}.txt` Text file recording the base-normal profiles for the `rep`th experimental repeat at a channel inclination `grad`, with reservoir release gate height `gap` (mm).

Use:
* Columns are the base-normal component `z`, in units of particle diameter $d$; the relative volume fraction `phi`; the mean downstream velocity `u`, in units $\sqrt{gd}$ for gravitational acceleration $g$; and the variance `T` of the 2D particle velocities, in units $gd$.
* Data were created with the Matlab functions in `DataAnalysis/PTV` and are read by the Python module `DataAnalysis/results.py`
SETTINGS FILE FOR PICOSCOPE 4824

/* SAMPLING SETTINGS */

FILENAME_PROTOTYPE    = F:\POSTDOC_MATT\Accelero_data\Plate_isolation\on_plate
	/* Prototype will be post-fixed by _[id].txt */

N_CHANNELS  	      = 8	// Total number of channels of device
TIME_UNITS 	      = us	// Use SI units fs, ps, ns, us, ms, s
SAMPLE_INTERVAL	      = 4	// In above-specified time units
N_SAMPLES_PRETRIGGER  = 250	// No. of samples recorded pre-trigger
N_SAMPLES_POSTTRIGGER = 9750	// No. of samples streamed post-trigger
AUTOSTOP 	      = TRUE	// Stop after above-specified number
BUFFER_LENGTH 	      = 10000	// No. of samples in streaming buffer
DOWNSAMPLE_RATIO      = 1
DOWNSAMPLE_MODE       = NONE	// NONE, MAX_MIN, FIRST, or AVERAGE
	 
/* CHANNEL SETTINGS */
Channels disabled by default

CHANNEL		= A		// Channel name, A-H
  ENABLED 	= 1		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 10		// In V
  OFFSET 	= 0.0	  	// In V

CHANNEL		= B		// Channel name, A-H
  ENABLED 	= 1		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 10		// In V
  OFFSET 	= 0.0	  	// In V

CHANNEL		= C		// Channel name, A-H
  ENABLED 	= 1		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 10		// In V
  OFFSET 	= 0.0	  	// In V

CHANNEL		= D		// Channel name, A-H
  ENABLED 	= 1		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 10		// In V
  OFFSET 	= 0.0	  	// In V

CHANNEL		= E		// Channel name, A-H
  ENABLED 	= 0		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 0.5		// In V
  OFFSET 	= 0.0	  	// In V

CHANNEL		= F		// Channel name, A-H
  ENABLED 	= 1		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 2.0		// In V
  OFFSET 	= 0.0	  	// In V

CHANNEL		= G		// Channel name, A-H
  ENABLED 	= 0		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 2.0		// In V
  OFFSET 	= -1.0	  	// In V

CHANNEL		= H		// Channel name, A-H
  ENABLED 	= 0		// 1 or 0
  COUPLING 	= DC		// DC or AC
  MAX_AMPLITUDE = 10		// In V
  OFFSET 	= 0	  	// In V

/* TRIGGER SETTINGS */

TRIGGER_ENABLED	= 1		// 1 or 0
TRIGGER_CHANNEL	= A		// Channel name, A-H
THRESHOLD 	= 0.5		// In V
DIRECTION 	= RISING	// ABOVE, BELOW, RISING, FALLING, or CROSSING
DELAY 		= 0		// No. of samples after trigger before recording
MAX_WAIT 	= 0		// Wait time until automatic trigger / ms
		  		   Infinite if set to zero

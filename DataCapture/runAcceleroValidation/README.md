Code to run a validation of the accelerometers' calibration

Contents:
* `on_settings.txt` Settings file for `record_picoscope.exe`, for an on-plate impact
* `off_settings.txt` Settings file for `record_picoscope.exe`, for an off-plate impact

Use:
* Run `record_picoscope.exe on_settings.txt`, before dropping a glass bead onto a random point on the plate, from a specified height, to record the accelerometers' response.
* Run `record_picoscope.exe off_settings.txt`, before dropping a glass bead onto a random point just outside the plate, from a specified height, to record the accelerometers' response.
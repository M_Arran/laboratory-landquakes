﻿# Program controlling the Kistler Charge Amplifier Type 5073

# Written using the manual at www.kistler.com/fr/produit/type-5073a/, section 10
# Defines functions for communication over an RS-232 connection
# Sets up continuous measurement of three sensor channels, with output via DN15

# User settings
port_name = '/dev/ttyS0'
run_time = 0
max_value = [100., 100., 100.]
sensitivity = [1., 1., 1.]
max_frequency = 0
external_trigger = 0
streaming = 0

import serial
import time
import numpy as np
import os

# Define number of channels
n_channel = 3

# Open serial port specified above
ser = serial.Serial(port=port_name,
                    baudrate=115200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=10)

# Dictionary of command return codes
return_message = {
    '0\r\n': 'OK',
    '1\r\n': 'Unknown command',
    '2\r\n': 'Parameter outside permissible range',
    '6\r\n': 'Channel is not available',
    '7\r\n': 'Checksum error',
    '18\r\n': 'Device is measuring via external input',
    '24\r\n': 'Warning: change in the charge capacitor configuration'
}    

# Dictionary of mode codes
mode = {
    '0': 'Reset',
    '1': 'Measurement'
}
# Dictionary of unit codes
units = {
    '1': 'N',
    '2': 'kN',
    '3': 'g-force',
    '4': 'kg-force',
    '5': 'bar',
    '6': 'kPa',
    '7': 'MPa',
    '8': 'M.U.',
    '9': 'g-acceleration',
    '10': 'm/s^2',
    '11': 'Nm',
    '12': 'Ncm',
    '13': 'pC/mV',
    '14': 'microns/metre',
    '15': 'mm',
    '16': 'A',
    '17': 'V',
    '101': 'lb-force',
    '102': 'psi',
    '103': 'lb-in',
    '104': 'lb-ft',
    '105': 'pC/mV',
    '107': 'g-acceleration',
    '108': 'ft/s^2'
}    

# Dictionary of peak detection modes
peak_mode = {
    '0': 'Off',
    '1': 'Maximum',
    '2': 'Minimum',
    '3': 'Average of extrema'
}

# Dictionary of jump mode codes
jump_mode = {
    '0': 'Off',
    '1': 'On'
}

# Dictionary of jump mode codes
stream_mode = {
    '0': 'Off',
    '3': 'On'
}

# Read ID of device
def read_ID():
    print('Reading device ID:')
    # Command: Initialise reading of ID string
    ser.write(':123\r\n'.encode())
    # Validation: should return '8,[n_char],[checksum]\r\n'
    id_spec = ser.readline().decode().split(',')
    if((len(id_spec) != 3) or not id_spec[1].isdigit()):
        print('Error requesting device ID')
        exit()
        
    # Command: Send ID string
    ser.write(':124\r\n'.encode())
    # Validation: should return string, length n_char, + '\r\n'
    # format '&1,[device type],&2,[serial no.],
    #         &3,[calibration date: mmyyyy],&4,[firmware],&0\r\n'
    id_string = ser.readline().decode()
    id_strings = id_string.split(',')
    if((len(id_string) != int(id_spec[1]) + len('\r\n'))
       or (len(id_strings) != 9)):
        print('Error reading device ID')
        exit()
        
    # Print device ID
    print('  Device type ' + id_strings[1])
    print('  Serial no. ' + id_strings[3])
    print('  Last calibrated ' + id_strings[5][:-4]
          + '/' + id_strings[5][-4:])
    print('  Firmware version ' + id_strings[7])
    print('', flush=True)

# Read device settings
def read_settings():
    print('Reading device settings:')
    # Command: Initialise reading of settings string
    ser.write(':119\r\n'.encode())
    # Validation: should return '8,[n_char],[checksum]\r\n'
    settings_spec = ser.readline().decode().split(',')
    if((len(settings_spec) != 3) or not settings_spec[1].isdigit()):
        print('Error requesting device settings')
        exit()
        
    # Command: Send settings string
    ser.write(':120\r\n'.encode())
    # Validation: should return string, length n_char, + '\r\n'
    # format '!9,[measure status off/on: 0/1],'
    #        + {'!13,[range: 1 or 2],' for each channel}
    #        + {'!18,[range 1],!18,[range 2],' for each channel}
    #        + {'!19,[sensitivity],' for each channel}
    #        + '!20,[p1],[p2],[0/5],1,'
    #        #  for low-pass filter off/on at p1*10^p2 Hz
    #        + {'!22,[peak detector mode],' for each channel}
    #        + {'!24,[measurement unit],' for each channel}
    #        + {'!36,[jump correction off/on: 0/1],' for each channel}
    #        + {'!43,[output offset],' for each channel}
    #        + '!60,[0/no. of channel in overflow],'
    #        + '!73,[0/3],3\r\n'
    #        # for streaming off/on, with this status unsaved or saved
    setting_string = ser.readline().decode()
    settings = setting_string.split(',')
    if((len(setting_string) != int(settings_spec[1]) + len('\r\n'))
       or (len(settings) != 12 + 16*n_channel)):
        print('Error reading device settings. Raw string:')
        print(setting_string)
        exit()
        
    # Print device settings
    print('  Mode : ' + mode.get(settings[1]))
    for channel in range(n_channel):
        print('  Channel {0:d}:'.format(channel+1))
        unit = units.get(settings[8+10*n_channel+2*channel])
        if(settings[3 + 2*channel] == '1'):
            print('    Range: {} {}'
                  .format(settings[3+2*n_channel+4*channel], unit))
        else:
            print('    Range: {} {}'
                  .format(settings[5+2*n_channel+4*channel], unit))
        print('    Sensitivity: {} pC/{}'
              .format(settings[3+6*n_channel+2*channel], unit))
        print('    Output offset: {0:f} mV'
              .format(int(settings[8+14*n_channel+2*channel]) * 1.171875))
        print('    Reset jump correction: '
              + jump_mode.get(settings[8+12*n_channel+2*channel]))
        print('    Peak detection: '
              + peak_mode.get(settings[8+8*n_channel+2*channel]))
    if(settings[5 + 8*n_channel] == '0'):
        print('Low pass filter off')
    else:
        print('Low pass filter at {}x10^{} Hz'
              .format(settings[3 + 8*n_channel],
                      settings[4 + 8*n_channel]))
    if(settings[8 + 16*n_channel] == '0'):
        print('No channels overflowing')
    else:
        print('Overflow on channel ' + settings[8 + 16*n_channel])
    print('Streaming: ' + stream_mode.get(settings[10 + 16*n_channel]))
    print('', flush=True)

# Set output offset to zero volts
def zero_offset(channel):
    # Command: Switch to relevant channel
    ser.write(':8,0,{0:d}\r\n'.format(channel).encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching to channel {0:d}:'.format(channel))
        print('  ' + message)
        exit()
        
    # Command: Set channel's output offset to zero volts
    ser.write((':43,+0\r\n').encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error setting channel ' + str(channel) + ' offset to zero:')
        print('  ' + message)
        exit()

# Set range, with units Newtons and max. sensitivity
def set_range(channel, max_value, sensitivity):
    # Command: Switch to relevant channel
    ser.write(':8,0,{0:d}\r\n'.format(channel).encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching to channel {0:d}:'.format(channel))
        print('  ' + message)
        exit()
        
    # Command: Set units to Newtons
    ser.write(':24,1\r\n'.encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error setting channel {0:d} units to Newtons:'.format(channel))
        print('  ' + message)
        exit()
        
    # Command: Switch to first measurement range
    ser.write(':13,1\r\n'.encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching channel {0:d} range to 1:'.format(channel))
        print('  ' + message)
        exit()
        
    # Command: Set first measurement range
    ser.write(':18,{0:.4f}\r\n'.format(max_value).encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error setting channel {0:d} range 1:'.format(channel))
        print('  ' + message)
        exit()
        
    # Command: Set sensitivity
    ser.write(':19,{0:.4f}\r\n'.format(sensitivity).encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error setting channel {0:d} sensitivity:'.format(channel))
        print('  ' + message)
        exit()

# Set up low pass filter
def set_up_filter(f):
    # Calculate nearest frequency representable as a x 10^b
    # (where a is in {1, 2, ...255} & b is in {0, 1, 2, 3})
    if f <= 255:
        a, b = max(1, round(f)), 0
    elif f >= 100000:
        a, b = min(255, round(f / 1000)), 3
    else:
        exponent = int(np.log(f) / np.log(10))
        significand = f / 10.**exponent
        if(significand < 2.575):
            a = min(255, round(significand * 100))
            b = exponent - 2
        else:
            a = round(significand * 10)
            b = exponent - 1
            # Command: Switch on 5th order Butterworth filter
    ser.write(':20,{0:d},{1:d},5,1\r\n'.format(a, b).encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error setting up low-pass filter:')
        print('  ' + message)
        exit()

# Turn off low-pass filter
def turn_off_filter():
    # Command: Switch low-pass filter to off
    ser.write(':20,1,0,0,1\r\n'.encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching off low-pass filter:')
        print('  ' + message)
        exit()

# Turn off continuous streaming mode
def turn_off_streaming():
    # Command: Switch streaming to off
    ser.write(':73,0,3,0\r\n'.encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching streaming off:')
        print('  ' + message)
        exit()

# Wait until the amplifier receives an external trigger
def wait_for_trigger(on):
    # Monitor serial port for an event code
    event_code = ser.read_until(b'\r\n').decode()
    # Validation: Should return 3-channel 'Remote Measure' code
    if ((on and event_code != '30,1,15,8,0\r\n')
        or (not on and event_code != '30,1,15,80,0\r\n')):
        print('Unexpected event code while waiting for trigger:')
        print(event_code)
        exit()
        
# Start measurement, allowing charge to build up
def start_measurement():
    # Command: Switch mode to 'Measure'
    ser.write(':9,1\r\n'.encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching mode to \'Measure\':')
        print('  ' + message)
        exit()

# Stop measurement, resetting charge to zero
def stop_measurement():
    # Command: Switch mode to 'Reset'
    ser.write(':9,0\r\n'.encode())
    # Validation: May receive streamed values '###;'
    #             but should then return OK code
    string_read = ';'
    while '\r\n' not in string_read:
        string_read = ser.read_until(b';'or b'\n').decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching mode to \'Reset\':')
        print('  ' + message)
        exit()

# Read current value of specified channel
def measure_channel(channel):
    # Command: Switch to relevant channel
    ser.write((':8,0,{0:d}\r\n').format(channel).encode())
    # Validation: Should return OK code
    string_read = ser.readline().decode()
    message = return_message.get(string_read,
                                 'Unrecognised error code: ' + string_read)
    if(message != 'OK'):
        print('Error switching to channel {0:d}:'.format(channel))
        print('  ' + message)
        exit()
        # Command: Request current value of current channel
    ser.write(':72,3\r\n'.encode())
    # Return: Message format '[+-]#####;'
    measurement_string = ser.read_until(b';').decode()
    return int(measurement_string[0:-1])

# Code to set settings, validate them, and take measurements
print('')
stop_measurement()
read_ID()

print('Setting settings')
for channel_id in range(n_channel):
    zero_offset(channel_id + 1)
    set_range(channel_id + 1,
              max_value[channel_id],
              sensitivity[channel_id])
if max_frequency == 0:
    turn_off_filter()
else:
    set_up_filter(max_frequency)
if streaming == 0:
    turn_off_streaming()
else:
    set_up_streaming(streaming)
print('  Done')

read_settings()
print('')
print('run time = {0:f} s'.format(run_time))

if external_trigger:
    print('Waiting for external trigger...', flush=True)
    wait_for_trigger(1)
else:
    print('Starting force measurement', flush=True)
    start_measurement()
initial_value = np.array([0,0,0])
for channel_id in range(n_channel):
    initial_value[channel_id] = measure_channel(channel_id + 1)
    
time.sleep(run_time)

final_value = np.array([0,0,0])
for channel_id in range(n_channel):
    final_value[channel_id] = measure_channel(channel_id + 1)
if external_trigger:
    print('Run time finished. Switch off external trigger', flush=True)
    wait_for_trigger(0)
else:
    stop_measurement()
ser.close()
print('Finished force measurement')
print('  Drift = ' + str(final_value - initial_value))
print('', flush=True)

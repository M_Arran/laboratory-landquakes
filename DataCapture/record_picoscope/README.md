Code to build the `record_picoscope.exe` executable, to read data from a Pico Technology PicoScope 4824

Contents:
* `printErrorMessage.h` C header file for printing error messages associated with PicoScope error codes
* `record_picoscope.c` C source code for the executable
* `makefile` Makefile to build the executable

Windows:
* Download and run the relevant SDK installer from https://www.picotech.com/downloads
* Build the executable with Windows Virtual Studio

Linux:
* Install the drivers as described at https://www.picotech.com/downloads/linux
* Check that the makefile points to the correct include and library directories
* Run the makefile

To modify the code, consult the [Programmer's Guide](https://www.picotech.com/download/manuals/picoscope-4000-series-a-api-programmers-guide.pdf) and the [example code](https://github.com/picotech/) provided by Pico Technology
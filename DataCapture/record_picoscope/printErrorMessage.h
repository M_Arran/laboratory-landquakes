/* C header file for processing error codes from Pico Technology PicoScope */

void printErrorMessage(PICO_STATUS status)
/* Print to standard output the error */
{
  switch(status) {
    case 0x00000000UL:
      printf("\t The PicoScope is functioning correctly.\n");
      break;
    case 0x00000001UL:
      printf("\t An attempt has been made to open more than <API>_MAX_UNITS.\n");
      break;
    case 0x00000002UL:
      printf("\t Not enough memory could be allocated on the host machine.\n");
      break;
    case 0x00000003UL:
      printf("\t No Pico Technology device could be found.\n");
      break;
    case 0x00000004UL:
      printf("\t Unable to download firmware.\n");
      break;
    case 0x00000005UL:
      printf("\t The driver is busy opening a device.\n");
      break;
    case 0x00000006UL:
      printf("\t An unspecified failure occurred.\n");
      break;
    case 0x00000007UL:
      printf("\t The PicoScope is not responding to commands from the PC.\n");
      break;
    case 0x00000008UL:
      printf("\t The configuration information in the PicoScope is corrupt or missing.\n");
      break;
    case 0x00000009UL:
      printf("\t The picopp.sys file is too old to be used with the device driver.\n");
      break;
    case 0x0000000AUL:
      printf("\t The EEPROM has become corrupt, so the device will use a default setting.\n");
      break;
    case 0x0000000BUL:
      printf("\t The operating system on the PC is not supported by this driver.\n");
      break;
    case 0x0000000CUL:
      printf("\t There is no device with the handle value passed.\n");
      break;
    case 0x0000000DUL:
      printf("\t A parameter value is not valid.\n");
      break;
    case 0x0000000EUL:
      printf("\t The timebase is not supported or is invalid.\n");
      break;
    case 0x0000000FUL:
      printf("\t The voltage range is not supported or is invalid.\n");
      break;
    case 0x00000010UL:
      printf("\t The channel number is not valid on this device or no channels have been set.\n");
      break;
    case 0x00000011UL:
      printf("\t The channel set for a trigger is not available on this device.\n");
      break;
    case 0x00000012UL:
      printf("\t The channel set for a condition is not available on this device.\n");
      break;
    case 0x00000013UL:
      printf("\t The device does not have a signal generator.\n");
      break;
    case 0x00000014UL:
      printf("\t Streaming has failed to start or has stopped without user request.\n");
      break;
    case 0x00000015UL:
      printf("\t Block failed to start - a parameter may have been set wrongly.\n");
      break;
    case 0x00000016UL:
      printf("\t A parameter that was required is NULL.\n");
      break;
    case 0x00000017UL:
      printf("\t The current functionality is not available while using ETS capture mode.\n");
      break;
    case 0x00000018UL:
      printf("\t No data is available from a run block call.\n");
      break;
    case 0x00000019UL:
      printf("\t The buffer passed for the information was too small.\n");
      break;
    case 0x0000001AUL:
      printf("\t ETS is not supported on this device.\n");
      break;
    case 0x0000001BUL:
      printf("\t The auto trigger time is less than the time it will take to collect the pre-trigger data.\n");
      break;
    case 0x0000001CUL:
      printf("\t The collection of data has stalled as unread data would be overwritten.\n");
      break;
    case 0x0000001DUL:
      printf("\t Number of samples requested is more than available in the current memory segment.\n");
      break;
    case 0x0000001EUL:
      printf("\t Not possible to create number of segments requested.\n");
      break;
    case 0x0000001FUL:
      printf("\t A null pointer has been passed in the trigger function or one of the parameters is out of range.\n");
      break;
    case 0x00000020UL:
      printf("\t One or more of the hold-off parameters are out of range.\n");
      break;
    case 0x00000021UL:
      printf("\t One or more of the source details are incorrect.\n");
      break;
    case 0x00000022UL:
      printf("\t One or more of the conditions are incorrect.\n");
      break;
    case 0x00000023UL:
      printf("\t The driver\'s thread is currently in the <API>Ready callback function and therefore the action cannot be carried out.\n");
      break;
    case 0x00000024UL:
      printf("\t An attempt is being made to get stored data while streaming. Either stop streaming by calling <API>Stop, or use <API>GetStreamingLatestValues.\n");
      break;
    case 0x00000025UL:
      printf("\t Data is unavailable because a run has not been completed.\n");
      break;
    case 0x00000026UL:
      printf("\t The memory segment index is out of range.\n");
      break;
    case 0x00000027UL:
      printf("\t The device is busy so data cannot be breaked yet.\n");
      break;
    case 0x00000028UL:
      printf("\t The start time to get stored data is out of range.\n");
      break;
    case 0x00000029UL:
      printf("\t The information number requested is not a valid number.\n");
      break;
    case 0x0000002AUL:
      printf("\t The handle is invalid so no information is available about the device. Only PICO_DRIVER_VERSION is available.\n");
      break;
    case 0x0000002BUL:
      printf("\t The sample interval selected for streaming is out of range.\n");
      break;
    case 0x0000002CUL:
      printf("\t ETS is set but no trigger has been set. A trigger setting is required for ETS.\n");
      break;
    case 0x0000002DUL:
      printf("\t Driver cannot allocate memory.\n");
      break;
    case 0x0000002EUL:
      printf("\t Incorrect parameter passed to the signal generator.\n");
      break;
    case 0x0000002FUL:
      printf("\t Conflict between the shots and sweeps parameters sent to the signal generator.\n");
      break;
    case 0x00000030UL:
      printf("\t A software trigger has been sent but the trigger source is not a software trigger.\n");
      break;
    case 0x00000031UL:
      printf("\t An <API>SetTrigger call has found a conflict between the trigger source and the AUX output enable.\n");
      break;
    case 0x00000032UL:
      printf("\t ETS mode is being used and AUX is set as an input.\n");
      break;
    case 0x00000033UL:
      printf("\t Attempt to set different EXT input thresholds set for signal generator and oscilloscope trigger.\n");
      break;
    case 0x00000034UL:
      printf("\t An <API>SetTrigger... function has set AUX as an output and the signal generator is using it as a trigger.\n");
      break;
    case 0x00000035UL:
      printf("\t The combined peak to peak voltage and the analog offset voltage exceed the maximum voltage the signal generator can produce.\n");
      break;
    case 0x00000036UL:
      printf("\t NULL pointer passed as delay parameter.\n");
      break;
    case 0x00000037UL:
      printf("\t The buffers for overview data have not been set while streaming.\n");
      break;
    case 0x00000038UL:
      printf("\t The analog offset voltage is out of range.\n");
      break;
    case 0x00000039UL:
      printf("\t The analog peak-to-peak voltage is out of range.\n");
      break;
    case 0x0000003AUL:
      printf("\t A block collection has been cancelled.\n");
      break;
    case 0x0000003BUL:
      printf("\t The segment index is not currently being used.\n");
      break;
    case 0x0000003CUL:
      printf("\t The wrong GetValues function has been called for the collection mode in use.\n");
      break;
    case 0x0000003FUL:
      printf("\t The function is not available.\n");
      break;
    case 0x00000040UL:
      printf("\t The aggregation ratio requested is out of range.\n");
      break;
    case 0x00000041UL:
      printf("\t Device is in an invalid state.\n");
      break;
    case 0x00000042UL:
      printf("\t The number of segments allocated is fewer than the number of captures requested.\n");
      break;
    case 0x00000043UL:
      printf("\t A driver function has already been called and not yet finished. Only one call to the driver can be made at any one time.\n");
      break;
    case 0x00000044UL:
      printf("\t Not used.\n");
      break;
    case 0x00000045UL:
      printf("\t An invalid coupling type was specified in <API>SetChannel.\n");
      break;
    case 0x00000046UL:
      printf("\t An attempt was made to get data before a data buffer was defined.\n");
      break;
    case 0x00000047UL:
      printf("\t The selected downsampling mode (used for data reduction) is not allowed.\n");
      break;
    case 0x00000048UL:
      printf("\t Aggregation was requested in rapid block mode.\n");
      break;
    case 0x00000049UL:
      printf("\t An invalid parameter was passed to <API>SetTriggerChannelProperties.\n");
      break;
    case 0x0000004AUL:
      printf("\t The driver was unable to contact the oscilloscope.\n");
      break;
    case 0x0000004BUL:
      printf("\t Resistance-measuring mode is not allowed in conjunction with the specified probe.\n");
      break;
    case 0x0000004CUL:
      printf("\t The device was unexpectedly powered down.\n");
      break;
    case 0x0000004DUL:
      printf("\t A problem occurred in <API>SetSigGenBuiltIn or <API>SetSigGenArbitrary.\n");
      break;
    case 0x0000004EUL:
      printf("\t FPGA not successfully set up.\n");
      break;
    case 0x00000050UL:
      printf("\t An impossible analog offset value was specified in <API>SetChannel.\n");
      break;
    case 0x00000051UL:
      printf("\t There is an error within the device hardware.\n");
      break;
    case 0x00000052UL:
      printf("\t There is an error within the device hardware.\n");
      break;
    case 0x00000053UL:
      printf("\t Unable to configure the signal generator.\n");
      break;
    case 0x00000054UL:
      printf("\t The FPGA cannot be initialized, so unit cannot be opened.\n");
      break;
    case 0x00000056UL:
      printf("\t The frequency for the external clock is not within 15%% of the nominal value.\n");
      break;
    case 0x00000057UL:
      printf("\t The FPGA could not lock the clock signal.\n");
      break;
    case 0x00000058UL:
      printf("\t You are trying to configure the AUX input as both a trigger and a reference clock.\n");
      break;
    case 0x00000059UL:
      printf("\t You are trying to congfigure the AUX input as both a pulse width qualifier and a reference clock.\n");
      break;
    case 0x0000005AUL:
      printf("\t The requested scaling file cannot be opened.\n");
      break;
    case 0x0000005BUL:
      printf("\t The frequency of the memory is reporting incorrectly.\n");
      break;
    case 0x0000005CUL:
      printf("\t The I2C that is being actioned is not responding to requests.\n");
      break;
    case 0x0000005DUL:
      printf("\t There are no captures available and therefore no data can be breaked.\n");
      break;
    case 0x0000005EUL:
      printf("\t The capture mode the device is currently running in does not support the current request.\n");
      break;
    case 0x0000005FUL:
      printf("\t The number of trigger channels is greater than 4, except for a PS4824 where 8 channels are allowed for rising/falling/rising_or_falling trigger directions.\n");
      break;
    case 0x00000060UL:
      printf("\t When more than 4 trigger channels are set on a PS4824 and the direction is out of range.\n");
      break;
    case 0x00000061UL:
      printf("\t  When more than 4 trigger channels are set and their trigger condition states are not <API>_CONDITION_TRUE.\n");
      break;
    case 0x00000104UL:
      printf("\t The device is currently connected via the IP Network socket and thus the call made is not supported.\n");
      break;
    case 0x00000105UL:
      printf("\t An incorrect IP address has been passed to the driver.\n");
      break;
    case 0x00000106UL:
      printf("\t The IP socket has failed.\n");
      break;
    case 0x00000107UL:
      printf("\t The IP socket has timed out.\n");
      break;
    case 0x00000108UL:
      printf("\t Failed to apply the requested settings.\n");
      break;
    case 0x00000109UL:
      printf("\t The network connection has failed.\n");
      break;
    case 0x0000010AUL:
      printf("\t Unable to load the WS2 DLL.\n");
      break;
    case 0x0000010BUL:
      printf("\t The specified IP port is invalid.\n");
      break;
    case 0x0000010CUL:
      printf("\t The type of coupling requested is not supported on the opened device.\n");
      break;
    case 0x0000010DUL:
      printf("\t Bandwidth limiting is not supported on the opened device.\n");
      break;
    case 0x0000010EUL:
      printf("\t The value requested for the bandwidth limit is out of range.\n");
      break;
    case 0x0000010FUL:
      printf("\t The arbitrary waveform generator is not supported by the opened device.\n");
      break;
    case 0x00000110UL:
      printf("\t Data has been requested with ETS mode set but run block has not been called, or stop has been called.\n");
      break;
    case 0x00000111UL:
      printf("\t White noise output is not supported on the opened device.\n");
      break;
    case 0x00000112UL:
      printf("\t The wave type requested is not supported by the opened device.\n");
      break;
    case 0x00000113UL:
      printf("\t The requested digital port number is out of range (MSOs only).\n");
      break;
    case 0x00000114UL:
      printf("\t The digital channel is not in the range <API>_DIGITAL_CHANNEL0 to <API>_DIGITAL_CHANNEL15, the digital channels that are supported.\n");
      break;
    case 0x00000115UL:
      printf("\t The digital trigger direction is not a valid trigger direction and should be equal in value to one of the <API>_DIGITAL_DIRECTION enumerations.\n");
      break;
    case 0x00000116UL:
      printf("\t Signal generator does not generate pseudo-random binary sequence.\n");
      break;
    case 0x00000117UL:
      printf("\t When a digital port is enabled, ETS sample mode is not available for use.\n");
      break;
    case 0x00000119UL:
      printf("\t 4-channel scopes only: The DC power supply is connected.\n");
      break;
    case 0x0000011AUL:
      printf("\t 4-channel scopes only: The DC power supply is not connected.\n");
      break;
    case 0x0000011BUL:
      printf("\t Incorrect power mode passed for current power source.\n");
      break;
    case 0x0000011CUL:
      printf("\t The supply voltage from the USB source is too low.\n");
      break;
    case 0x0000011DUL:
      printf("\t The oscilloscope is in the process of capturing data.\n");
      break;
    case 0x0000011EUL:
      printf("\t A USB 3.0 device is connected to a non-USB 3.0 port.\n");
      break;
    case 0x0000011FUL:
      printf("\t A function has been called that is not supported by the current device.\n");
      break;
    case 0x00000120UL:
      printf("\t The device resolution is invalid (out of range).\n");
      break;
    case 0x00000121UL:
      printf("\t The number of channels that can be enabled is limited in 15 and 16-bit modes. (Flexible Resolution Oscilloscopes only).\n");
      break;
    case 0x00000122UL:
      printf("\t USB power not sufficient for all requested channels.\n");
      break;
    case 0x00000123UL:
      printf("\t The signal generator does not have a configurable DC offset.\n");
      break;
    case 0x00000124UL:
      printf("\t An attempt has been made to define pre-trigger delay without first enabling a trigger.\n");
      break;
    case 0x00000125UL:
      printf("\t An attempt has been made to define pre-trigger delay without first arming a trigger.\n");
      break;
    case 0x00000126UL:
      printf("\t Pre-trigger delay and post-trigger delay cannot be used at the same time.\n");
      break;
    case 0x00000127UL:
      printf("\t The array index points to a nonexistent trigger.\n");
      break;
    case 0x00000129UL:
      printf("\t There are more 4 analog channels with a trigger condition set.\n");
      break;
    case 0x0000012AUL:
      printf("\t The condition parameter is a null pointer.\n");
      break;
    case 0x0000012BUL:
      printf("\t There is more than one condition pertaining to the same channel.\n");
      break;
    case 0x0000012CUL:
      printf("\t The parameter relating to condition information is out of range.\n");
      break;
    case 0x0000012DUL:
      printf("\t Reading the metadata has failed.\n");
      break;
    case 0x0000012EUL:
      printf("\t Writing the metadata has failed.\n");
      break;
    case 0x0000012FUL:
      printf("\t A parameter has a value out of the expected range.\n");
      break;
    case 0x00000130UL:
      printf("\t The driver does not support the hardware variant connected.\n");
      break;
    case 0x00000131UL:
      printf("\t The driver does not support the digital hardware variant connected.\n");
      break;
    case 0x00000132UL:
      printf("\t The driver does not support the analog hardware variant connected.\n");
      break;
    case 0x00000133UL:
      printf("\t Converting a channel\'s ADC value to resistance has failed.\n");
      break;
    case 0x00000134UL:
      printf("\t The channel is listed more than once in the function call.\n");
      break;
    case 0x00000135UL:
      printf("\t The range cannot have resistance conversion applied.\n");
      break;
    case 0x00000136UL:
      printf("\t An invalid value is in the max buffer.\n");
      break;
    case 0x00000137UL:
      printf("\t An invalid value is in the min buffer.\n");
      break;
    case 0x00000138UL:
      printf("\t When calculating the frequency for phase conversion, the frequency is greater than that supported by the current variant.\n");
      break;
    case 0x00000139UL:
      printf("\t The device\'s EEPROM is corrupt. Contact Pico Technology support: https://www.picotech.com/tech-support.\n");
      break;
    case 0x0000013AUL:
      printf("\t The EEPROM has failed.\n");
      break;
    case 0x0000013BUL:
      printf("\t The serial buffer is too small for the required information.\n");
      break;
    case 0x0000013CUL:
      printf("\t The signal generator trigger and the external clock have both been set. This is not allowed.\n");
      break;
    case 0x0000013DUL:
      printf("\t The AUX trigger was enabled and the external clock has been enabled, so the AUX has been automatically disabled.\n");
      break;
    case 0x00000013EUL:
      printf("\t The AUX I/O was set as a scope trigger and is now being set as a signal generator gating trigger. This is not allowed.\n");
      break;
    case 0x00000013FUL:
      printf("\t The AUX I/O was set by the signal generator as a gating trigger and is now being set as a scope trigger. This is not allowed.\n");
      break;
    case 0x00000140UL:
      printf("\t A resource has failed to initialise.\n");
      break;
    case 0x000000141UL:
      printf("\t The temperature type is out of range.\n");
      break;
    case 0x000000142UL:
      printf("\t A requested temperature type is not supported on this device.\n");
      break;
    case 0x00000143UL:
      printf("\t A read/write to the device has timed out.\n");
      break;
    case 0x00000144UL:
      printf("\t The device cannot be connected correctly.\n");
      break;
    case 0x00000145UL:
      printf("\t The driver has experienced an unknown error and is unable to recover from this error.\n");
      break;
    case 0x00000146UL:
      printf("\t Used when opening units via IP and more than multiple units have the same ip address.\n");
      break;
    case 0x00000148UL:
      printf("\t the calibration pin states argument is out of range.\n");
      break;
    case 0x00000149UL:
      printf("\t the calibration pin frequency argument is out of range.\n");
      break;
    case 0x0000014AUL:
      printf("\t the calibration pin amplitude argument is out of range.\n");
      break;
    case 0x0000014BUL:
      printf("\t the calibration pin wavetype argument is out of range.\n");
      break;
    case 0x0000014CUL:
      printf("\t the calibration pin offset argument is out of range.\n");
      break;
    case 0x0000014DUL:
      printf("\t the probe\'s identity has a problem.\n");
      break;
    case 0x0000014EUL:
      printf("\t the probe has not been identified.\n");
      break;
    case 0x0000014FUL:
      printf("\t enabling the probe would cause the device to exceed the allowable current limit.\n");
      break;
    case 0x00000150UL:
      printf("\t the DC power supply is connected; enabling the probe would cause the device to exceed the allowable current limit.\n");
      break;
    case 0x00000151UL:
      printf("\t failed to complete probe configuration.\n");
      break;
    case 0x00000152UL:
      printf("\t failed to set the callback function, as currently in current callback function.\n");
      break;
    case 0x00000153UL:
      printf("\t the probe has been verified but not know on this driver.\n");
      break;
    case 0x00000154UL:
      printf("\t the intelligent probe cannot be verified.\n");
      break;
    case 0x00000155UL:
      printf("\t the callback is null, probe collection will only start when first callback is a none null pointer.\n");
      break;
    case 0x00000156UL:
      printf("\t the current drawn by the probe(s) has exceeded the allowed limit.\n");
      break;
    case 0x00000157UL:
      printf("\t the channel range limits have changed due to connecting or disconnecting a probe the channel has been enabled.\n");
      break;
    case 0x01000000UL:
      printf("\t The time stamp per waveform segment has been reset.\n");
      break;
    case 0x10000000UL:
      printf("\t An internal erorr has occurred and a watchdog timer has been called.\n");
      break;
    case 0x10000001UL:
      printf("\t The picoipp.dll has not been found.\n");
      break;
    case 0x10000002UL:
      printf("\t A function in the picoipp.dll does not exist.\n");
      break;
    case 0x10000003UL:
      printf("\t The Pico IPP call has failed.\n");
      break;
    case 0x10000004UL:
      printf("\t Shadow calibration is not available on this device.\n");
      break;
    case 0x10000005UL:
      printf("\t Shadow calibration is currently disabled.\n");
      break;
    case 0x10000006UL:
      printf("\t Shadow calibration error has occurred.\n");
      break;
    case 0x10000007UL:
      printf("\t The shadow calibration is corrupt.\n");
      break;
    case 0x10000008UL:
      printf("\t The memory onboard the device has overflowed.\n");
      break;
  default:
    printf("Error code not recognised.\n");
  }
  return;
}

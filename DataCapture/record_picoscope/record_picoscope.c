/* C source file to read data from a Pico Technology PicoScope */

/* Include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <linux/kd.h>
#include <unistd.h>
#include <string.h>
#include "ps4000aApi.h"
#ifndef PICO_STATUS
#include <PicoStatus.h>
#endif
#include "printErrorMessage.h"

#define TRUE 1
#define FALSE 0

/* Set up formats of SETTINGS and BUFFERS formats */
typedef struct{
  PS4000A_CHANNEL channel;
  int8_t enabled;
  PS4000A_COUPLING dc_coupling;
  PICO_CONNECT_PROBE_RANGE range;
  float offset;
} CHANNEL_SETTINGS;

typedef struct{
  int8_t enabled;
  PS4000A_CHANNEL channel;
  float threshold;
  PS4000A_THRESHOLD_DIRECTION direction;
  uint32_t delay;
  int16_t autotrigger;
} TRIGGER_SETTINGS;
  
typedef struct{
  int8_t n_channels;
  uint32_t sample_interval;
  PS4000A_TIME_UNITS time_units;
  uint32_t n_samples_pretrigger;
  uint32_t n_samples_posttrigger;
  int8_t autostop;
  uint32_t buffer_length;
  int16_t downsample_ratio;
  PS4000A_RATIO_MODE downsample_mode;
  char filename_prototype[256];
  CHANNEL_SETTINGS ch[PS4000A_MAX_CHANNELS];
  TRIGGER_SETTINGS tr;
} SETTINGS;

typedef struct{
  int8_t n_channels;
  int8_t enabled[PS4000A_MAX_CHANNELS];
  int32_t n_pretrigger;
  int8_t new_data;
  int32_t n_new_samples;
  uint32_t trigger_index;
  int8_t trigger_hit;
  int8_t autostopped;
  int16_t* dev_main[PS4000A_MAX_CHANNELS];
  int16_t* dev_min[PS4000A_MAX_CHANNELS];
  int16_t* app_main[PS4000A_MAX_CHANNELS];
  int16_t* app_min[PS4000A_MAX_CHANNELS];
  int16_t* pre_main[PS4000A_MAX_CHANNELS];
  int16_t* pre_min[PS4000A_MAX_CHANNELS];
  int32_t position;
} BUFFERS;

PS4000A_TIME_UNITS prefix_to_time_units(char prefix)
/* Use first character of input to infer time unit used */
{
  switch (prefix) {
  case 'f':
    return PS4000A_FS;
  case 'p':
    return PS4000A_PS;
  case 'n':
    return PS4000A_NS;
  case 'u':
    return PS4000A_US;
  case 'm':
    return PS4000A_MS;
  case 's':
    return PS4000A_S;
  default:
    printf("\t Time unit unrecognised. Using microseconds.\n");
    return PS4000A_US;
  }
}

PS4000A_RATIO_MODE str_to_downsample_mode(char* str)
/* Convert input string to hardcoded int. for ratio mode */
{
  if (!strcmp(str, "NONE")) {
    return PS4000A_RATIO_MODE_NONE;
  }else if(!strcmp(str, "MAX_MIN")) {
    return PS4000A_RATIO_MODE_AGGREGATE;
  }else if (!strcmp(str, "FIRST")) {
    return PS4000A_RATIO_MODE_DECIMATE;
  }else if (!strcmp(str, "AVERAGE")) {
    return PS4000A_RATIO_MODE_AVERAGE;
  }else {
    printf("\t Downsampling mode not recognised. Using AVERAGE");
    return PS4000A_RATIO_MODE_AVERAGE;
  }
}

PS4000A_COUPLING str_to_coupling(char* str, int ch_id)
/* Convert input string to hardcoded int. for channel ch_id coupling mode */
{
  if(!strcmp(str, "TRUE")) {
    return TRUE;
  }else if(!strcmp(str, "FALSE")) {
    return FALSE;
  }else {
    printf("\t Channel %d DC Coupling mode unrecognised. Using FALSE", ch_id);
    return FALSE;
  }
}

PICO_CONNECT_PROBE_RANGE max_amp_to_range(float max_amp)
/* Convert user-specified max. amplitude to appropriate hardcoded int. */
{
  if (max_amp <= 0.01) {
    return PICO_X1_PROBE_10MV;
  }else if(max_amp <= 0.02) {
    return PICO_X1_PROBE_20MV;
  }else if(max_amp <= 0.05) {
    return PICO_X1_PROBE_50MV;
  }else if(max_amp <= 0.1) {
    return PICO_X1_PROBE_100MV;
  }else if(max_amp <= 0.2) {
    return PICO_X1_PROBE_200MV;
  }else if(max_amp <= 0.5) {
    return PICO_X1_PROBE_500MV;
  }else if(max_amp <= 1) {
    return PICO_X1_PROBE_1V;
  }else if(max_amp <= 2) {
    return PICO_X1_PROBE_2V;
  }else if(max_amp <= 5) {
    return PICO_X1_PROBE_5V;
  }else if(max_amp <= 10) {
    return PICO_X1_PROBE_10V;
  }else if(max_amp <= 20) {
    return PICO_X1_PROBE_20V;
  }else if(max_amp <= 50) {
    return PICO_X1_PROBE_50V;
  }else if(max_amp <= 100) {
    return PICO_X1_PROBE_100V;
  }else if(max_amp <= 200) {
    return PICO_X1_PROBE_200V;
  }else {
    printf("\t Max. amplitude too high. Exiting...\n");
    exit(-1);
  }
}

PS4000A_THRESHOLD_DIRECTION str_to_trigger_direction(char* str)
/* Convert input string to hardcoded int. for trigger mode */
{
  if(!strcmp(str, "ABOVE")) {
    return PS4000A_ABOVE;
  }else if(!strcmp(str, "BELOW")) {
    return PS4000A_BELOW;
  }else if(!strcmp(str, "RISING")) {
    return PS4000A_RISING;
  }else if(!strcmp(str, "FALLING")) {
    return PS4000A_FALLING;
  }else if(!strcmp(str, "CROSSING")) {
    return PS4000A_RISING_OR_FALLING;
  }else {
    printf("\t Threshold direction unrecognised. Using ABOVE.\n");
    return PS4000A_ABOVE;
  }
}
 
void read_settings(char* settings_filename,
		   SETTINGS* settings_p)
/* Reads settings from settings_filename, if specified, 
   and stores in structure at settings_p */
{
  /* Opens settings file for reading, storing pointer to file */
  FILE* settings_file = fopen(settings_filename,
				"r");
  int16_t strcount = 0;		/* No. of strings read */
  char str1[32], str2[32];	/* Last two strings read */
  char setting_name[32];	/* Setting name identified */
  char setting_val[256];	/* Setting value identified */
  char* str[2] = {str1, str2};	/* Pointer to appropriate string */
  int8_t ch_id;			/* Channel for which settings are read */
  float float_input;		/* Float read, before setting assignment */
  
  /* Sets default settings, in case any aren't specified */
  /* Sampling settings */
  settings_p->n_channels = 8;
  settings_p->sample_interval = 5;
  settings_p->time_units = PS4000A_US;
  settings_p->n_samples_pretrigger = 0;
  settings_p->n_samples_posttrigger = 1000000;
  settings_p->autostop = TRUE;
  settings_p->buffer_length = 100000;
  settings_p->downsample_ratio = 1;
  settings_p->downsample_mode = PS4000A_RATIO_MODE_NONE;
  strcpy(settings_p->filename_prototype,
	 "./picoscope_data");
  /* Channel settings */
  for (int ch_id = 0; ch_id < settings_p->n_channels; ch_id++) {
    settings_p->ch[ch_id].channel = PS4000A_CHANNEL_A + ch_id;
    settings_p->ch[ch_id].enabled = (ch_id < 3);
    settings_p->ch[ch_id].dc_coupling = TRUE;
    settings_p->ch[ch_id].range = PICO_X1_PROBE_10V;
    settings_p->ch[ch_id].offset = 0.0f;
  }
  /* Trigger settings */
  settings_p->tr.enabled = FALSE;
  settings_p->tr.channel = PS4000A_CHANNEL_A;
  settings_p->tr.threshold = 0;
  settings_p->tr.direction = PS4000A_RISING;
  settings_p->tr.delay = 0;
  settings_p->tr.autotrigger = 0;

  /* If the settings file is readable*/
  if (settings_file) {
    /* Reads first string, storing as str1 */
    fscanf(settings_file, "%s", str[strcount % 2]);
    /* Continues reading strings until the end of the file */
    while (fscanf(settings_file, "%s", str[(strcount + 1) % 2]) != EOF) {
      /* If the most recently read string is "=", ... */
      if(!(strcmp(str[(strcount + 1) % 2], "="))) {
	/* ...checks if the previously read string was a setting's name
	   and, if so, assigns the next string to the relevant setting*/
	strcpy(setting_name, str[strcount % 2]);
	fscanf(settings_file, "%s", setting_val);
	strcount++;
	if(!(strcmp(setting_name, "N_CHANNELS"))) {
	  sscanf(setting_val, "%hhd", &settings_p->n_channels);
	}else if(!(strcmp(setting_name, "SAMPLE_INTERVAL"))) {
	  sscanf(setting_val, "%u", &settings_p->sample_interval);
	}else if(!(strcmp(setting_name, "TIME_UNITS"))) {
	  sscanf(setting_val, "%s", str1);
	  settings_p->time_units = prefix_to_time_units(str1[0]);
	}else if(!(strcmp(setting_name, "N_SAMPLES_PRETRIGGER"))) {
	  sscanf(setting_val, "%u", &settings_p->n_samples_pretrigger);
	}else if(!(strcmp(setting_name, "N_SAMPLES_POSTTRIGGER"))) {
	  sscanf(setting_val, "%u", &settings_p->n_samples_posttrigger);
	}else if(!(strcmp(setting_name, "AUTOSTOP"))) {
	  sscanf(setting_val, "%hhd", &settings_p->autostop);
	}else if(!(strcmp(setting_name, "BUFFER_LENGTH"))) {
	  sscanf(setting_val, "%u", &settings_p->buffer_length);
	}else if(!(strcmp(setting_name, "DOWNSAMPLE_RATIO"))) {
	  sscanf(setting_val, "%hd", &settings_p->downsample_ratio);
	}else if(!(strcmp(setting_name, "DOWNSAMPLE_MODE"))) {
	  sscanf(setting_val, "%s", str1);
	  settings_p->downsample_mode = str_to_downsample_mode(str1);
	}else if(!(strcmp(setting_name, "FILENAME_PROTOTYPE"))) {
	  sscanf(setting_val, "%s", settings_p->filename_prototype);
	}else if(!(strcmp(setting_name, "CHANNEL"))) {
	  sscanf(setting_val, "%s", str1);
	  ch_id = (int)str1[0] - 65;
	  settings_p->ch[ch_id].channel = PS4000A_CHANNEL_A + ch_id;
	}else if(!(strcmp(setting_name, "ENABLED"))) {
	  sscanf(setting_val, "%hhd", &settings_p->ch[ch_id].enabled);
	}else if(!(strcmp(setting_name, "DC_COUPLING"))) {
	  sscanf(setting_val, "%s", str1);
	  settings_p->ch[ch_id].dc_coupling = str_to_coupling(str1, ch_id);
	}else if(!(strcmp(setting_name, "MAX_AMPLITUDE"))) {
	  sscanf(setting_val, "%f", &float_input);
	  settings_p->ch[ch_id].range = max_amp_to_range(float_input);
	}else if(!(strcmp(setting_name, "OFFSET"))) {
	  sscanf(setting_val, "%f", &settings_p->ch[ch_id].offset);
	}else if(!(strcmp(setting_name, "TRIGGER_ENABLED"))) {
	  sscanf(setting_val, "%hhd", &settings_p->tr.enabled);
	}else if(!(strcmp(setting_name, "TRIGGER_CHANNEL"))) {
	  sscanf(setting_val, "%s", str1);
	  settings_p->tr.channel = PS4000A_CHANNEL_A + ((int)str1[0] - 65);
	}else if(!(strcmp(setting_name, "THRESHOLD"))) {
	  sscanf(setting_val, "%f", &settings_p->tr.threshold);
	}else if(!(strcmp(setting_name, "DIRECTION"))) {
	  sscanf(setting_val, "%s", str1);
	  settings_p->tr.direction = str_to_trigger_direction(str1);
	}else if(!(strcmp(setting_name, "DELAY"))) {
	  sscanf(setting_val, "%u", &settings_p->tr.delay);
	}else if(!(strcmp(setting_name, "MAX_WAIT"))) {
	  sscanf(setting_val, "%hd", &settings_p->tr.autotrigger);
	}else {
	  /* If =-preceding string not recognised, says so */
	  printf("\t Unrecognised setting %s.\n", setting_name);
	  sscanf(setting_val, "%*s");
	}
	/* Reads next potential setting name */
	fscanf(settings_file, "%s", str[strcount % 2]);
	strcount++;
      }
      strcount++;
    }
    fclose(settings_file);
  }else {
    /* If settings file unreadable, says so */
    printf("\t File not readable. Using defaults.\n");
  }
}

float adc_to_v(PICO_CONNECT_PROBE_RANGE range,
	       int16_t picoscope_handle)
/* Returns voltage corresponding to unit recorded adc */
{
  int16_t max_adc;		/* Maximum adc outputted by device */
  float max_v;			/* Max. voltage measured on channel */
  /* Use int. representing probe range to output channel max. voltage */
  switch (range) {
  case PICO_X1_PROBE_10MV:
    max_v = 0.01;
    break;
  case PICO_X1_PROBE_20MV:
    max_v = 0.02;
    break;
  case PICO_X1_PROBE_50MV:
    max_v = 0.05;
    break;
  case PICO_X1_PROBE_100MV:
    max_v = 0.1;
    break;
  case PICO_X1_PROBE_200MV:
    max_v = 0.2;
    break;
  case PICO_X1_PROBE_500MV:
    max_v = 0.5;
    break;
  case PICO_X1_PROBE_1V:
    max_v = 1;
    break;
  case PICO_X1_PROBE_2V:
    max_v = 2;
    break;
  case PICO_X1_PROBE_5V:
    max_v = 5;
    break;
  case PICO_X1_PROBE_10V:
    max_v = 10;
    break;
  case PICO_X1_PROBE_20V:
    max_v = 20;
    break;
  case PICO_X1_PROBE_50V:
    max_v = 50;
    break;
  case PICO_X1_PROBE_100V:
    max_v = 100;
    break;
  case PICO_X1_PROBE_200V:
    max_v = 200;
    break;
  default:
    printf("Probe range unknown. Exiting \n");
    return -1;
  }
  /* Extract max adc outputted by device */
  ps4000aMaximumValue(picoscope_handle, &max_adc);
  /* V / adc = max_v / max_adc */
  return max_v / (float)max_adc;
}

void set_filename(char* filename, char* filename_prototype)
/* Sets data filename according to prototype provided:
   filename_prototype_[f_id].txt for smallest f_id avoiding overwrite */
{
  FILE* file = NULL;
  /* Avoids 'sort ascending' problems with single-digit-only f_id */
  for (int f_id = 0; f_id < 10; f_id++) {
    sprintf(filename,
	    "%s_%1d.txt",
	    filename_prototype,
	    f_id);
    file = fopen(filename, "r");
    if (file) {
      fclose(file);
    }
    else {
      return;
    }
  }
  printf("Code designed for file ids up to 9. Exiting. \n");
  exit(-1);
}
    
int8_t time_units_to_exponent(PS4000A_TIME_UNITS time_units)
/* Returns log_10 of (Picoscope time units / s)*/
{
  switch (time_units) {
  case PS4000A_FS:
    return -15;
  case PS4000A_PS:
    return  -12;
  case PS4000A_NS:
    return -9;
  case PS4000A_US:
    return -6;
  case PS4000A_MS:
    return -3;
  case PS4000A_S:
    return  0;
  default:
    printf("Time unit unknown. Fix bug.\n");
    return -1; 
  }
}

void PREF4 pretrigger_callback_fn(int16_t picoscope_handle,
				  int32_t n_new_samples,
				  uint32_t start_index,
				  int16_t overflow,
				  uint32_t trigger_index,
				  int16_t trigger_hit,
				  int16_t autostopped,
				  BUFFERS* buffers_p)
/* Callback function, copying data from picoscope stream to pretrigger
   buffer, when such data is available, and monitoring trigger */
{
  int32_t n_buffer_left, n_post_trigger = 0,
    n_to_write, read_position, write_position;
  n_buffer_left = buffers_p->n_pretrigger - buffers_p->position;

  /* Manage which samples to write to pretrigger buffer:
     Only those before the trigger and at most N_PRETRIGGER*/
  if (trigger_hit) {
    buffers_p->trigger_hit = 1;
    n_post_trigger = n_new_samples - trigger_index;
    n_new_samples = trigger_index;
  }
  while (n_new_samples > buffers_p->n_pretrigger) {
    start_index += buffers_p->n_pretrigger;
    n_new_samples -= buffers_p->n_pretrigger;
  }
  for (int ch_id = 0; ch_id < buffers_p->n_channels; ch_id++) {
    /* Copy all pre-trigger data from device buffer to
       pretrigger buffer, wrapping around if necessary*/
    if (buffers_p->enabled[ch_id]) {
      n_to_write = n_new_samples;
      read_position = start_index;
      write_position = buffers_p->position;
      if (n_to_write > n_buffer_left) {
	memcpy(&buffers_p->pre_main[ch_id][write_position],
	       &buffers_p->dev_main[ch_id][read_position],
	       n_buffer_left * sizeof(int16_t));
	memcpy(&buffers_p->pre_min[ch_id][write_position],
	       &buffers_p->dev_min[ch_id][read_position],
	       n_buffer_left * sizeof(int16_t));
	read_position += n_buffer_left;
	write_position = 0;
	n_to_write -= n_buffer_left;
      }
      memcpy(&buffers_p->pre_main[ch_id][write_position],
	     &buffers_p->dev_main[ch_id][read_position],
	     n_to_write * sizeof(int16_t));
      memcpy(&buffers_p->pre_min[ch_id][write_position],
	     &buffers_p->dev_min[ch_id][read_position],
	     n_to_write * sizeof(int16_t));
      write_position += n_to_write;
      read_position += n_to_write;
      /* Copy any post-trigger data to the app buffer */
      if (trigger_hit) {
	memcpy(&buffers_p->app_main[ch_id][0],
	       &buffers_p->dev_main[ch_id][read_position],
	       n_post_trigger * sizeof(int16_t));
	memcpy(&buffers_p->app_min[ch_id][0],
	       &buffers_p->dev_min[ch_id][read_position],
	       n_post_trigger * sizeof(int16_t));
      }
    }
  }
  buffers_p->position = write_position;
  buffers_p->n_new_samples = n_post_trigger;
}

void PREF4 streaming_callback_fn(int16_t picoscope_handle,
				 int32_t n_new_samples,
				 uint32_t start_index,
				 int16_t overflow,
				 uint32_t trigger_index,
				 int16_t trigger_hit,
				 int16_t autostopped,
				 BUFFERS* buffers_p)
/* Callback function, copying data from picoscope stream to buffers,
   when such data is available, and setting metadata appropriately */
{
  buffers_p->new_data = TRUE;
  buffers_p->n_new_samples = n_new_samples;
  buffers_p->autostopped = autostopped;
  for (int ch_id = 0; ch_id < buffers_p->n_channels; ch_id++) {
    if (buffers_p->enabled[ch_id]) {
      memcpy(&buffers_p->app_main[ch_id][0],
	     &buffers_p->dev_main[ch_id][start_index],
	     n_new_samples * sizeof(int16_t));
      memcpy(&buffers_p->app_min[ch_id][0],
	     &buffers_p->dev_min[ch_id][start_index],
	     n_new_samples * sizeof(int16_t));
    }
  }
}

void writeData(SETTINGS* settings,
	       int16_t** buffer,
	       int32_t start_id,
	       int32_t end_id,
	       int32_t n_old_samples,
	       float* adc2v,
	       FILE* file_id)
{
  int32_t s_id;
  int8_t ch_id;
  
  for (s_id = start_id; s_id < end_id; s_id++) {
    fprintf(file_id,
	    "%7d       ",
	    settings->sample_interval * (n_old_samples + s_id - start_id));
    for (ch_id = 0; ch_id < settings->n_channels; ch_id++) {
      if (settings->ch[ch_id].enabled) {
	fprintf(file_id,
		", % 10.6f",
		adc2v[ch_id] * buffer[ch_id][s_id]
		- settings->ch[ch_id].offset);
      }
    }
    fprintf(file_id,
	    "\n");
  }
}
  
int main(int n_args, char* arg_values[])
{
  SETTINGS settings;
  int16_t count = 0;
  int16_t serial_length = 100;
  int8_t serials[100];
  int16_t picoscope_handle;
  int8_t ch_id;
  BUFFERS buffers;
  char filename[256];
  FILE* file_id;
  float adc2v[PS4000A_MAX_CHANNELS];
  int32_t n_old_samples = 0;
  PICO_STATUS pico_status;
  int8_t tty = open("/dev/tty10", O_RDONLY);
  int32_t tone_count = (200<<16) + 0xc7e;
  int32_t tone_go = (50<<16) + 0x637;
    
  printf("Reading data from picoscope\n\n");

  /* Read settings, as defined above */
  printf("Reading user settings...\n");
  read_settings(arg_values[1], &settings);
  printf("\t Done.\n");
  
  /* Find picoscope and print serial number */
  printf("Searching for picoscope...\n");
  pico_status = ps4000aEnumerateUnits(&count, serials, &serial_length);
  if (pico_status != PICO_OK) {
    printf("Problem searching. Error message %d:\n", pico_status);
    printErrorMessage(pico_status);
    return 0;
  }
  printf("\t %d picoscope(s) found, serial number(s) %s.\n", count, serials);
  
  /* Open picoscope, store handle, and manage non-standard connection */
  printf("Connecting to picoscope...\n");
  pico_status = ps4000aOpenUnit(&picoscope_handle, NULL);
  if (pico_status == PICO_USB3_0_DEVICE_NON_USB3_0_PORT || pico_status == PICO_POWER_SUPPLY_NOT_CONNECTED) {
    printErrorMessage(pico_status);
    printf("\t Continuing anyway...\n");
    /* Confirm to device that this connection is OK */
    pico_status = ps4000aChangePowerSource(picoscope_handle, pico_status);
  }
  if (pico_status != PICO_OK) {
    printf("Problem connecting. Error message %d:\n", pico_status);
    printErrorMessage(pico_status);
    return 0;
  }
  printf("\t Done.\n");

    /* Set up channels, according to 'settings.ch' */
  printf("Setting channel settings...\n");
  for (ch_id = 0; ch_id < settings.n_channels; ch_id++){
    pico_status = ps4000aSetChannel(picoscope_handle,
			    (PS4000A_CHANNEL)settings.ch[ch_id].channel,
			    settings.ch[ch_id].enabled,
			    (PS4000A_COUPLING)settings.ch[ch_id].dc_coupling,
			    (PICO_CONNECT_PROBE_RANGE)settings.ch[ch_id].range,
			    settings.ch[ch_id].offset);
    adc2v[ch_id] = adc_to_v(settings.ch[ch_id].range,
			    picoscope_handle);
  }
  if (pico_status != PICO_OK) {
    printf("Problem setting up channels. Error message %d:\n", pico_status);
    printErrorMessage(pico_status);
    return 0;
  }
  printf("\t Done. \n");

  /* Set up trigger, according to 'settings.tr' */
  ch_id = settings.tr.channel - PS4000A_CHANNEL_A;
  printf("Setting trigger settings...\n");
  pico_status = ps4000aSetSimpleTrigger(picoscope_handle,
					settings.tr.enabled,
					settings.tr.channel,
					(int)(settings.tr.threshold	\
					      / adc2v[ch_id]),
					settings.tr.direction,
					settings.tr.delay,
					settings.tr.autotrigger);
  if (pico_status != PICO_OK) {
    printf("Problem setting up triggers. Error message %d:\n", pico_status);
    printErrorMessage(pico_status);
    return 0;
  }
  printf("\t Done.");
  if (settings.tr.enabled) {
    printf("Trigger threshold %f V, delay %u samples \n",
	   settings.tr.threshold, settings.tr.delay);
  } else {
    printf("Trigger off. Recording from sample %u \n",
	   settings.tr.delay);
  }

  /* Set up data buffers, according to 'settings' */
  printf("Setting up data buffers...\n");
  buffers.n_channels = settings.n_channels;
  buffers.n_pretrigger = settings.n_samples_pretrigger;
  buffers.trigger_hit = 0;
  for (ch_id = 0; ch_id < settings.n_channels; ch_id++){
    buffers.enabled[ch_id] = settings.ch[ch_id].enabled;
    if(settings.ch[ch_id].enabled){
      buffers.dev_main[ch_id] = (int16_t*)calloc(settings.buffer_length,
					    sizeof(int16_t));
      buffers.dev_min[ch_id] = (int16_t*)calloc(settings.buffer_length,
					    sizeof(int16_t));
      pico_status = ps4000aSetDataBuffers(picoscope_handle,
					  settings.ch[ch_id].channel,
					  buffers.dev_main[ch_id],
					  buffers.dev_min[ch_id],
					  settings.buffer_length,
					  0,
					  settings.downsample_mode);
      
      buffers.app_main[ch_id] = (int16_t*)calloc(settings.buffer_length,
						 sizeof(int16_t));
      buffers.app_min[ch_id] = (int16_t*)calloc(settings.buffer_length,
						sizeof(int16_t));
      buffers.pre_main[ch_id] = (int16_t*)calloc(buffers.n_pretrigger,
						 sizeof(int16_t));
      buffers.pre_min[ch_id] = (int16_t*)calloc(buffers.n_pretrigger,
						sizeof(int16_t));
      
      if (pico_status != PICO_OK) {
	printf("Problem setting up buffers for channel %d. Error message %d:\n", ch_id, pico_status);
    printErrorMessage(pico_status);
	return 0;
      }
    }
    buffers.n_new_samples = 0;
    buffers.autostopped = FALSE;
  }
  printf("\t Done. Buffer length %u \n", settings.buffer_length);

  /* Set up data file, according to 'settings' */
  printf("Setting up data file...\n");

  set_filename(filename,
	       settings.filename_prototype);
  printf("  filename set\n");
  file_id = fopen(filename,
		  "w");
  if(file_id == NULL) {
    printf("Problem opening data file at address\n"
	    "%s\n", filename);
    return 0;
  }
  printf("  file opened\n");
  fprintf(file_id,
	  "Time / 10^%-3ds, Voltage / V \n"
	  "t             ",
	  time_units_to_exponent(settings.time_units));
  printf("  header written\n");
  for (ch_id = 0; ch_id < settings.n_channels; ch_id++) {
    if(settings.ch[ch_id].enabled) {
      fprintf(file_id,
	      ", %C         ",
	      (char)('A' + ch_id));
    }
  }
  fprintf(file_id,
	  "\n");
  printf("\t Done. Writing to %s.\n\n", filename);
  
  /* Start streaming of data from Picoscope */
  printf("Starting streaming...\n");
  if(tty == -1){
    printf("(No beeps in countdown)\n");
  }
  if(!settings.tr.enabled){
    printf("Recording in:\n");
    for (int countdown = 2; countdown >= 0; countdown--) {
      printf("\t %d\n", countdown);
      ioctl(tty, KDMKTONE, tone_count);
      sleep(1);
    }
  }
  ioctl(tty, KDMKTONE, tone_go);
  pico_status = ps4000aRunStreaming(picoscope_handle,
				    &(settings.sample_interval),
				    settings.time_units,
				    settings.n_samples_pretrigger,
				    settings.n_samples_posttrigger,
				    settings.autostop,
				    settings.downsample_ratio,
				    settings.downsample_mode,
				    settings.buffer_length);
  if (pico_status != PICO_OK) {
    printf("Problem starting streaming. Error message %d:\n", pico_status);
    printErrorMessage(pico_status);
    return 0;
  }

  /* Manage pre-trigger data collection */
  if (settings.tr.enabled) {
    /* Collect streaming data while waiting for trigger */
    while (!buffers.trigger_hit) {
      ps4000aGetStreamingLatestValues(picoscope_handle,
				      pretrigger_callback_fn,
				      &buffers);
    }
    /* Write pre-trigger data from designated buffer */
    writeData(&settings,
	      buffers.pre_main,
	      buffers.position,
	      settings.n_samples_pretrigger,
	      -settings.n_samples_pretrigger,
	      adc2v,
	      file_id);
    writeData(&settings,
	      buffers.pre_main,
	      0,
	      buffers.position,
	      -buffers.position,
	      adc2v,
	      file_id);
    /*  Write immediate post-trigger data */
    writeData(&settings,
	      buffers.app_main,
	      0,
	      buffers.n_new_samples,
	      n_old_samples,
	      adc2v,
	      file_id);
    n_old_samples += buffers.n_new_samples;
  }
  
  /* Manage post-trigger data collection */
  printf("\nRecording...\n"
	 "\t Sample: %7d", n_old_samples);
  while (!buffers.autostopped) {
    buffers.new_data = FALSE;
    ps4000aGetStreamingLatestValues(picoscope_handle,
				    streaming_callback_fn,
				    &buffers);
    if (buffers.new_data) {
      if ((n_old_samples + buffers.n_new_samples)
	  >= settings.tr.delay) {
	writeData(&settings,
		  buffers.app_main,
		  0,
		  buffers.n_new_samples,
		  n_old_samples,
		  adc2v,
		  file_id);
      }
      n_old_samples += buffers.n_new_samples;
      printf("\b\b\b\b\b\b\b%7d", n_old_samples);
    }
  }
  printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bDone. %7d samples \n", n_old_samples);
  
  /* Stop data collection */
  printf("Disconnecting...\n");
  ps4000aStop(picoscope_handle);

  for (ch_id = 0; ch_id < settings.n_channels; ch_id++) {
    if (settings.ch[ch_id].enabled) {
      free(buffers.dev_main[ch_id]);
      free(buffers.dev_min[ch_id]);
      free(buffers.app_main[ch_id]);
      free(buffers.app_min[ch_id]);
      free(buffers.pre_main[ch_id]);
      free(buffers.pre_min[ch_id]);
    }
  }

  /* Close picoscope */
  ps4000aCloseUnit(picoscope_handle);
  printf("\t Done. \n");
  return 0;
}

Code to run a calibration of the accelerometers

Contents:
* `settings.txt` Settings file for `record_picoscope.exe`, to run a calibration
* `zero_settings.txt` Settings file for `record_picoscope.exe`, to take null data

Use:
* Run `record_picoscope.exe zero_settings.txt` to record the accelerometers' signal in the absence of forcing
* Run `record_picoscope.exe settings.txt`, before dropping a ball bearing onto the plate from a specified height, to record the accelerometers' signal in response to a fixed forcing
Code to run the calibration of the force sensor.

Contents:
* `runCalibration.ino` Arduino sketch to control the calibration
* `calib_settings.txt` Settings file for Picoscope recording during calibration

Use:
* See `experimental_procedure.pdf`
// Arduino Sketch file for control of force sensor calibration

// Define pins
const int triggerPin = A0;
const int forceSensorPin = 12;
const int picoscopePin = 13;
// Define threshold for switch trigger (1023 = 5V)
const int triggerValue = 511;
// Define experiment duration (ms)
const unsigned long experimentDuration = 5000;

void setup() {
  // Set up pins
  pinMode(forceSensorPin, OUTPUT);
  pinMode(picoscopePin, OUTPUT);
  digitalWrite(forceSensorPin, LOW);
  digitalWrite(picoscopePin, LOW);
}

void loop() {
  // Wait for switch trigger input
  while (analogRead(triggerPin) < triggerValue) {
    delayMicroseconds(100);
  }
  // Ensure timings exact and trigger force sensor
  unsigned long startTime = millis();
  digitalWrite(forceSensorPin, HIGH);
  digitalWrite(picoscopePin, HIGH);
  // Wait, if necessary, then reset force sensor
  while (millis() - startTime < experimentDuration) {
    delayMicroseconds(100);
  }
  digitalWrite(forceSensorPin, LOW);
  digitalWrite(picoscopePin, LOW);
  // Permit interruptions and wait for switch to be reset
  while (analogRead(triggerPin) > triggerValue) {}
}

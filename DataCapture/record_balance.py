### Program to read Dymo scales, modified from that of Steven Snyder
### (see http://steventsnyder.com/reading-a-dymo-usb-scale-using-python/)

import winsound
import os
import numpy as np
import usb.core
import usb.util
import time

## User settings
# Interval between and total duration of measurements (seconds)
interval = 0.2
duration = 20.
# Filename will be [filename_prototype]_[n].txt for lowest non-existing n
filename_prototype = 'balance_data'
# Frequency (Hz) and duration (ms) of the countdown & 'go' beeps
freq_count, freq_go = 440, 523
duration_count, duration_go = 200, 100

## Scale codes
VENDOR_ID = 0x0922
PRODUCT_ID = 0x8009

DATA_MODE_KG = 3
DATA_MODE_OZ = 12

DATA_UNSTABLE = 3
DATA_STABLE = 4
DATA_OVERFLOW = 6

# find the USB device
device = usb.core.find(idVendor=VENDOR_ID,
                       idProduct=PRODUCT_ID)
if device is None:
    raise ValueError('Balance not found')
else:
    print('DYMO S50 balance found')

# use the first/default configuration
device.set_configuration()
# first endpoint
endpoint = device[0][(0,0)][0]

# read data to check balance settings
data = None
attempts = 0
while (data == None and attempts < 10):
    try:
        data = device.read(endpoint.bEndpointAddress,
                           endpoint.wMaxPacketSize)
    except usb.core.USBError as e:
        data = None
        attempts = attempts + 1
if data is None:
    raise ValueError('No data from balance')
# check units
if data[2] == DATA_MODE_OZ:
    raise ValueError('Units ounces. Switch to kilograms')
elif not data[2] == DATA_MODE_KG:
    raise ValueError('Unrecognised units. Switch to kilograms')
# check scaling factor
scaling_factor = scaling_factor = 10**(data[3] - 256)

# avoid overwriting an existing file
rep = 0
filename = filename_prototype + '_{:d}.txt'.format(rep)
while os.path.isfile(filename):
    rep += 1
    filename = filename_prototype + '_{:d}.txt'.format(rep)

# open data file and write header
file = open(filename, 'w+')
file.write('Time / s, Mass / kg\n')

# provide countdown to measurement
for id in range(3):
    winsound.Beep(freq_count, duration_count)
    time.sleep(1)
winsound.Beep(freq_go, duration_go)

start_time = time.perf_counter()

# take measurements at regular intervals, over specified duration 
for sample in range(1 + int(duration / interval)):
    wait_time = start_time + sample * interval - time.perf_counter()
    time.sleep(max(0, wait_time))
    t = time.perf_counter() - start_time
    # read a data packet
    try:
        data = device.read(endpoint.bEndpointAddress,
                           endpoint.wMaxPacketSize)
        if data[1] == DATA_OVERFLOW:
            mass = np.nan
        else:
            mass = scaling_factor * (256 * data[5] + data[4])
    except usb.core.USBError as e:
        mass = np.nan
    file.write('{:8.2f}, {:4.1f}\n'.format(t, mass))


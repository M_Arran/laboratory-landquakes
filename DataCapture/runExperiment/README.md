Code to run an experiment.

Contents:
* `runExperiment.ino` Arduino sketch to control the experiment
* `exp_settings.txt` Settings file for Picoscope recording during an experiment

Use:
* See `experimental_procedure.pdf`
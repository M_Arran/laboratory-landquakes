// Arduino Sketch file for control of an experiment

// Define pins
const int triggerPin = A0;
const int cameraPin = 11;
const int forceSensorPin = 12;
const int picoscopePin = 13;
// Define threshold for switch trigger (1023 = 5V)
const int triggerValue = 511;
// Define delay before camera trigger (ms)
const unsigned long cameraDelay = 8000;
// Define footage duration (ms)
const unsigned long footageDuration = 2000;
// Define experiment duration (ms)
const unsigned long experimentDuration = 20000;

void setup() {
  // Set up pins
  pinMode(forceSensorPin, OUTPUT);
  pinMode(cameraPin, OUTPUT);
  pinMode(picoscopePin, OUTPUT);
  digitalWrite(forceSensorPin, LOW);
  digitalWrite(cameraPin, HIGH);
  digitalWrite(picoscopePin, LOW);
}

void loop() {
  // Wait for switch trigger input
  while (analogRead(triggerPin) < triggerValue) {
    delayMicroseconds(100);
  }
  // Ensure timings exact and trigger force sensor
  unsigned long startTime = millis();
  digitalWrite(forceSensorPin, HIGH);
  digitalWrite(picoscopePin, HIGH);
  // Wait, then trigger camera
  while (millis() - startTime < cameraDelay) {
    delayMicroseconds(100);
  }
  digitalWrite(cameraPin, LOW);
  digitalWrite(picoscopePin, LOW);
  // Wait, then reset camera trigger
  while (millis() - startTime < cameraDelay + footageDuration) {
    delayMicroseconds(100);
  }
  digitalWrite(cameraPin, HIGH);
  digitalWrite(picoscopePin, HIGH);
  // Wait, if necessary, then reset force sensor
  while (millis() - startTime < experimentDuration) {
    delayMicroseconds(100);
  }
  digitalWrite(forceSensorPin, LOW);
  digitalWrite(picoscopePin, LOW);
  // Permit interruptions and wait for switch to be reset
  while (analogRead(triggerPin) > triggerValue) {}
}
